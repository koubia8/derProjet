

function sendAjaxRequestDELETE(type, url) {
    var contextPath = $("meta[name='ctx']").attr("content");
    $.ajax({
        type: type,
        url: contextPath + "/" + url,
        contentType: 'application/json; charset=utf-8',
        timeout: 10000,
        data: null,
        beforeSend: function () {
            
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200 ) {
                $.uiAlert({
                       textHead: 'Opération réussie avec succés',
                       text: xhr.getResponseHeader('X-lubam-alert'),
                       bgcolor: '#19c3aa',
                       textcolor: '#fff',
                       position: 'top-right',
                       icon: 'checkmark box',
                       time: 3,
                   });
                           //$("#act").attr("class", "ui green mini basic label")
            }
            else{
                
            }
//            setTimeout(function () {
//                location.reload();
//            }, 1500);
            
        },
        error: function (data) {
               
               // var message = xhr.getResponseHeader('X-myappApp-error');
                $.uiAlert({
                   textHead: "Erreur lors de l'opération", // header
                   text: data.responseJSON.message, // Text
                   bgcolor: '#DB2828', // background-color
                   textcolor: '#fff', // color
                   position: 'top-right',// position . top And bottom ||  left / center / right
                   icon: 'remove circle', // icon in semantic-UI
                   time: 3, // time
                 });
        }
    });
}

function sendAjaxRequest(type, url, data) {
    var contextPath = $("meta[name='ctx']").attr("content");
    
    $.ajax({
        type: type,
        url: contextPath + "/" + url,
        contentType: 'application/json; charset=utf-8',
        timeout: 10000,
        data: JSON.stringify(data),
        dataType: 'json',
        beforeSend: function () {
          
         $("#saveuser, #savesecteur, #savepart").addClass("loading disabled");
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200 || xhr.status === 201) {
                //$.notify(xhr.getResponseHeader('X-lubam-alert'), "success");
                 $("#saveuser, #savesecteur, #savepart").removeClass("loading disabled");
                
                $.uiAlert({
                       textHead: 'Opération réussie avec succés',
                       text: xhr.getResponseHeader('X-lubam-alert'),
                       bgcolor: '#19c3aa',
                       textcolor: '#fff',
                       position: 'top-right',
                       icon: 'checkmark box',
                       time: 3,
                   });
            }
            
            setTimeout(function () {
                location.reload();
            }, 1500);
                    
        },
        error: function (data) {
                   $.uiAlert({
                   textHead: "Erreur lors de la saisie", // header
                   text: data.responseJSON.message, // Text
                   bgcolor: '#DB2828', // background-color
                   textcolor: '#fff', // color
                   position: 'top-right',// position . top And bottom ||  left / center / right
                   icon: 'remove circle', // icon in semantic-UI
                   time: 3, // time
                 });
        }
    });
}



function sendAjaxRequestEdit(type, url, data,redirect) {
    var contextPath = $("meta[name='ctx']").attr("content");
    
    $.ajax({
        type: type,
        url: contextPath + "/" + url,
        contentType: 'application/json; charset=utf-8',
        timeout: 10000,
        data: JSON.stringify(data),
        dataType: 'json',
        beforeSend: function () {
          
          $("#edituser").addClass("loading").attr("disabled","disabled");
        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200 || xhr.status === 201) {
                //$.notify(xhr.getResponseHeader('X-lubam-alert'), "success");
                $("#edituser").removeClass("loading").removeAttr("disabled");
                $.uiAlert({
                       textHead: 'Opération réussie avec succés',
                       text: xhr.getResponseHeader('X-lubam-alert'),
                       bgcolor: '#19c3aa',
                       textcolor: '#fff',
                       position: 'top-right',
                       icon: 'checkmark box',
                       time: 3,
                   });
            }
            
            setTimeout(function () {
                window.location.href=contextPath+"/"+redirect;
            }, 1500);
                    
        },
        error: function (data) {
                   $.uiAlert({
                   textHead: "Erreur lors de la saisie", // header
                   text: data.responseJSON.message, // Text
                   bgcolor: '#DB2828', // background-color
                   textcolor: '#fff', // color
                   position: 'top-right',// position . top And bottom ||  left / center / right
                   icon: 'remove circle', // icon in semantic-UI
                   time: 3, // time
                 });
        }
    });
}



function dataTable(id, nomObjet) {

    $(id).dataTable({
        responsive: true,
        "aaSorting": [[0, "desc"]],
        oLanguage: {
            sLengthMenu: "Afficher par: _MENU_ ",
            sSearch: "Rechercher : ",
            sZeroRecords: "Aucune valeur trouv&eacute;",
            sInfo: "Afficher page _PAGE_ sur _PAGES_",
            sInfoFiltered: "(Filtres sur _MAX_ )",
            sInfoEmpty: "Aucun resultat trouv&eacute;",
            oPaginate: {
                sFirst: "<<",
                sPrevious: "<",
                sNext: ">",
                sLast: ">>"
            }
        },
        "pagingType": "full_numbers"
    });
    
}

function loadSelectDepartement(region,departement,commune){
    $("#"+departement).html('');
    $("#"+commune).html('');
    var parentId = $('#'+region).val();
    var contextPath = $("meta[name='ctx']").attr("content");
    $.ajax({
        type: 'GET',
        url: contextPath + "/" + 'departement/' + parentId,
        contentType: 'application/json; charset=utf-8',
        timeout: 10000,
        data: null,
        dataType: 'json',
        beforeSend: function () {
            
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var option = "<option value=" + data[i].id + ">" + data[i].nom + "</option>";
                $("#"+departement).append(option);
            }
          
        },
        error: function () {
            
        }
    });
}

function loadSelectCommune(departement,commune){
    $("#"+commune).html('');
    var parentId = $('#'+departement).val();
    var contextPath = $("meta[name='ctx']").attr("content");
    $.ajax({
        type: 'GET',
        url: contextPath + "/" + 'commune/' + parentId,
        contentType: 'application/json; charset=utf-8',
        timeout: 10000,
        data: null,
        dataType: 'json',
        beforeSend: function () {
           
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var option = "<option value=" + data[i].id + ">" + data[i].nom + "</option>";
                $("#"+commune).append(option);
            }
            
        },
        error: function () {
            
        }
    });
}



function loadSousSecteur(secteur,sousSecteur){
    $("#"+sousSecteur).html('');
    var secteurId = $('#'+secteur).val();
    var contextPath = $("meta[name='ctx']").attr("content");
    $.ajax({
        type: 'GET',
        url: contextPath + "/api/" + 'getSousSecteursBySecteur/' + secteurId,
        contentType: 'application/json; charset=utf-8',
        timeout: 10000,
        data: null,
        dataType: 'json',
        beforeSend: function () {
             
        },
        success: function (data, textStatus, xhr) {
            for (var i = 0; i < data.length; i++) {
                var option = "<option value=" + data[i].id + ">" + data[i].nom + "</option>";
                $("#"+sousSecteur).append(option);
            }
            
        },
        error: function (){
            
        }
    });
}

$("#liste-user").DataTable();
$("#table_secteurs").DataTable();
$("#table_sous_secteurs").DataTable();
$("#liste-partenaire").DataTable();
$("#liste-demande-all").DataTable();
$("#liste-demande-pending").DataTable();
$("#liste-demande-valided").DataTable();
$("#liste-demande-rejected").DataTable();
$("#liste-demande-listagent").DataTable();
$(document).ready(function () {
//    dataTable("#table_secteurs", "secteur");
//    dataTable("#liste-user", "user");
//    dataTable("#liste-partenaire", "partenaire");
//    dataTable("#liste-demande", "demande");
//    dataTable("#liste-demande-departementale", "demande");


});  
