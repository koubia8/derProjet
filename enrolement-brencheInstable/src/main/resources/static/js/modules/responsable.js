$(document).on('submit', '#formResponsable', function (e) {
    e.preventDefault();
    var nom = $("#nomResponsable").val();
    var prenom = $("#prenomResponsable").val();
    var sexe = $("#sexeResponsable").val();
    var dateNaissance = $("#dateNaissanceResponsable").val();
    var demandeId = $("#responsableDemandeId").val();
    var cni = $("#cniResponsable").val();
    var fonction= $("#fonctionResponsable").val();
    var data = {
        "demandeId": demandeId,
        "nom": nom,
        "prenom": prenom,
        "sexe": sexe,
        "cni":cni,
        "fonction":fonction,
        "dateNaissance": dateNaissance
    };
    //console.log(data);
    sendAjaxRequest("POST", "api/responsables", data);
});

$(document).on('click', '.btnGetResponsable', function (e) {
    var id = $(this).attr("data-id");
    e.preventDefault();
    $.ajax({
        url: contextPath + '/api/responsables/' + id,
        type: 'GET',
        data: null,
        beforeSend: function () {

        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                $('#idResponsableEdit').val(data.id);
                $('#nomResponsableEdit').val(data.nom);
                $('#prenomResponsableEdit').val(data.prenom);
                $('#cniResponsableEdit').val(data.cni);
                $('#fonctionResponsableEdit').val(data.fonction);
                $('#dateNaissanceResponsableEdit').val(data.dateNaissance);
                $('#sexeResponsableEdit option[value="' + data.sexe + '"]').prop('selected', true);
                
                $('#modalEditResponsable').modal("show");
            } else {

            }
        },
        error: function () {

        }
    });
});


$(document).on('submit', '#formResponsableEdit', function (e) {
    e.preventDefault();
    var demandeId = $("#responsableDemandeId").val();
    var id = $("#idResponsableEdit").val();
    var nom = $("#nomResponsableEdit").val();
    var prenom = $("#prenomResponsableEdit").val();
    var sexe = $("#sexeResponsableEdit").val();
    var dateNaissance = $("#dateNaissanceResponsableEdit").val();
    var cni = $("#cniResponsableEdit").val();
    var fonction= $("#fonctionResponsableEdit").val();
    var data = {
        "demandeId": demandeId,
        "id":id,
        "nom": nom,
        "prenom": prenom,
        "sexe": sexe,
        "cni":cni,
        "fonction":fonction,
        "dateNaissance": dateNaissance
    };
    sendAjaxRequest("PUT", "api/responsables", data);
});


$(document).on('click', '.btnDeleteResponsable', function (e) {
    e.preventDefault();
    var id = $(this).attr("data-id");
    $("#modalConfirmation").modal("show");

    $(document).on('click', '#btnValiderConfirmation', function (e) {
        $("#modalConfirmation").modal("hide");
        sendAjaxRequestDELETE("DELETE", "api/responsables/" + id);
    });

});
