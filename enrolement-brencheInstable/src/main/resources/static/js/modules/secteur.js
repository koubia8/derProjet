$(document).on('submit', '#formSecteur', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var nom = $("#nomSecteur").val();
    var description = $("#descriptionSecteur").val();
    var verified = $("#verified_add").is(':checked') ? "true":"false";
    var data = {
        "nom": nom,
        "description": description,
        "verified": verified
    };
    // console.log(data);
     sendAjaxRequest("POST", "api/secteurs", data);
});

$(document).on('click', '.btnGetSecteur', function (e) {
    var id = $(this).attr("data-id");
    e.preventDefault();
    e.stopImmediatePropagation();
    $.ajax({
        url: contextPath + '/api/secteurs/' + id,
        type: 'GET',
        data: null,
        beforeSend: function () {

        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                $('#idSecteurEdit').val(data.id);
                $('#nomSecteurEdit').val(data.nom);
                $('#descriptionSecteurEdit').val(data.description);
                if (data.verified === true) {
                    $('#verified_edit option[value=true]').prop('selected', true);
                } else if (data.verified === false) {
                    $('#verified_edit option[value=false]').prop('selected', true);
                }

                $('#modalEditSecteur').modal("show");
            }
        },
        error: function () {

        }
    });
});


$(document).on('submit', '#formSecteurEdit', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $("#idSecteurEdit").val();
    var nom = $("#nomSecteurEdit").val();
    var description = $("#descriptionSecteurEdit").val();
    var verified = $("#verified_edit").is(':checked') ? "true":"false";
    var data = {
        "id": id,
        "nom": nom,
        "description": description,
        "verified": verified
    };
    //console.log(data);
   sendAjaxRequest("PUT", "api/secteurs", data);
});


$(document).on('click', '.btnDeleteSecteur', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    $("#modalConfirmation").modal("show");

    $(document).on('click', '#btnValiderConfirmation', function (e) {
        $("#modalConfirmation").modal("hide");
        sendAjaxRequestDELETE("DELETE", "api/secteurs/" + id);
    });

});
$(document).on('change', '.EnableOrDisableSecteur', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    var choix = $(this).is(":checked");
    if(choix){
       // console.log(id);
      sendAjaxRequestDELETE("DELETE", "api/secteur/active/"+id); 
    }
    else{
       // console.log(id);
        sendAjaxRequestDELETE("DELETE", "api/secteur/desactive/"+id); 
    }
    
});



