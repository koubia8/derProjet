$(document).on('submit', '#formSousSecteur', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var nom = $("#nomSousSecteur").val();
    var description = $("#descriptionSouSecteur").val();
    var secteurId = $("#sousSecteurSecteurId").val();
    var data = {
        "nom": nom,
        "secteurId": secteurId,
        "description": description
    };
    //console.log(data);
   sendAjaxRequest("POST", "api/sousSecteurs", data);
});

$(document).on('click', '.btnGetSousSecteur', function (e) {
    var id = $(this).attr("data-id");
    e.preventDefault();
    e.stopImmediatePropagation();
    $.ajax({
        url: contextPath + '/api/sousSecteurs/' + id,
        type: 'GET',
        data: null,
        beforeSend: function () {

        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                $('#idSousSecteurEdit').val(data.id);
                $('#nomSousSecteurEdit').val(data.nom);
                $('#modalEditSousSecteur').modal("show");
            } else {

            }
        },
        error: function () {

        }
    });
});


$(document).on('submit', '#formSousSecteurEdit', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $("#idSousSecteurEdit").val();
    var nom = $("#nomSousSecteurEdit").val();
    var secteurId = $("#sousSecteurSecteurId").val();
    var description = $("#descriptionSousSecteurEdit").val();
    var data = {
        "id": id,
        "nom": nom,
        "secteurId":secteurId,
        "description":description
    };
   // console.log(data);
    sendAjaxRequest("PUT", "api/sousSecteurs", data);
});


$(document).on('click', '.btnDeleteSousSecteur', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    $("#modalConfirmation").modal("show");
   // alert("mod");
   // sendAjaxRequestDELETE("DELETE", "api/sousSecteurs/" + id);
    
    
  $(document).on('click', '#btnValiderConfirmation', function (e) {
        $("#modalConfirmation").modal("hide");
        sendAjaxRequestDELETE("DELETE", "api/sousSecteurs/" + id);
    });
   
});

 $(document).on('change', '.EnableOrDisableSousSecteur', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    var choix = $(this).is(":checked");
    if(choix){
        console.log(id);
        sendAjaxRequestDELETE("DELETE", "api/sousecteur/active/"+id); 
    }
    else{
        console.log(id);
        sendAjaxRequestDELETE("DELETE", "api/sousecteur/desactive/"+id); 
    }
    
});