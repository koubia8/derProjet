$('.ui.formpatner').form({
        inline: true,
        on: 'blur',
        fields: {
            nomp: {
                identifier: 'nomp',
                rules: [{
                    type: 'empty',
                    prompt: 'Veuillez donner le nom '
                }]
            },
            email: {
             identifier: 'email',
             rules: [{
                 type   : 'email',
                 prompt : 'Veuillez donner une adresse mail valide'
                 }]
            },
            typp: {
                identifier: 'typp',
                rules: [{
                    type: 'empty',
                    prompt: 'Veuillez selectionner le type de partenaire'
                }]
            },
            telp: {
                    identifier: 'telp',
                    rules: [{
                        type: 'empty',
                        prompt: 'Veuillez donner votre numéro de téléphone'
                    }, {
                        type: 'minLength[9]',
                        prompt: 'Le numéro de téléphone ne doit pas dépasser {ruleValue} chiffres'
                    }]
                },
            patneru: {
                identifier: 'patneru',
                rules: [{
                    type: 'empty',
                    prompt: 'Selectionner un partenaire'
                }]
            },
            regionp: {
                identifier: 'regionp',
                rules: [{
                    type: 'empty',
                    prompt: 'Selectionner une region'
                }]
            },
            depp: {
                identifier: 'depp',
                rules: [{
                    type: 'empty',
                    prompt: 'Selectionner un departement'
                }]
            },
            comp: {
                identifier: 'comp',
                rules: [{
                    type: 'empty',
                    prompt: 'Selectionner une commune'
                }]
            }
          },
          onSuccess: function(event){
        	  event.preventDefault();
        	  event.stopImmediatePropagation();
    var nom = $("#nomPartenaire").val();
    var email = $("#emailPartenaire").val();
    var activated = "true";
    var telephone = $("#telephonePartenaire").val();
    var regionId = $("#regionIdPartenaire").val();
    var departementId = $("#departementIdPartenaire").val();
    var communetId = $("#communeIdPartenaire").val();
    var idTypePartenaire = $("#typePartenaireIdPartenaire").val();
    var commentairePartenaire = $("#commentairePartenaire").val();
    var data = {
        "id":null,
        "nom": nom,
        "email":email,
        "activated":activated,
        "telephone":telephone,
        "regionId":regionId,
        "departementId":departementId,
        "communetId":communetId,
        "idTypePartenaire":idTypePartenaire,
        "commentaire":commentairePartenaire
    };
   
    sendAjaxRequest("POST", "api/partenaires", data);
          },
          onFailure: function(event){
              event.preventDefault();
                $.uiAlert({
                textHead: "Erreur lors de la saisie", // header
                text: 'Veuillez bien remplir le formulaire', // Text
                bgcolor: '#DB2828', // background-color
                textcolor: '#fff', // color
                position: 'top-right',// position . top And bottom ||  left / center / right
                icon: 'remove circle', // icon in semantic-UI
                time: 3, // time
              });
          }
     });

     $("#savepart").click(function(){
         $('.ui.formpatner').form('validate formpatner');
     });





$(document).on('click', '.btnGetPartenaire', function (e) {
    var id = $(this).attr("data-id");
    e.preventDefault();
    e.stopImmediatePropagation();
    $.ajax({
        url: contextPath + '/api/partenaires/' + id,
        type: 'GET',
        data: null,
        beforeSend: function () {

        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                $('#idPartenaireEdit').val(data.id);
                $('#nomPartenaireEdit').val(data.nom);
                if(data.verified === true){
                    $('#verified_edit option[value=true]').prop('selected', true);
                }
                else if(data.verified === false){
                  $('#verified_edit option[value=false]').prop('selected', true);  
                }
                
                $('#modalEditPartenaire').modal("show");
            } 
        },
        error: function () {

        }
    });
});



$('.ui.formeditpatner').form({
    inline: true,
    on: 'blur',
    fields: {
        nom: {
            identifier: 'nom',
            rules: [{
                type: 'empty',
                prompt: 'Veuillez donner le nom '
            }]
        },
        email: {
         identifier: 'email',
         rules: [{
             type   : 'email',
             prompt : 'Veuillez donner une adresse mail valide'
             }]
        },
        typp: {
            identifier: 'typp',
            rules: [{
                type: 'empty',
                prompt: 'Veuillez selectionner le type de partenaire'
            }]
        },
        telephone: {
                identifier: 'telephone',
                rules: [{
                    type: 'empty',
                    prompt: 'Veuillez donner votre numéro de téléphone'
                }, {
                    type: 'minLength[9]',
                    prompt: 'Le numéro de téléphone ne doit pas dépasser {ruleValue} chiffres'
                }]
            },
        patneru: {
            identifier: 'patneru',
            rules: [{
                type: 'empty',
                prompt: 'Selectionner un partenaire'
            }]
        },
        regionp: {
            identifier: 'regionp',
            rules: [{
                type: 'empty',
                prompt: 'Selectionner une region'
            }]
        },
        depp: {
            identifier: 'depp',
            rules: [{
                type: 'empty',
                prompt: 'Selectionner un departement'
            }]
        },
        comp: {
            identifier: 'comp',
            rules: [{
                type: 'empty',
                prompt: 'Selectionner une commune'
            }]
        }
      },
      onSuccess: function(event){
    	  event.preventDefault();
    	  event.stopImmediatePropagation();
		    var id = $("#idPartenaireEdit").val();
		    var nom = $("#nomPartenaireEdit").val();
		    var email = $("#emailPartenaireEdit").val();
		    var activated = "true";
		    var telephone = $("#telephonePartenaireEdit").val();
		    var regionId = $("#regionIdPartenaire").val();
		    var departementId = $("#departementIdPartenaireEdit").val();
		    var communetId = $("#communeIdPartenaireEdit").val();
		    var idTypePartenaire = $("#typePartenaireIdPartenaireEdit").val();
		    var commentairePartenaire = $("#commentairePartenaireEdit").val();
		    var data = {
		        "id":id,
		        "nom": nom,
		        "email":email,
		        "activated":activated,
		        "telephone":telephone,
		        "idRegion":regionId,
		        "departementId":departementId,
		        "communetId":communetId,
		        "idTypePartenaire":idTypePartenaire,
		        "commentaire":commentairePartenaire
		    };
		    
		    console.log(data);
		    sendAjaxRequest("PUT", "api/partenaires", data);
		},
		onFailure: function(event){
			  event.preventDefault();
		      $.uiAlert({
		      textHead: "Erreur lors de la saisie", // header
		      text: 'Veuillez bien remplir le formulaire', // Text
		      bgcolor: '#DB2828', // background-color
		      textcolor: '#fff', // color
		      position: 'top-right',// position . top And bottom ||  left / center / right
		      icon: 'remove circle', // icon in semantic-UI
		      time: 3, // time
		    });
		}
		});
		
		$("#editpartner").click(function(){
		$('.ui.formeditpatner').form('validate formeditpatner');
		});



$(document).on('click', '.btnDeletePartenaire', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    $("#modalConfirmation").modal("show");

    $(document).on('click', '#btnValiderConfirmation', function (e) {
        $("#modalConfirmation").modal("hide");
        sendAjaxRequestDELETE("DELETE", "api/partenaires/"+id);
    });

});

$(document).on('change', '.EnableOrDisablePartenaire', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    var choix = $(this).is(":checked");
    if(choix){
        console.log(id);
      sendAjaxRequestDELETE("DELETE", "api/partenaire/active/"+id); 
    }
    else{
       // console.log(id);
      sendAjaxRequestDELETE("DELETE", "api/partenaire/desactive/"+id); 
    }
    
});




