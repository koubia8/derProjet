$(document).ready(function () {
    /* $('#info-demand').off('click');
     $('#info-sp').off('click');
     $('#info-dp').off('click');
     $('#rel-if').off('click'); */
    /*     function getvals(){ */

    /* form first */
    $('.ui.myform').form({
        inline: true,
        on: 'blur',
        fields: {
            projet: {
                identifier: 'projet',
                rules: [{
                        type: 'checked',
                        prompt: 'Choisir un type de projet'
                    }]
            },
            amount: {
                identifier: 'amount',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez donner un fond'
                    }]
            },
            secteur: {
                identifier: 'secteur',
                rules: [{
                        type: 'empty',
                        prompt: 'Selectionnez un secteur'
                    }]
            },
            bp: {
                identifier: 'bp',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez donnez un Business plan'
                    }]
            }
        },
        onSuccess: function (event, fields) {
            location.hash = "demande/info-demandeur";
            $.tab('change tab', 'demande/info-demandeur');
            $('#info-demand').on('click');
            event.preventDefault();
        },
        onFailure: function (event, formErrors, fields) {
            event.preventDefault();
        }
    });

    $("#next").click(function () {
        $('.ui.myform').form('validate myform');
    });



    /* form two */
    $('.ui.myform2').form({
        inline: true,
        on: 'blur',
        fields: {
            nom: {
                identifier: 'nom',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez donner votre nom'
                    }]
            },
            prenom: {
                identifier: 'prenom',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez donner votre prénom'
                    }]
            },
            genre: {
                identifier: 'genre',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez choisir le sexe'
                    }]
            },
            adresse: {
                identifier: 'adresse',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez renseigner adresse'
                    }]
            },
            lieu: {
                identifier: 'lieu',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez donner votre lieu de naissance'
                    }]
            },
            cin: {
                identifier: 'cin',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez donner votre numéro de CNI'
                    }, {
                        type: 'minLength[13]',
                        prompt: 'Le numéro de CNI ne doit pas dépasser {ruleValue} caractères'
                    }]
            },

            tel1: {
                identifier: 'tel1',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez donner votre numéro de téléphone'
                    }, {
                        type: 'minLength[9]',
                        prompt: 'Le numéro de téléphone ne doit pas dépasser {ruleValue} chiffres'
                    }]
            },

            tel2: {
                identifier: 'tel2',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez donner votre numéro de téléphone'
                    }, {
                        type: 'minLength[9]',
                        prompt: 'Le numéro de téléphone ne doit pas dépasser {ruleValue} chiffres'
                    }]
            },
            email: {
                identifier: 'email',
                rules: [
                    {
                        type: 'email',
                        prompt: 'Veuillez donner une adresse mail valide'
                    }
                ]
            },
            url: {
                identifier: 'url',
                rules: [
                    {
                        type: 'url',
                        prompt: 'Veuillez donner un url valide'
                    }
                ]
            }
        },
        onSuccess: function (event, fields) {
            location.hash = "demande/info-sprojet";
            $.tab('change tab', 'demande/info-sprojet');
            $('#info-sp').on('click');
            $("#fond-roulm").val($("#amount").val());
            event.preventDefault();
        },
        onFailure: function (event, formErrors, fields) {
            event.preventDefault();
        }
    });

    $("#next1").click(function () {
        $('.ui.myform2').form('validate myform2');

    });

    /* Three form */

    $('.ui.myform3').form({
        inline: true,
        on: 'blur',
        fields: {
            resume: {
                identifier: 'resume',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez faire un résumé de votre projet'
                    }]
            },
            pactivity: {
                identifier: 'pactivity',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez donner l\'activité à réaliser'
                    }]
            },
            pservices: {
                identifier: 'pservices',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez renseigner les produits ou services'
                    }]
            },
            choix: {
                identifier: 'choix',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez faire le choix de votre besoin'
                    }]
            },
            nhomme: {
                identifier: 'nhomme',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Veuillez donner le nombre d\'homme'
                    }
                ]
            },
            nfemme: {
                identifier: 'nfemme',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Veuillez donner le nombre d\'homme'
                    }
                ]
            }

        },
        onSuccess: function (event, fields) {
            location.hash = "demande/info-dprojet";
            $.tab('change tab', 'demande/info-dprojet');
            $('#info-dp').on('click');
            event.preventDefault();
        },
        onFailure: function (event, formErrors, fields) {
            event.preventDefault();
        }
    });

    $("#next2").click(function () {
        $('.ui.myform3').form('validate myform3');
    });


    /* Four form */

    $('.ui.myform4').form({
        inline: true,
        on: 'blur',
        fields: {
            entite: {
                identifier: 'entite',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez choisir une entité'
                    }]
            },
            region: {
                identifier: 'region',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez choisir une région'
                    }]
            },
            departement: {
                identifier: 'departement',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez choisir un département'
                    }]
            },
            commune: {
                identifier: 'commune',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez choisir une commune'
                    }]
            },
            soussecteur: {
                identifier: 'soussecteur',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez choisir un sous-secteur'
                    }]
            },
            qualification: {
                identifier: 'qualification',
                rules: [{
                        type: 'checked',
                        prompt: 'Veuillez selectionner la qualification'
                    }]
            },
            competence: {
                identifier: 'competence',
                rules: [{
                        type: 'checked',
                        prompt: 'Vauillez selectionner la compétence'
                    }]
            },
            nbannee: {
                identifier: 'nbannee',
                rules: [{
                        type: 'empty',
                        prompt: 'Veuillez donner le nombre d\'année d\'experience'
                    }]
            },

        },
        onSuccess: function (event, fields) {
            location.hash = "demande/rel-if";
            $.tab('change tab', 'demande/rel-if');
            $('#rel-if').on('click');
            event.preventDefault();
        },
        onFailure: function (event, formErrors, fields) {
            event.preventDefault();
        }
    });

    $("#next3").click(function () {
        $('.ui.myform4').form('validate myform4');
    });



    /* Five form */

    $('.ui.myform5').form({
        inline: true,
        on: 'blur',
        fields: {
            compte: {
                identifier: 'compte',
                rules: [{
                        type: 'checked',
                        prompt: 'Veuillez répondre à cette question'
                    }]
            },
            beneficiare: {
                identifier: 'beneficiare',
                rules: [{
                        type: 'checked',
                        prompt: 'Veuillez répondre à cette question'
                    }]
            },
            credit: {
                identifier: 'credit',
                rules: [{
                        type: 'checked',
                        prompt: 'Veuillez répondre à cette question'
                    }]
            }

        },
        onSuccess: function (event, fields) {
            // alert("validé");
            //$.tab('change tab', 'liste');
            event.preventDefault();
            var responsablesDTO = [];
            var equipementsDTO = [];
            //var typeDemande = $("[name=projet]:checked").val();
            var typeDemande = "INDIVIDUEL";
            var formeJuridique = $("#forme").val();
            var id = $("#idDemande").val();
            var nomProjet = $("#nomprojet").val(); 
            var adequationQA = $('input[name=qualification]:checked').val();
            var adequationCA = $('input[name=competence]:checked').val();
            var numero = $("#numeroDemande").val();
            var ninea = $("#nineai").val();
            var rccm = $("#rccmi").val();
            var affliation = $("#affiliationi").val();
            var description = $("#res_projet").val();
            var montant = $("#amount").val();
            var cni = $("#cni").val();
            var secteurId = $("#dom_act").val();
            var regionId = $("#region_indiv").val();
            var departementId = $("#departement_indiv").val();
            var communeId = $("#commune_indiv").val();
            var activites = $("#p_activity").val();
            var produitsServices = $("#p_services").val();
            var nombreHomme = $("#nhomme").val();
            var nombreFemme = $("#nfemme").val();
            var compteIf = $("input[name=compte]:checked").val() === 'oui' ? true:false;
            var creditIf = $("input[name=credit]:checked").val() === 'oui' ? true:false;
            var financement = $("input[name=beneficiare]:checked").val() === 'oui' ? true:false;
            var prospects = $("input[name=prospect]:checked").val() === 'oui' ? true:false;
            var clients = $("input[name=client]:checked").val() === 'oui' ? true:false;
            for (var i = 0; i < 1; i++) {
                var nom = $("#nomi").val();
                var prenom = $("#prenomi").val();
                var sexe = $("#sexe").val();
                var fonction = "fonction";
                var telephone = $("#tel1").val();
                var dateNaissance = $("#date-naiss").val();
                var email = $("#email").val();
                var siteweb = $("#siteweb").val();
                var telephone2 = $("#tel2").val();
                var telephoneProche = $("#telp").val();
                var addrese = $("#adressei").val();
                var lieuNaissance = $("#lieu").val();
                var experience = $("#nbre_an").val();
                var qualification = 1;
                responsablesDTO.push(new ResponsableDTO(null, nom, prenom, sexe, cni, fonction, dateNaissance, qualification, experience, addrese, telephone, lieuNaissance,telephone2,telephoneProche,email,siteweb));
            }

            for (var i = 0; i <= rowCount; i++) {
                var type = $("#ityp" + i).val();
                var quantite = $("#iqt" + i).val();
                var prixUnitaire = $("#ipnt" + i).val();
                var fournisseur = $("#fourn" + i).val();
                if (type !== undefined && quantite !== undefined && prixUnitaire !== undefined && fournisseur !== undefined) {
                    equipementsDTO.push(new EquipementDTO(null, type, quantite, prixUnitaire, fournisseur));
                }

            }
            var data = {
                "id": id,
                "numero": numero,
                "montant": montant,
                "adequationQA":adequationQA,
                "adequationCA":adequationCA,
                "typeDemande": typeDemande,
                "nomProjet":nomProjet,
                "formeJuridique": formeJuridique,
                "ninea": ninea,
                "rccm": rccm,
                "affliation": affliation,
                "description": description,
                "secteurId": secteurId,
                "regionId": regionId,
                "departementId": departementId,
                "communeId": communeId,
                "responsablesDTO": responsablesDTO,
                "equipementsDTO": equipementsDTO,
                "activites": activites,
                "produitsServices": produitsServices,
                "prospects": prospects,
                "clients": clients,
                "nombreHomme": nombreHomme,
                "nombreFemme": nombreFemme,
                "compteIf": compteIf,
                "creditIf": creditIf,
                "financement": financement
            };
              //console.log(data);
              //console.log(compteIf);
              //console.log(creditIf);
              //console.log(financement);
            if (id === undefined) {
                CustomSendAjaxRequest("POST", "api/demandes", data);
            } else {
                CustomSendAjaxRequest("PUT", "api/demandes", data);
            }
        },
        onFailure: function (event, formErrors, fields) {
            event.preventDefault();
        }
    });

    $("#valid").click(function () {
        $('.ui.myform5').form('validate myform5');
    });

    /* année experience */
    $(".afcach").on("change", function () {
        var an = $(this).dropdown('get value');
        if (an == 'sans_experience') {
            $('.experience').addClass('hide').removeClass('show');
        } else {
            $('.experience').addClass('show').removeClass('hide');
        }
    });

    /* show and hide compte Relations if */
    $('.affcachcompt').click(function () {
        var resp = $("[name=compte]:checked").val();
        if (resp == 'oui') {
            $('.intitut-fin').addClass('show').removeClass('hide');
        } else {
            $('.intitut-fin').addClass('hide').removeClass('show');

        }
    });

    /* show and hide credit Relations if */
    $('.affcachcred').click(function () {
        var resp = $("[name=credit]:checked").val();
        if (resp == 'oui') {
            $('.creditif').addClass('show').removeClass('hide');
            /* $('.bankef').attr("name","bankef"); */
        } else {
            $('.creditif').addClass('hide').removeClass('show');
            /*  $('.bankef').attr("name","");
             $('#err-mess-relif').val(""); */

        }
    });

    /* show and hide benef Relations if */
    $('.benefinanc').click(function () {
        var resp = $("[name=beneficiare]:checked").val();
        if (resp == 'oui') {
            $('.infobenef').addClass('show').removeClass('hide');
        } else {
            $('.infobenef').addClass('hide').removeClass('show');
        }

    });

    var rowCount = 0;
    /* Choice between amount or equi */
    $("#choix").change(function () {
        if ($("#choix").dropdown('get value') == 'fond-roulement') {
            $('.fond-rl').addClass('show').removeClass('hide');
            $('.equipement').addClass('hide').removeClass('show');
            $("#fond-roulm").html($("#amount").val());
        } else if ($("#choix").dropdown('get value') == 'equipement') {
            $('.equipement').addClass('show').removeClass('hide');
            $('.fond-rl').addClass('hide').removeClass('show');
            rowCount = 0;
            $('#addedRows').empty();
        }
    });


    /* choice between amount or equipment */
    function check_newfield() {
        if ($.trim($('.typ').val()) == '' && $.trim($('.qt').val()) == '' && $.trim($('.pnt').val()) == '' && $.trim($('.totl').val()) == '') {
            return false;
        } else {
            return true;
        }
    }
    function get_amount_total(n) {
        var amount_total = 0;
        var check_amount = true;
        for (var i = 1; i <= n; i++) {
            amount_total += parseFloat($('#itotl' + i).val() * 1);
        }
        if (amount_total <= $("#amount").val()) {
            check_amount = true;
        } else {
            check_amount = false;
        }
        return check_amount;
    }


    $('#addMoreRows').click(function (e) {
        e.preventDefault();
        if (get_amount_total(rowCount)) {
            rowCount++;
            var new_equip = '<div class="ui inverted segment" id="' + rowCount + '" style="padding: 0.5em 1em; background: #d4d4d5;"> <div class="ui inverted form"> <div class="six fields"> <div class="field"> <label>Type<i class="asteris">*</i></label> <input type="text" class="typ" id="ityp' + rowCount + '" name="type"> </div> <div class="field"> <label>Prix unitaire<i class="asteris">*</i></label> <input type="number" class="pnt" id="ipnt' + rowCount + '" name="prix-unit"> </div> <div class="field"> <label>Quantité<i class="asteris">*</i></label> <input type="number" class="qt" id="iqt' + rowCount + '" name="quantite"> </div> <div class="field"> <label>Total</label> <input type="text" class="totl" id="itotl' + rowCount + '" disabled="disabled" readonly="readonly" name="total"> </div> <div class="field"> <label>Fournisseur</label> <input id="fourn' + rowCount + '" type="text" name=""> </div> <div class="field"> <label style="padding-top:19px;"></label> <button class="ui redli button" onclick="$(' + "'#" + rowCount + "'" + ').remove()"> <i class="icon cancel"></i> Supprimer </button> </div> </div> </div> </div>';
            $('#addedRows').append(new_equip);
        } else {
            //swal("Attention!", "Les "+ $('#amount').val() +" Fcfa ne doivent pas être depassés", "error");
            alert("Les " + $('#amount').val() + " Fcfa ne doivent pas être depassés");
        }

        function get_newtotal() {
            var som = 0;
            som = $('#ipnt1').val() * $('#iqt1').val();
            $('#itotl1').val(parseFloat(som));
        }
        $('#iqt1').keyup(function () {
            if (check_newfield()) {
                get_newtotal();
            } else {
                $('#iqt1').focus();
                $('#itotl1').val(0);
            }
        });
        $('#ipnt1').keyup(function () {
            if (check_newfield()) {
                get_newtotal();
            } else {
                $('#ipnt1').focus();
                $('#itotl1').val(0);
            }
        });

        $('#ipnt1').blur(function () {
            get_newtotal();
        });

        function get_newtot(rowCount) {
            var som = 0;
            som = $('#ipnt' + rowCount).val() * $('#iqt' + rowCount).val();
            $('#itotl' + rowCount).val(parseFloat(som));
        }
        $('#iqt' + rowCount).keyup(function () {
            if (check_newfield()) {
                get_newtot(rowCount);
            } else {
                $('#iqt' + rowCount).focus();
                $('#itotl' + rowCount).val(0);
            }
        });
        $('#ipnt' + rowCount).keyup(function () {
            if (check_newfield()) {
                get_newtot(rowCount);
            } else {
                $('#ipnt' + rowCount).focus();
                $('#itotl' + rowCount).val(0);
            }
        });

        $('#ipnt' + rowCount).blur(function () {
            get_newtot(rowCount);
        });


    });

    /******* choice between amount or equipment *******/

    /* calcul total nbre de femmes et hommes */

    function getTotal() {
        var nbtotal;
        nbtotal = parseInt($('#nfemme').val()) + parseInt($('#nhomme').val());
        $('#ntotal').val(parseInt(nbtotal));
    }
    function checkfieldnbre() {
        if ($('#nfemme').val() != "" && $('#nhomme').val() != "") {
            if ($('#nfemme').val() != 0 && $('#nhomme').val() != 0) {
                return true;
            }
        } else {
            return false;
        }
    }
    $('#nfemme').keyup(function () {
        if (checkfieldnbre()) {
            getTotal();
        } else {
            $('#nfemme').focus();
            $('#ntotal').val(0);
        }
    });
    $('#nhomme').keyup(function () {
        if (checkfieldnbre()) {
            getTotal();
        } else {
            $('#nhomme').focus();
            $('#ntotal').val(0);
        }
    });

    $('#nhomme').blur(function () {
        getTotal();
    });


    /* get age */

    $('#date-naiss').change(function () {
        var Bdate = document.getElementById('date-naiss').value;
        var Bday = new Date(Bdate);
        var Q4A = ~~((Date.now() - Bday) / (31557600000));
        $("#age").text(Q4A + " ans");
    });

    $("#sexe").on("change", function () {
        var startcni = $(this).dropdown('get value');
        if (startcni == "HOMME") {
            $("#start-cin").text(1);
        } else {
            $("#start-cin").text(2);
        }
    });

    /* Previous */
    $("#previous1").click(function (e) {
        e.preventDefault();
        location.hash = "demande/type-projet";
        $.tab('change tab', 'demande/type-projet');
    });
    $("#previous2").click(function (e) {
        e.preventDefault();
        location.hash = "demande/info-demandeur";
        $.tab('change tab', 'demande/info-demandeur');
    });
    $("#previous3").click(function (e) {
        e.preventDefault();
        location.hash = "demande/info-sprojet";
        $.tab('change tab', 'demande/info-sprojet');
    });
    $("#previous4").click(function (e) {
        e.preventDefault();
        location.hash = "demande/info-dprojet";
        $.tab('change tab', 'demande/info-dprojet');
    });

    /* input file js */
    $('input:text, .ui.button', '.ui.action.input')
            .on('click', function (e) {
                $('input:file', $(e.target).parents()).click();
            });

    $('input:file', '.ui.action.input')
            .on('change', function (e) {
                var name = e.target.files[0].name;
                $('input:text', $(e.target).parent()).val(name);
            });

    $("#amount").on("keyup", function () {
        if ($(this).val() <= 500000) {
            $('#business_plan, #btbp').attr('disabled', 'disabled');
            $('#inputfile').attr("name", "");
        } else {
            $('#business_plan, #btbp').removeAttr('disabled');
            $('#inputfile').attr("name", "bp");
        }
    });

    function ResponsableDTO(id, nom, prenom, sexe, cni, fonction, dateNaissance, qualification, experience, a, tel, lnais,tel2,tel3,email,sw) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sexe;
        this.cni = cni;
        this.fonction = fonction;
        this.dateNaissance = dateNaissance;
        this.qualification = qualification;
        this.experience = experience;
        this.adresse = a;
        this.telephone = tel;
        this.lieuNaissance = lnais;
        this.telephone2 = tel2;
        this.telephoneProche=tel3;
        this.email = email;
        this.siteWeb = sw;
    }

    function EquipementDTO(id, type, quantite, prixUnitaire, fournisseur) {
        this.id = id;
        this.type = type;
        this.quantite = quantite;
        this.prixUnitaire = prixUnitaire;
        this.fournisseur = fournisseur;
    }

    function CustomSendAjaxRequest(type, url, data) {
        var contextPath = $("meta[name='ctx']").attr("content");
        $.ajax({
            type: type,
            url: contextPath + "/" + url,
            contentType: 'application/json; charset=utf-8',
            timeout: 10000,
            data: JSON.stringify(data),
            dataType: 'json',
            beforeSend: function () {
                showLoadingModal();

            },
            success: function (data, textStatus, xhr) {
                if (xhr.status === 200 || xhr.status === 201) {
                    $.notify(xhr.getResponseHeader('X-lubam-alert'), "success");
                }

                setTimeout(function () {
                    window.location.replace(contextPath + '/demande/add');
                }, 1500);
                hideLoadingModal();
            },
            error: function (data) {
                hideLoadingModal();
                $.notify(data.responseJSON.message, "warn");
            }
        });
    }


});


