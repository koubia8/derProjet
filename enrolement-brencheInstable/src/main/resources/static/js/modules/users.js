
$('.ui.formuser').form({
           inline: true,
           on: 'blur',
           fields: {
               nomu: {
                   identifier: 'nomu',
                   rules: [{
                       type: 'empty',
                       prompt: 'Veuillez donner le nom'
                   }]
               },
               prenomu: {
                   identifier: 'prenomu',
                   rules: [{
                       type: 'empty',
                       prompt: 'Veuillez donner le prénom'
                   }]
               },
               email: {
                identifier: 'email',
                rules: [{
                    type   : 'email',
                    prompt : 'Veuillez donner une adresse mail valide'
                    }]
               },
               username: {
                   identifier: 'username',
                   rules: [{
                       type: 'empty',
                       prompt: 'Veuillez donner le nom d\'utilisateur'
                   }]
               },
               telu: {
                       identifier: 'telu',
                       rules: [{
                           type: 'empty',
                           prompt: 'Veuillez donner votre numéro de téléphone'
                       }, {
                           type: 'minLength[9]',
                           prompt: 'Le numéro de téléphone ne doit pas dépasser {ruleValue} chiffres'
                       }]
                   },
               patneru: {
                   identifier: 'patneru',
                   rules: [{
                       type: 'empty',
                       prompt: 'Selectionner un partenaire'
                   }]
               },
               role: {
                   identifier: 'role',
                   rules: [{
                       type: 'empty',
                       prompt: 'Donnez un rôle à l\'utilisateur'
                   }]
               }
             },
             onSuccess: function(event){
                    event.preventDefault();
                    event.stopImmediatePropagation();
                    var nom = $("#nomUser").val();
                    var prenom = $("#prenomUser").val();
                    var telephone = $("#telephoneUser").val();
                    var email = $("#emailUser").val();
                    var login = $("#loginUser").val();
                    var role = $("#authoritiesUser").val();
                    var activated = $('#userActivated').is(':checked') ? "true":"false";
                    var partenaireId = $("#partenaireIdUser").val();
                    var authorities = [];
                    authorities.push(role);
                    var data = {
                        "id":null,
                        "partenaireId":partenaireId,
                        "nom": nom,
                        "prenom":prenom,
                        "telephone":telephone,
                        "authorities":authorities,
                        "activated": activated,
                        "login":login,
                        "email":email
                    };
                    //console.log(data);
                    sendAjaxRequest("POST", "api/users", data);
               
            },
             onFailure: function(event){
                 event.preventDefault();
                   $.uiAlert({
                   textHead: "Erreur lors de la saisie", // header
                   text: 'Veuillez bien remplir le formulaire', // Text
                   bgcolor: '#DB2828', // background-color
                   textcolor: '#fff', // color
                   position: 'top-right',// position . top And bottom ||  left / center / right
                   icon: 'remove circle', // icon in semantic-UI
                   time: 3, // time
                 });
             }
        });

        $("#saveuser").click(function(){
            $('.ui.formuser').form('validate formuser');
        });




$(document).on('click', '.btnGetUser', function (e) {
    var id = $(this).attr("data-id");
    e.preventDefault();
    $.ajax({
        url: contextPath + '/api/users/'+id,
        type: 'GET',
        data: null,
        beforeSend: function () {

        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                $('#idUserEdit').val(data.id);
                $('#nomUserEdit').val(data.nom);
                $('#loginUserEdit').val(data.login);
                $('#prenomUserEdit').val(data.prenom);
                $('#telephoneUserEdit').val(data.telephone);
                $('#emailUserEdit').val(data.email);
                $('#userActivatedEdit').prop('checked', data.activated);
                $('#partenaireIdUserEdit option[value="' + data.partenaireId + '"]').prop('selected', true);
                $('#authoritiesUserEdit option[value="' + data.authorities[0] + '"]').prop('selected', true);
                
                $('#modalAddUserEdit').modal("show");
            } 
        },
        error: function () {

        }
    });
});


$(document).on('submit', '#formUserEdit', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $("#idUserEdit").val();
    var nom = $("#nomUserEdit").val();
    var prenom = $("#prenomUserEdit").val();
    var telephone = $("#telephoneUserEdit").val();
    var email = $("#emailUserEdit").val();
    var login = $("#loginUserEdit").val();
    var role = $("#authoritiesUserEdit").val();
    var activated = $('#userActivatedEdit').is(':checked') ? "true":"false";
    var authorities = [];
    var partenaireId = $("#partenaireIdUserEdit").val();
    authorities.push(role);
    var data = {
        "id":id,
        "partenaireId":partenaireId,
        "nom": nom,
        "prenom":prenom,
        "telephone":telephone,
        "authorities":authorities,
        "activated": activated,
        "login":login,
        "email":email
    };
    //console.log(data);
    sendAjaxRequest("PUT", "api/users", data);
});


$(document).on('click', '.btnEnableUser', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    sendAjaxRequestDELETE("DELETE", "api/users/enable/"+id);
});

$(document).on('click', '.btnDisableUser', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    sendAjaxRequestDELETE("DELETE", "api/users/disable/"+id);
});


$(document).on('click', '.btnDeleteUser', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    
    if (confirm('Voulez vous vraiment effectuer cette action?')) {
        sendAjaxRequestDELETE("DELETE", "api/users/"+id);
    }
});


$(document).on('change', '.EnableOrDisableUser', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
  
    var id = $(this).attr("data-id");
    var choix = $(this).is(":checked");
    if(choix){
       sendAjaxRequestDELETE("DELETE", "api/users/enable/"+id); 
      
    }
    else{
        sendAjaxRequestDELETE("DELETE", "api/users/disable/"+id);
       
    }
    
});



$(document).on('click', '.btnGetUser', function (e) {
    var id = $(this).attr("data-id");
    e.preventDefault();
    $.ajax({
        url: contextPath + '/api/users/'+id,
        type: 'GET',
        data: null,
        beforeSend: function () {

        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                $('#idUserEdit').val(data.id);
                $('#nomUserEdit').val(data.nom);
                $('#loginUserEdit').val(data.login);
                $('#prenomUserEdit').val(data.prenom);
                $('#telephoneUserEdit').val(data.telephone);
                $('#emailUserEdit').val(data.email);
                $('#userActivatedEdit').prop('checked', data.activated);
                $('#partenaireIdUserEdit option[value="' + data.partenaireId + '"]').prop('selected', true);
                $('#authoritiesUserEdit option[value="' + data.authorities[0] + '"]').prop('selected', true);
                
                $('#modalAddUserEdit').modal("show");
            } 
        },
        error: function () {

        }
    });
});


$('.ui.formuseredit').form({
           inline: true,
           on: 'blur',
           fields: {
               nomu: {
                   identifier: 'nomu',
                   rules: [{
                       type: 'empty',
                       prompt: 'Veuillez donner le nom'
                   }]
               },
               prenomu: {
                   identifier: 'prenomu',
                   rules: [{
                       type: 'empty',
                       prompt: 'Veuillez donner le prénom'
                   }]
               },
               email: {
                identifier: 'email',
                rules: [{
                    type   : 'email',
                    prompt : 'Veuillez donner une adresse mail valide'
                    }]
               },
               username: {
                   identifier: 'username',
                   rules: [{
                       type: 'empty',
                       prompt: 'Veuillez donner le nom d\'utilisateur'
                   }]
               },
               telu: {
                       identifier: 'telu',
                       rules: [{
                           type: 'empty',
                           prompt: 'Veuillez donner votre numéro de téléphone'
                       }, {
                           type: 'minLength[9]',
                           prompt: 'Le numéro de téléphone ne doit pas dépasser {ruleValue} chiffres'
                       }]
                   },
               patneru: {
                   identifier: 'patneru',
                   rules: [{
                       type: 'empty',
                       prompt: 'Selectionner un partenaire'
                   }]
               },
               role: {
                   identifier: 'role',
                   rules: [{
                       type: 'empty',
                       prompt: 'Donnez un rôle à l\'utilisateur'
                   }]
               }
             },
   onSuccess: function(event){
    event.preventDefault();
    event.stopImmediatePropagation();
    var id = $("#idUserEdit").val();
    var nom = $("#nomUserEdit").val();
    var prenom = $("#prenomUserEdit").val();
    var telephone = $("#telephoneUserEdit").val();
    var email = $("#emailUserEdit").val();
    var login = $("#loginUserEdit").val();
    var role = $("#authoritiesUserEdit").val();
    var activated = $('#userActivatedEdit').is(':checked') ? "true":"false";
    var authorities = [];
    var partenaireId = $("#partenaireIdUserEdit").val();
    authorities.push(role);
    var data = {
        "id":id,
        "partenaireId":partenaireId,
        "nom": nom,
        "prenom":prenom,
        "telephone":telephone,
        "authorities":authorities,
        "activated": activated,
        "login":login,
        "email":email
    };
    //console.log(data);
    sendAjaxRequestEdit("PUT", "api/users", data, "users/add");
 },
    onFailure: function(event){
        event.preventDefault();
          $.uiAlert({
          textHead: "Erreur lors de la saisie", // header
          text: 'Veuillez bien remplir le formulaire', // Text
          bgcolor: '#DB2828', // background-color
          textcolor: '#fff', // color
          position: 'top-right',// position . top And bottom ||  left / center / right
          icon: 'remove circle', // icon in semantic-UI
          time: 3, // time
        });
             }
        });

        $("#edituser").click(function(){
            $('.ui.formuseredit').form('validate formuseredit');
        });


$(document).on('click', '.btnEnableUser', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    sendAjaxRequestDELETE("DELETE", "api/users/enable/"+id);
});

$(document).on('click', '.btnDisableUser', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    sendAjaxRequestDELETE("DELETE", "api/users/disable/"+id);
});


$(document).on('click', '.btnDeleteUser', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    
    if (confirm('Voulez vous vraiment effectuer cette action?')) {
        sendAjaxRequestDELETE("DELETE", "api/users/"+id);
    }
});


$(document).on('change', '.EnableOrDisableUser', function (e) {
 var contextPath = $("meta[name='ctx']").attr("content");
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    var choix = $(this).is(":checked");
    if(choix){
       sendAjaxRequestDELETE("DELETE", "api/users/enable/"+id); 
       window.location.href=contextPath+"/"+users/add;
    }
    else{
        sendAjaxRequestDELETE("DELETE", "api/users/disable/"+id); 
       
    }

});