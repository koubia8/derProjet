$(document).on('click', '#valideCheckedDemande', function (e) {
    e.preventDefault();
    var ids = [];
    $.each($("input[name='optionSelected']:checked"), function () {
            ids.push($(this).val());
        });
     if(ids.length === 0){
        alert("Veuillez selectionner un element!", "", "error");
        return ;
     } 
     
    var data = {
        "action":0,
        "ids":ids
    }; 
    //console.log(data);
    sendAjaxRequest("PUT", "api/demandesupdatelist", data);
});


$(document).on('click', '#rejeteCheckedDemande', function (e) {
    e.preventDefault();
    var ids = [];
    $.each($("input[name='optionSelected']:checked"), function () {
            ids.push($(this).val());
        });
        
     if(ids.length === 0){
        alert("Veuillez selectionner un element!", "", "error");
        return ;
     }   
        
    var data = {
        "action":1,
        "ids":ids
    };
    //console.log(data);
    sendAjaxRequest("PUT", "api/demandesupdatelist", data);
});

$(document).on('click', '#restaureCheckedDemande', function (e) {
    e.preventDefault();
    var ids = [];
    $.each($("input[name='optionSelectedToRestaure']:checked"), function () {
            ids.push($(this).val());
        });
        
     if(ids.length === 0){
        alert("Veuillez selectionner un element!", "", "error");
        return ;
     }   
        
    var data = {
        "action":2,
        "ids":ids
    };
    //console.log(data);
    sendAjaxRequest("PUT", "api/demandesupdatelist", data);
});