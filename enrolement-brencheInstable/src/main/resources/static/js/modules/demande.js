var number = 1;
$(document).on('submit', '#wizard_with_validation', function (e) {
    e.preventDefault();
    var responsablesDTO = [];
    var equipementsDTO = [];
    var typeDemande = $("[name=t_project]:checked").val();
    var formeJuridique = $("#forme").val();
    var ninea = $("#nineai").val();
    var rccm = $("#rccmi").val();
    var affliation = $("#affiliationi").val();
    var description=$("#resume").val();
    var montant = $("#fond").val();
    var cni = $("#cni").val();
    var secteurId = $("#dom_act").val();
    var regionId = $("#region_indiv").val();
    var departementId = $("#departement_indiv").val();
    var communeId = $("#commune_indiv").val();
    for (var i = 0; i < number; i++) {
        var nom = $("#nomi").val();
        var prenom = $("#prenomi").val();
        var sexe = $("#sexei").val();
        var fonction = "fonction";
        var dateNaissance = $("#birthi").val();
        var experience = $("#nbre_an").val();
        var qualification=1;
        responsablesDTO.push(new ResponsableDTO(null, nom, prenom, sexe,cni,fonction, dateNaissance,qualification,experience,null,null,null));
    }
    
    for (var i = 0; i <= rowCount; i++) {
        var type = $("#itype"+i).val();
        var quantite = $("#iqt"+i).val();
        var prixUnitaire = $("#ipnt"+i).val();
        var fournisseur = $("#ifrn"+i).val();
        if(type !== undefined && quantite !== undefined && prixUnitaire !== undefined && fournisseur !== undefined){
            equipementsDTO.push(new EquipementDTO(null,type,quantite,prixUnitaire,fournisseur));
        }
        
    }
    
    var data = {
        "montant": montant,
        "typeDemande": typeDemande,
        "formeJuridique": formeJuridique,
        "ninea":ninea,
        "rccm":rccm,
        "affliation":affliation,
        "description": description,
        "secteurId": secteurId,
        "regionId": regionId,
        "departementId": departementId,
        "communeId": communeId,
        "responsablesDTO": responsablesDTO,
        "equipementsDTO": equipementsDTO
    };
    //console.log(data);
    sendAjaxRequest("POST", "api/demandes", data);
});


var numberC = 1;
$(document).on('submit', '#wizard_with_validation_c', function (e) {
    e.preventDefault();
    var responsablesDTO = [];
    var equipementsDTO = [];
    var typeDemande = $("[name=t_project]:checked").val();
    var formeJuridique = $("#forme").val();
    var ninea = $("#nineac").val();
    var rccm = $("#rccmc").val();
    var affliation = $("#affiliationc").val();
    var description=$("#resumec").val();
    var montant = $("#fond").val();
    var cni = $("#cni1").val();
    var secteurId = $("#dom_act").val();
    var regionId = $("#region_indiv").val();
    var departementId = $("#departement_indiv").val();
    var communeId = $("#commune_indiv").val();
    for (var i = 1; i <= numberC; i++) {
        var nom = $("#nomr"+i).val();
        var prenom = $("#prenomr"+i).val();
        var sexe = $("#sexer"+i).val();
        var fonction = $("#roler"+i).val();
        var dateNaissance = $("#date_naiss_resp"+i).val();
        var experience = $("#nbre_anc").val();
        var qualification=1;
        responsablesDTO.push(new ResponsableDTO(null, nom, prenom, sexe,cni,fonction, dateNaissance,qualification,experience,null,null,null));
    }
    
    for (var i = 0; i <= rowC; i++) {
        var type = $("#ctype"+i).val();
        var quantite = $("#cqt"+i).val();
        var prixUnitaire = $("#cpnt"+i).val();
        var fournisseur = $("#cfrn"+i).val();
        if(type !== undefined && quantite !== undefined && prixUnitaire !== undefined && fournisseur !== undefined){
            equipementsDTO.push(new EquipementDTO(null,type,quantite,prixUnitaire,fournisseur));
        }
        
    }
    
    var data = {
        "montant": montant,
        "typeDemande": typeDemande,
        "formeJuridique": formeJuridique,
        "ninea":ninea,
        "rccm":rccm,
        "affliation":affliation,
        "description": description,
        "secteurId": secteurId,
        "regionId": regionId,
        "departementId": departementId,
        "communeId": communeId,
        "responsablesDTO": responsablesDTO,
        "equipementsDTO": equipementsDTO
    };
    //console.log(data);
   sendAjaxRequest("POST", "api/demandes", data);
});



$(document).on('click', '.btnGetDemande', function (e) {
    var id = $(this).attr("data-id");
    e.preventDefault();
    $.ajax({
        url: contextPath + '/api/demandes/' + id,
        type: 'GET',
        data: null,
        beforeSend: function () {

        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                $('#idDemandeEdit').val(data.id);
                $('#ninea_indivEdit').val(data.ninea);
                $("#rccm_indivEdit").val(data.rccm);
                $("#affliation_indivEdit").val(data.affliation);
                $("#description_indivEdit").val(data.affliation);
                $('#formeJuridique_indivEdit option[value="' + data.formeJuridique + '"]').prop('selected', true);
                //$('#secteur_indivEdit option[value="' + data.secteurId + '"]').prop('selected', true);
                //$('#region_indivEdit option[value="' + data.regionId + '"]').prop('selected', true);
                //$('#departement_indivEdit option[value="' + data.departementId + '"]').prop('selected', true);
                //$('#commune_indivEdit option[value="' + data.communeId + '"]').prop('selected', true);

                $('#modalEditIndividuel').modal("show");
            } else {

            }
        },
        error: function () {

        }
    });
});


$(document).on('submit', '#formDemandeEdit', function (e) {
    e.preventDefault();
    var id = $("#idDemandeEdit").val();
    var formeJuridique = $("#formeJuridique_indivEdit").val();
    var ninea = $("#ninea_indivEdit").val();
    var rccm = $("#rccm_indivEdit").val();
    var affliation = $("#affliation_indivEdit").val();
    var description = $("#description_indivEdit").val();
   // var secteurId = $("#secteur_indivEdit").val();
    //var regionId = $("#region_indivEdit").val();
    //var departementId = $("#departement_indivEdit").val();
   // var communeId = $("#commune_indivEdit").val();
    var data = {
        "id":id,
        "formeJuridique": formeJuridique,
        "ninea": ninea,
        "description": description,
        "rccm": rccm,
        "affliation": affliation
    };
    //console.log(data);
    sendAjaxRequest("PUT", "api/demandes/update", data);
});



$(document).on('submit', '#formDemandeListToValide', function (e) {
    e.preventDefault();
    var ids = [];
    $.each($("input[name='optionSelected']:checked"), function () {
            ids.push($(this).val());
        });
     if(ids.length === 0){
        swal("Veuillez selectionner un element!", "", "error");
        return ;
     } 
     
    var data = {
        "action":true,
        "ids":ids
    };
    $("#modalConfirmation").modal("show");

    $(document).on('click', '#btnValiderConfirmation', function (e) {
        $("#modalConfirmation").modal("hide");
        sendAjaxRequest("PUT", "api/demandesupdatelist", data);
    });
    
});


$(document).on('click', '#rejeter_liste_demande', function (e) {
    e.preventDefault();
    var ids = [];
    $.each($("input[name='optionSelected']:checked"), function () {
            ids.push($(this).val());
        });
        
     if(ids.length === 0){
        swal("Veuillez selectionner un element!", "", "error");
        return ;
     }   
        
    var data = {
        "action":false,
        "ids":ids
    };
    //console.log(data);
    $("#modalConfirmation").modal("show");

    $(document).on('click', '#btnValiderConfirmation', function (e) {
        $("#modalConfirmation").modal("hide");
        sendAjaxRequest("PUT", "api/demandesupdatelist", data);
    });

});


$(document).on('click', '.btnDeleteDemande', function (e) {
    e.preventDefault();
    var id = $(this).attr("data-id");
    $("#modalConfirmation").modal("show");

    $(document).on('click', '#btnValiderConfirmation', function (e) {
        $("#modalConfirmation").modal("hide");
        sendAjaxRequestDELETE("DELETE", "api/demandes/" + id);
    });

});


$('#region_indiv').change(function () {
    var region = $('#region_indiv').val();
    $("#departement_indiv").html('');
    var contextPath = $("meta[name='ctx']").attr("content");
    $.ajax({
        type: 'GET',
        url: contextPath + "/" + 'departement/' + region,
        contentType: 'application/json; charset=utf-8',
        timeout: 10000,
        data: null,
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data, textStatus, xhr) {
            var option = "<option value=''>Departement</option>";
            $("#departement_indiv").append(option);
            for (var i = 0; i < data.length; i++) {
                var option = "<option value=" + data[i].id + ">" + data[i].nom + "</option>";
                $("#departement_indiv").append(option);
            }
        },
        error: function () {

        }
    });
});


$('#region_indivEdit').change(function () {
    $("#departement_indivEdit").html('');
    var region = $('#region_indivEdit').val();
    var contextPath = $("meta[name='ctx']").attr("content");
    $.ajax({
        type: 'GET',
        url: contextPath + "/" + 'departement/' + region,
        contentType: 'application/json; charset=utf-8',
        timeout: 10000,
        data: null,
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data, textStatus, xhr) {
            var option = "<option value='' selected='selected'>Département</option>";
            $("#departement_indivEdit").append(option);
            for (var i = 0; i < data.length; i++) {
                var option = "<option value=" + data[i].id + ">" + data[i].nom + "</option>";
                $("#departement_indivEdit").append(option);
            }
        },
        error: function () {

        }
    });
});



$('#departement_indiv').change(function () {
    $("#commune_indiv").html('');
    var departement = $('#departement_indiv').val();
    var contextPath = $("meta[name='ctx']").attr("content");
    $.ajax({
        type: 'GET',
        url: contextPath + "/" + 'commune/' + departement,
        contentType: 'application/json; charset=utf-8',
        timeout: 10000,
        data: null,
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data, textStatus, xhr) {
            var option = "<option value='' selected='selected'>Commune</option>";
            $("#commune_indiv").append(option);
            for (var i = 0; i < data.length; i++) {
                var option = "<option value=" + data[i].id + ">" + data[i].nom + "</option>";
                $("#commune_indiv").append(option);
            }
        },
        error: function () {

        }
    });
});

$('#departement_indivEdit').change(function () {
    $("#commune_indivEdit").html('');
    var departement = $('#departement_indivEdit').val();
    var contextPath = $("meta[name='ctx']").attr("content");
    $.ajax({
        type: 'GET',
        url: contextPath + "/" + 'commune/' + departement,
        contentType: 'application/json; charset=utf-8',
        timeout: 10000,
        data: null,
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data, textStatus, xhr) {
            var option = "<option value=''>Commune</option>";
            $("#commune_indiv").append(option);
            for (var i = 0; i < data.length; i++) {
                var option = "<option value=" + data[i].id + ">" + data[i].nom + "</option>";
                $("#commune_indivEdit").append(option);
            }
        },
        error: function () {

        }
    });
});

//
//$('#dom_act').change(function () {
//    $("#soussecteur_indiv").html('');
//    var secteurId = $('#dom_act').val();
//    var contextPath = $("meta[name='ctx']").attr("content");
//    $.ajax({
//        type: 'GET',
//        url: contextPath + "/api/" + 'getSousSecteursBySecteur/' + secteurId,
//        contentType: 'application/json; charset=utf-8',
//        timeout: 10000,
//        data: null,
//        dataType: 'json',
//        beforeSend: function () {
//        },
//        success: function (data, textStatus, xhr) {
//            var option = "<option selected='selected' value=''>Sous-secteurs d'activités</option>";
//            $("#soussecteur_indiv").append(option);
//            for (var i = 0; i < data.length; i++) {
//                var option = "<option value=" + data[i].id + ">" + data[i].nom + "</option>";
//                $("#soussecteur_indiv").append(option);
//            }
//        },
//        error: function () {
//
//        }
//    });
//});
//


function ResponsableDTO(id, nom, prenom, sexe,cni,fonction, dateNaissance,qualification,experience,a,tel,lnais) {
    this.id = id;
    this.nom = nom;
    this.prenom = prenom;
    this.sexe = sexe;
    this.cni = cni;
    this.fonction = fonction;
    this.dateNaissance = dateNaissance;;
    this.qualification = qualification;
    this.experience = experience;
    this.adresse=a;
    this.telephone=tel;
    this.lieuNaissance=lnais;
}


function EquipementDTO(id, type, quantite, prixUnitaire,fournisseur) {
    this.id = id;
    this.type = type;
    this.quantite = quantite;
    this.prixUnitaire = prixUnitaire;
    this.fournisseur = fournisseur;
}


function collectFormData(fields) {
    var formData = new FormData();

    for (var i = 0; i < fields.length; i++) {
        var $item = $(fields[i]);

        if ($item.attr('type') =="file"){
            var file = $('input[type=file]')[0].files[0];
            formData.append( $item.attr('name') , file);

        } else {
            formData.append( $item.attr('name') , $item.val());
        }
    }
    return formData;
}


$(document).on('submit', '#formUploadFile', function (e) {
    e.preventDefault();
    
    var fields = form.find('input, textarea, select');
    var formData = collectFormData(fields);
    
    //var form = $('#formUploadFile')[0];
    //var data = 
    //data.append("numero","numero");
   // data.append("demande", null);
//    var data = {
//        "demande":null,
//        "businessPlan" :new FormData(form)
//    };
   
    console.log(formData);
    $("#submitButton").prop("disabled", true);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: contextPath + "/api/demandes/businessplan",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 1000000,
        success: function (data, textStatus, jqXHR) {

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
});


$('#new_responsable').click(function () {
    var recRow = '<div id="responsable' + number + '">\n\
<div class="row clearfix"><div><div class="form-group form-float">\n\
<div><input type="text" class="form-control" id="nom' + number + '" placeholder="nom"></div></div></div>\n\
<div><div class="form-group form-float"><div class="form-line"><input type="text" class="form-control" id="prenom' + number + '" placeholder="prenom"></div></div></div>\n\
<div><div class="form-group form-float"><div class="form-line"> <select id="sexe' + number + '">\n\
<option value="FEMME">Femme</option>\n\
<option value="HOMME">Homme</option>\n\
</select> </div></div></div>\n\
<div><div class="form-group form-float"><div class="form-line"><input type="text" class="form-control" id="fonction' + number + '" placeholder="fonction"></div></div></div>\n\
<div><div class="form-group form-float"><div class="form-line"><input type="text" class="form-control" id="dateNaissance' + number + '" placeholder="date naissance"></div></div></div>\n\
<div><button type="button" onclick="$(' + "'#responsable" + number + "'" + ').remove()" class="btn btn-info waves-effect"><i class="material-icons">remove</i></button></div></div></div>';
    $('#add_responsable').append(recRow);
    number++;
});