$(document).on('submit', '#formEquipement', function (e) {
    e.preventDefault();
    var type = $("#nomEquipement").val();
    var quantite = $("#prenomEquipement").val();
    var prixUnitaire = $("#sexeEquipement").val();
    var fournisseur = $("#dateNaissanceEquipement").val();
    var demandeId = $("#equipementDemandeId").val();
    var data = {
        "demandeId": demandeId,
        "type": type,
        "quantite": quantite,
        "prixUnitaire": prixUnitaire,
        "fournisseur":fournisseur
    };
    //console.log(data);
    sendAjaxRequest("POST", "api/equipements", data);
});

$(document).on('click', '.btnGetEquipement', function (e) {
    var id = $(this).attr("data-id");
    e.preventDefault();
    $.ajax({
        url: contextPath + '/api/equipements/' + id,
        type: 'GET',
        data: null,
        beforeSend: function () {

        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                $('#idEquipementEdit').val(data.id);
                $('#nomEquipementEdit').val(data.nom);
                $('#prenomEquipementEdit').val(data.prenom);
                $('#cniEquipementEdit').val(data.cni);
                $('#fonctionEquipementEdit').val(data.fonction);
                $('#dateNaissanceEquipementEdit').val(data.dateNaissance);
                $('#sexeEquipementEdit option[value="' + data.sexe + '"]').prop('selected', true);
                
                $('#modalEditEquipement').modal("show");
            } else {

            }
        },
        error: function () {

        }
    });
});


$(document).on('submit', '#formEquipementEdit', function (e) {
    e.preventDefault();
    var demandeId = $("#equipementDemandeId").val();
    var id = $("#idEquipementEdit").val();
    var nom = $("#nomEquipementEdit").val();
    var prenom = $("#prenomEquipementEdit").val();
    var sexe = $("#sexeEquipementEdit").val();
    var dateNaissance = $("#dateNaissanceEquipementEdit").val();
    var cni = $("#cniEquipementEdit").val();
    var fonction= $("#fonctionEquipementEdit").val();
    var data = {
        "demandeId": demandeId,
        "id":id,
        "nom": nom,
        "prenom": prenom,
        "sexe": sexe,
        "cni":cni,
        "fonction":fonction,
        "dateNaissance": dateNaissance
    };
    sendAjaxRequest("PUT", "api/equipements", data);
});


$(document).on('click', '.btnDeleteEquipement', function (e) {
    e.preventDefault();
    var id = $(this).attr("data-id");
    $("#modalConfirmation").modal("show");

    $(document).on('click', '#btnValiderConfirmation', function (e) {
        $("#modalConfirmation").modal("hide");
        sendAjaxRequestDELETE("DELETE", "api/equipements/" + id);
    });

});
