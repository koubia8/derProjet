$(document).on('submit', '#formGuichet', function (e) {
    e.preventDefault();
    var nom = $("#nomGuichet").val();
    var max = $("#maxGuichet").val();
    var data = {
        "nom": nom,
        "max":max
    };
    //console.log(data);
    sendAjaxRequest("POST", "api/guichets", data);
    //window.location.replace("guichet/list");
});

$(document).on('click', '.btnGetGuichet', function (e) {
    var id = $(this).attr("data-id");
    e.preventDefault();
    $.ajax({
        url: contextPath + '/api/guichets/' + id,
        type: 'GET',
        data: null,
        beforeSend: function () {

        },
        success: function (data, textStatus, xhr) {
            if (xhr.status === 200) {
                $('#idGuichetEdit').val(data.id);
                $('#maxGuichetEdit').val(data.max);
                $('#nomGuichetEdit option[value="' + data.nom + '"]').prop('selected', true);
                
                $('#modalEditGuichet').modal("show");
            } 
        },
        error: function () {

        }
    });
});


$(document).on('submit', '#formGuichetEdit', function (e) {
    e.preventDefault();
    var id = $("#idGuichetEdit").val();
    var nom = $("#nomGuichetEdit").val();
    var max = $("#maxGuichetEdit").val();
    var data = {
        "id": id,
        "nom": nom,
        "max": max
    };
    sendAjaxRequest("PUT", "api/guichets", data);
});


$(document).on('click', '.btnDeleteGuichet', function (e) {
    e.preventDefault();
    var id = $(this).attr("data-id");
    $("#modalConfirmation").modal("show");

    $(document).on('click', '#btnValiderConfirmation', function (e) {
        $("#modalConfirmation").modal("hide");
        sendAjaxRequestDELETE("DELETE", "api/guichets/"+id);
    });

});

$(document).on('change', '.EnableOrDisableGuichet', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var id = $(this).attr("data-id");
    var choix = $(this).is(":checked");
    if(choix){
        console.log(id);
      sendAjaxRequestDELETE("DELETE", "api/guichet/active/"+id); 
    }
    else{
        console.log(id);
         sendAjaxRequestDELETE("DELETE", "api/guichet/desactive/"+id); 
    }
    
});


 

