CREATE VIEW view_sd_region AS
SELECT `reg`.`nom` AS `region`,`d`.`region_id` AS `region_id`,COUNT(0) AS `demandes`,sum((`d`.`etat` = 'VALIDE')) AS `beneficiaires`,SUM((`d`.`etat` = 'REJETE')) AS `nonbeneficiaires` 
FROM (`websiteu_derdb`.`region` `reg` join `websiteu_derdb`.`demande` `d`) 
WHERE (`d`.`region_id` = `reg`.`id`) GROUP BY `d`.`region_id`;