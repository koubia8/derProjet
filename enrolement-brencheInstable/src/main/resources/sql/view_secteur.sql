CREATE VIEW view_diagramme AS
SELECT `s`.`nom_secteur` AS `secteur`, COUNT(0) AS `total`
FROM (`websiteu_derdb`.`secteur` `s` join `websiteu_derdb`.`demande` `d`) 
WHERE (`s`.`id` = `d`.`secteur_id`) GROUP BY `d`.`secteur_id`;