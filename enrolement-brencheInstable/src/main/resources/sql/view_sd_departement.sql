CREATE VIEW view_sd_departement AS
SELECT `dep`.`region_id` AS `region_id`,`dep`.`nom` AS `departement`,`d`.`departement_id` AS `departement_id`,COUNT(0) AS `demandes`,sum((`d`.`etat` = 'VALIDE')) AS `beneficiaires`,SUM((`d`.`etat` = 'REJETE')) AS `nonbeneficiaires` 
FROM (`websiteu_derdb`.`departement` `dep` join `websiteu_derdb`.`demande` `d`) 
WHERE (`d`.`departement_id` = `dep`.`id`) GROUP BY `d`.`departement_id`;