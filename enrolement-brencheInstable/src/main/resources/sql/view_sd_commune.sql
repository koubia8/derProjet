CREATE VIEW view_sd_commune AS
SELECT `com`.`departement_id` AS `departement_id`,`com`.`nom` AS `commune`,`d`.`commune_id` AS `commune_id`,COUNT(0) AS `demandes`,sum((`d`.`etat` = 'VALIDE')) AS `beneficiaires`,SUM((`d`.`etat` = 'REJETE')) AS `nonbeneficiaires` 
FROM (`websiteu_derdb`.`commune` `com` join `websiteu_derdb`.`demande` `d`) 
WHERE (`d`.`commune_id` = `com`.`id`) GROUP BY `d`.`commune_id`;