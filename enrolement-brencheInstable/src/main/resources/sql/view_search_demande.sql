CREATE VIEW view_search_demande AS
SELECT `d`.`id` AS `id`, `d`.`created_date` AS `date`,`d`.`numero` AS `numero`,`d`.`montant` AS `montant`,`d`.`etat` AS `etat`,`r`.`nom` AS `nom`,`r`.`prenom` AS `prenom`,`r`.`sexe` AS `sexe`,`r`.`telephone` AS `telephone`,`r`.`cni` AS `cni`
FROM (`websiteu_derdb`.`responsable` `r` join `websiteu_derdb`.`demande` `d`) 
WHERE (`r`.`demande_id` = `d`.`id`)