/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.constantes;

/**
 *
 * @author guisse
 */
public final class EtatDemande {
    public static final String EN_COURS = "EN COURS";
    
    public static final String REJETE = "REJETE";
    
    public static final String VALIDE = "VALIDE";
}
