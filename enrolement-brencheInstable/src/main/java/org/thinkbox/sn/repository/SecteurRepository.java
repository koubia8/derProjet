package org.thinkbox.sn.repository;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.thinkbox.sn.domain.Secteur;

/**
 * Spring Data JPA repository for the Secteur entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SecteurRepository extends JpaRepository<Secteur, Long> {

    @Query("select secteur from Secteur secteur left join fetch secteur.sousSecteurs where secteur.id =:id")
    public Secteur findOneWithSousSecteurById(@Param("id") Long id);
    
    //@Query("select secteur from Secteur secteur left join fetch secteur.sousSecteurs.active where secteur.id =:id")
    
    //public Secteur findOneWithSousSecteurByEtat(@Param("id") Long id);
    
    public Page<Secteur> findByActivated(Boolean activeted, Pageable pageable);
    Optional<Secteur> findOneByNomIgnoreCase(String nom);
    

    public Secteur findByNom(String nom);

}
