/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.thinkbox.sn.domain.Demande;

/**
 *
 * @author guisse
 */
@Repository
public interface DemandeRepository extends JpaRepository<Demande, Long>{
    
    public Demande findByNumero(String numero);
    
    @Query("SELECT COUNT(*) FROM Demande demande WHERE demande.etat=:etat")
    public Long countAllDemandeByStatus(@Param("etat") String etat);
    
    
    @Query("SELECT COUNT(*) FROM Demande demande WHERE demande.departement.id=:id AND demande.etat=:etat")
    public Long countALlByStarus(@Param("id") Long id,@Param("etat") String etat);
    
    
    @Query("SELECT COUNT(*) FROM Demande demande WHERE demande.departement.id=:id")
    public Long countALlByDepartement(@Param("id") Long id);
    
    
    @Query("SELECT demande FROM Demande demande WHERE demande.departement.id=:id")
    public List<Demande> findDemandeByDepartement(@Param("id") Long id);
    
    @Query("SELECT demande FROM Demande demande WHERE demande.commune.id=:id")
    public List<Demande> findDemandeByCommune(@Param("id") Long id);
    
    @Query("SELECT demande FROM Demande demande WHERE demande.region.id=:id")
    public List<Demande> findDemandeByRegion(@Param("id") Long id);
    
    public List<Demande> findByEtat(String etat);
    
    @Modifying
    @Query("UPDATE Demande d SET d.etat=:etat WHERE d.id in :ids" )
    public void updateDemandeByIdsAndEtat(@Param("etat") String etat,@Param("ids") List<Long> ids);
    
    @Modifying
    @Query("UPDATE Demande d SET d.ninea=:ninea ,d.rccm=:rccm, d.affliation=:affliation, d.description=:description WHERE d.id =:id" )
    public void update(@Param("ninea") String ninea,@Param("rccm") String rccm,@Param("affliation") String affliation,@Param("description") String description,@Param("id") Long id);
    
    @Query("SELECT demande FROM Demande demande WHERE demande.createdBy=:user ORDER BY demande.createdDate DESC")
    public Page<Demande> findAll(Pageable pageable,@Param("user")String user);
    
    @Query("SELECT demande FROM Demande demande ORDER BY demande.createdDate DESC")
    public Page<Demande> allDemande(Pageable pageable);
    
    @Query("SELECT demande FROM Demande demande WHERE demande.etat=:etat ORDER BY demande.createdDate DESC")
    public Page<Demande> allDemandeByStatus(Pageable pageable,@Param("etat") String etat);
    
    
    
    
    
}
