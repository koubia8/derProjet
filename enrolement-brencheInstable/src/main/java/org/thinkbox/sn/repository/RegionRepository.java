package org.thinkbox.sn.repository;

import org.thinkbox.sn.domain.Departement;
import org.thinkbox.sn.domain.Region;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


/**
 * Spring Data JPA repository for the Region entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface RegionRepository extends JpaRepository<Region, Long> {
    
    @Query("select region from Region region left join fetch region.departements where region.id =:id")
    public Region findOneWithDepartementById(@Param("id") Long id);
    
    public Region findByNom(String nom);

}
