package org.thinkbox.sn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.domain.TypePartenaire;

@Repository
@Transactional
public interface TypePartenaireRepository extends JpaRepository<TypePartenaire, Long>{

}
