/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.thinkbox.sn.domain.Authority;

/**
 *
 * @author guisse
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
