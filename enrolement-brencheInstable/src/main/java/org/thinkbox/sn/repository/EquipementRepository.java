/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.thinkbox.sn.domain.Equipement;

/**
 *
 * @author guisse
 */
@Repository
public interface EquipementRepository extends JpaRepository<Equipement, Long>{
    
}
