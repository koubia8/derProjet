package org.thinkbox.sn.repository;

import java.util.List;
import org.thinkbox.sn.domain.Commune;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


/**
 * Spring Data JPA repository for the Commune entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface CommuneRepository extends JpaRepository<Commune, Long> {
    public Commune findByNom(String nom);
    
    
    @Query("SELECT COUNT(c) FROM Commune c WHERE c.departement.id=:id")
    public Long countCommuneByDepartement(@Param("id") Long id);
}
