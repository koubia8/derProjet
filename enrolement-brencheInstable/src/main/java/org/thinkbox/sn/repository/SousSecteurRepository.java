/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.repository;

import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.domain.SousSecteur;

/**
 *
 * @author guisse
 */
@Repository
public interface SousSecteurRepository extends JpaRepository<SousSecteur, Long> {

    @Query("select ss from SousSecteur ss WHERE ss.secteur.id=:id")
    public List<SousSecteur> findSousSecteurBySecteurId(@Param("id") Long id);
    
  //  @Query("select ss from SousSecteur ss WHERE  ss.activeted =:activeted and ss.secteur.id=:id")
  //  public List<SousSecteur> findSousSecteurActiveBySecteurId(@Param("activeted") Boolean activeted,@Param("id") Long id);

    public Page<SousSecteur> findByActivated(Boolean activeted, Pageable pageable);
    Optional<SousSecteur> findOneByNomIgnoreCase(String nom);
    

    @Query("UPDATE SousSecteur ss set ss.activated=:activated WHERE ss.id=:id")
    @Modifying
    @Transactional
    public void enableOrdisable(@Param("id")Long id,@Param ("activated")Boolean activated);
    
    
}
