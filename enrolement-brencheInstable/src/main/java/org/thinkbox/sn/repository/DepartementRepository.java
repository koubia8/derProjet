package org.thinkbox.sn.repository;

import java.util.List;
import org.thinkbox.sn.domain.Departement;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


/**
 * Spring Data JPA repository for the Departement entity.
 */
@SuppressWarnings("unused")
@Repository
@Transactional
public interface DepartementRepository extends JpaRepository<Departement, Long> {
    
    @Query("select departement from Departement departement left join fetch departement.communes where departement.id =:id")
    public Departement findOneWithCommunetById(@Param("id") Long id);
    
    public Departement findByNom(String nom);
    
    @Query("SELECT COUNT(d) FROM Departement d WHERE d.region.id=:id")
    public Long countDepartementByRegion(@Param("id") Long id);
}
