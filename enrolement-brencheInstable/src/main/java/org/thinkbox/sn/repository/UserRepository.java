package org.thinkbox.sn.repository;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    public User findByLogin(String username);

    public User findByEmail(String email);

    @Query("UPDATE User u set u.motdepasse=?1 WHERE u.id=?2")
    @Modifying
    @Transactional
    public void updatePassword(String password, Long id);

    Optional<User> findOneByEmailIgnoreCase(String email);

    Optional<User> findOneByLogin(String login);
    
    Optional<User> findOneByLoginAndActivatedTrue(String login);
    
    Optional<User> findOneByLoginAndActivatedFalse(String login);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesById(Long id);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesByEmail(String email);

    Page<User> findAllByLoginNot(Pageable pageable, String login);
}
