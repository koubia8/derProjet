package org.thinkbox.sn.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.thinkbox.sn.domain.Guichet;

@Repository
public interface GuichetRepository extends JpaRepository<Guichet, Long> {

    public Guichet findByNom(String nom);

    public Page<Guichet> findByActivated(Boolean activeted, Pageable pageable);

    @Query("SELECT MIN(g.max) FROM Guichet g")
    public Double getMaxMontant();

}
