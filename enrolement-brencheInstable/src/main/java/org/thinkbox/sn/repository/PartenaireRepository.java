package org.thinkbox.sn.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.domain.Partenaire;
@Repository
public interface PartenaireRepository extends JpaRepository<Partenaire, Long> {
    //@Query
    public Page<Partenaire> findByActivatedTrue(Pageable pageable);
    public Page<Partenaire> findByActivatedFalse(Pageable pageable);
    
    public Page<Partenaire> findByActivated(Boolean activeted,Pageable pageable);
    
    @Query("UPDATE Partenaire ss set ss.activated=:activated WHERE ss.id=:id")
    @Modifying
    @Transactional
    public void enableOrdisable(@Param("id")Long id,@Param ("activated")Boolean activated);
    
    
    
    

}
