package org.thinkbox.sn.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thinkbox.sn.domain.Commune;
import org.thinkbox.sn.domain.Departement;
import org.thinkbox.sn.domain.Region;
import org.thinkbox.sn.service.dto.CommuneDTO;
import org.thinkbox.sn.repository.CommuneRepository;
import org.thinkbox.sn.repository.DepartementRepository;
import org.thinkbox.sn.repository.RegionRepository;

@Controller
public class CommuneController {

	@Autowired 
	RegionRepository repositoryRegion;
	
	@Autowired 
	DepartementRepository repositoryDepartement;
	
	@Autowired
	CommuneRepository repositoryCommune;
	
    @RequestMapping(value = "commune/list", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("communes", repositoryCommune.findAll());
        return "commune/list";
    }
    
    @RequestMapping(value="commune/add",method=RequestMethod.GET)
   	public String add(CommuneDTO communeDTO,Model model){
    	model.addAttribute("regions", repositoryRegion.findAll());
   		return "commune/add";
   	}
    
    @RequestMapping(value="commune/add",method=RequestMethod.POST)
	public String add(CommuneDTO communeDTO,BindingResult result, Model model,RedirectAttributes ra){
		try {
			Departement dep = repositoryDepartement.findOne(communeDTO.getIdDepartement());
			Commune commune = communeDTO.toEntity();
			commune.setDepartement(dep);
			repositoryCommune.save(commune);
			
		} catch (Exception e) {
			model.addAttribute("erreur","une erreur s'est produite lors de l'operation");
			return "commune/add";
		}
		ra.addFlashAttribute("success","Commune ajoute avec succes");
		return "redirect:/commune/list";
	}
	
    
        @ResponseBody
	@RequestMapping(value="departement/{idRegion}",method=RequestMethod.GET)
	public List<Departement> getDepartement(@PathVariable Long idRegion){
    	Region region = repositoryRegion.findOneWithDepartementById(idRegion);
		return region.getDepartements();
	}
    
        @ResponseBody
	@RequestMapping(value="commune/{idDepartement}",method=RequestMethod.GET)
	public List<Commune> getCommune(@PathVariable Long idDepartement){
    	Departement dep = repositoryDepartement.findOneWithCommunetById(idDepartement);
		return dep.getCommunes();
	}
	
}
