/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.thinkbox.sn.service.DemandeSearchService;

/**
 *
 * @author guisse
 */
@Controller
public class DemandeSearchController {
    
    private final DemandeSearchService demandeSearchService;

    public DemandeSearchController(DemandeSearchService demandeSearchService) {
        this.demandeSearchService = demandeSearchService;
    }
    
    @RequestMapping(value = "demande/search", method = RequestMethod.GET)
    public String list(@RequestParam(required = false) String numero,@RequestParam(required = false) String cni,@RequestParam(required = false) String telephone,Model model) {
        
        model.addAttribute("demandeSearchModel",demandeSearchService.getDemandes(numero,cni,telephone));
        return "demande/simple_search";
    }
}
