package org.thinkbox.sn.web.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.thinkbox.sn.domain.Secteur;
import org.thinkbox.sn.domain.SousSecteur;
import org.thinkbox.sn.repository.SecteurRepository;
import org.thinkbox.sn.repository.SousSecteurRepository;
import org.thinkbox.sn.security.AuthoritiesConstants;
import org.thinkbox.sn.service.SecteurService;
import org.thinkbox.sn.service.mapper.SousSecteurMapper;

@Controller
public class SecteurController {

    private final SecteurService secteurService;

    private final SecteurRepository secteurRepository;
    private final SousSecteurRepository sousSecteurRepository;

    private final SousSecteurMapper sousSecteurMapper;

    @Autowired
    public SecteurController(SecteurService secteurService, SecteurRepository secteurRepository, SousSecteurRepository sousSecteurRepository, SousSecteurMapper sousSecteurMapper) {
        this.secteurService = secteurService;
        this.secteurRepository = secteurRepository;
        this.sousSecteurRepository = sousSecteurRepository;
        this.sousSecteurMapper = sousSecteurMapper;
    }

    // SousSecteur
    @RequestMapping(value = "/sous-secteur-add/{id}", method = RequestMethod.GET)
    public String joutSouSecteur(@PathVariable Long id,Model model) {
        Secteur secteur = secteurRepository.findOneWithSousSecteurById(id);
        model.addAttribute("secteurId", secteur.getId());
       // model.addAttribute("secteurId", 9);
        model.addAttribute("souSecteur", new SousSecteur());
        return "/sous_secteur/add";
    }

    // Secteur
    @RequestMapping(value = "/secteur-add", method = RequestMethod.GET)
    public String joutSecteur(Model model) {
        model.addAttribute("secteur", new Secteur());
        return "/secteur/add";
    }

    @RequestMapping(value = "/secteur-update")
    public String edit(Model model, @RequestParam(name = "id") long id) {
        //model.addAttribute("allIntervention", interventionrepository.findAll());
        model.addAttribute("secteur", secteurRepository.findOne(id));
        return "/secteur/modif_secteur";
    }
    @RequestMapping(value = "/sous-secteur-update")
    public String editSousSecteur(Model model, @RequestParam(name = "id") long id) {
        //model.addAttribute("allIntervention", interventionrepository.findAll());
        model.addAttribute("sousSecteur", sousSecteurRepository.findOne(id));
        return "/sous_secteur/update_sous_secteur";
    }

    @RequestMapping(value = "/maintenance-saveGarage", method = RequestMethod.POST)
    public String save(Secteur secteur) {
        secteurRepository.save(secteur);
        return "redirect:list";
    }

    @RequestMapping(value = "secteur/active/{id}", method = RequestMethod.POST)
    public void active(@PathVariable("id") long id) {
        Secteur secteur = secteurRepository.findOne(id);
        secteur.setActivated(Boolean.TRUE);
        secteurRepository.save(secteur);
    }

    @RequestMapping(value = "secteur/desactive/{id}", method = RequestMethod.POST)
    public void desactive(@PathVariable("id") long id) {
        Secteur secteur = secteurRepository.findOne(id);
        secteur.setActivated(Boolean.FALSE);
        secteurRepository.save(secteur);
    }

    @RequestMapping(value = "sousecteur/active/{id}", method = RequestMethod.POST)
    public void activeSousecteur(@PathVariable("id") long id) {
        SousSecteur sousSecteur = sousSecteurRepository.findOne(id);
        sousSecteur.setActivated(Boolean.TRUE);
        sousSecteurRepository.save(sousSecteur);
    }

    @RequestMapping(value = "sousecteur/desactive/{id}", method = RequestMethod.POST)
    public void desactiveSousec(@PathVariable("id") long id) {
        SousSecteur sousSecteur = sousSecteurRepository.findOne(id);
        sousSecteur.setActivated(Boolean.FALSE);
        sousSecteurRepository.save(sousSecteur);
    }

    @RequestMapping(value = "secteur/list", method = RequestMethod.GET)
    @Secured(AuthoritiesConstants.ADMIN)
    public String list(Model model) {
        model.addAttribute("secteurs", secteurService.findAll());
        return "secteur/list";
    }

    @RequestMapping(value = "secteur/active/list", method = RequestMethod.GET)
    public String listSecteurActive(Model model, Pageable pageable) {
        model.addAttribute("secteurs", secteurRepository.findByActivated(Boolean.TRUE, pageable));
        return "secteur/list";
    }

    @RequestMapping(value = "secteur/desactive/list", method = RequestMethod.GET)
    public String listSecteurDesative(Model model, Pageable pageable) {
        model.addAttribute("secteurs", secteurRepository.findByActivated(Boolean.FALSE, pageable));
        return "secteur/list";
    }

    @RequestMapping(value = "sous_secteur/fiche/{id}", method = RequestMethod.GET)
    public String fiche(@PathVariable Long id, Model model) {
        Secteur secteur = secteurRepository.findOneWithSousSecteurById(id);
        Set<SousSecteur> set = secteur.getSousSecteurs();
        List<SousSecteur> sousSecteurs = set
                .stream()
                .collect(Collectors.toList());
        model.addAttribute("secteurId", secteur.getId());
        model.addAttribute("secteurNom", secteur.getNom());
        model.addAttribute("sousSecteurs", sousSecteurMapper.toDto(sousSecteurs));
        return "sous_secteur/list";
    }
    /* @RequestMapping(value = "secteur/fiche/{id}", method = RequestMethod.GET)
    public String fiched(@PathVariable Long id, Model model) {
        List<SousSecteur>  list= sousSecteurRepository.findSousSecteurActiveBySecteurId(Boolean.TRUE, id);
        model.addAttribute("sousSecteurs", list);
        return "secteur/fiche";
    }*/
}
