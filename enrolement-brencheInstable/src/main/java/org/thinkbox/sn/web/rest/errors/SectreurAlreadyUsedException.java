package org.thinkbox.sn.web.rest.errors;

public class SectreurAlreadyUsedException extends BadRequestAlertException {

    public SectreurAlreadyUsedException() {
        super(ErrorConstants.SECTEUR_ALREADY_USED_TYPE, "Ce secteur existe  déja ", "secteurManagement", "seteurexists");
    }
}
