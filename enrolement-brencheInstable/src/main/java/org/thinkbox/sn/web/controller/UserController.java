package org.thinkbox.sn.web.controller;

import java.util.Optional;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thinkbox.sn.domain.User;
import org.thinkbox.sn.service.dto.UserDTO;
import org.thinkbox.sn.service.dto.UserForm;
import org.thinkbox.sn.repository.PartenaireRepository;
import org.thinkbox.sn.repository.UserRepository;
import org.thinkbox.sn.service.CustomMailSender;
import org.thinkbox.sn.service.UserService;
import org.thinkbox.sn.service.dto.CustomUser;
import org.thinkbox.sn.service.dto.PartenaireDTO;
import org.thinkbox.sn.service.util.RandomUtil;

@Controller
public class UserController {

    private final PasswordEncoder passwordEncoder;

    private final UserService userService;

    private final UserRepository userRepository;

    private final CustomMailSender mailSender;

    private final PartenaireRepository repositoryPartenaire;

    public UserController(PasswordEncoder passwordEncoder, UserService userService, UserRepository userRepository, CustomMailSender mailSender, PartenaireRepository repositoryPartenaire) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.userRepository = userRepository;
        this.mailSender = mailSender;
        this.repositoryPartenaire = repositoryPartenaire;
    }

//    @RequestMapping(value = "user/profil", method = RequestMethod.GET)
//    public String updatePassword(Model model) {
//        model.addAttribute("user", getCurrentUser());
//        return "user/profil";
//    }
    @RequestMapping(value = "users/add", method = RequestMethod.GET)
    public String add(Model model ,Pageable pageable) {
        
         model.addAttribute("userDTO", new UserDTO());
        final Page<UserDTO> page = userService.getAllManagedUsers(pageable);
        model.addAttribute("users", page.getContent());
        model.addAttribute("partenaires", repositoryPartenaire.findAll());
        return "users/add";
    }

    @RequestMapping(value = "users/edit/{login}", method = RequestMethod.GET)
    public String edit(Model model, @PathVariable(required = true) String login) {
        UserDTO user = userService.getUserWithAuthoritiesByLogin(login)
                .map(UserDTO::new).get();
        System.out.println("Partenaire id " + user.getPartenaireId());
        model.addAttribute("user", user);
        model.addAttribute("partenaires", repositoryPartenaire.findAll());
        return "users/edit";
    }

    @RequestMapping(value = "users/list", method = RequestMethod.GET)
    public String list(Model model, Pageable pageable) {
        model.addAttribute("userDTO", new UserDTO());
        final Page<UserDTO> page = userService.getAllManagedUsers(pageable);
        model.addAttribute("users", page.getContent());
        model.addAttribute("partenaires", repositoryPartenaire.findAll());
        return "users/list";
    }

    @RequestMapping(value = "account/changepassword", method = RequestMethod.GET)
    public String updatePassword(UserForm userForm) {
        return "account/changepassword";
    }

    @RequestMapping(value = "account/changepassword", method = RequestMethod.POST)
    public String updatePassword(UserForm userForm, Model model, RedirectAttributes redirectAttributes) {
        if (!userForm.controle()) {
            model.addAttribute("erreur", "les deux mots de passe ne sont pas identiques");
            return "account/changepassword";
        }
        try {
            userService.changePassword(userForm.getNewPassword());
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("erreur", "une erreur est survenue, le mot de passe n'a pas ete modifie");
            return "account/changepassword";
        }
        redirectAttributes.addFlashAttribute("info", "le mot de passe a ete modifie avec succes");
        return "redirect:/logout";
    }
//
//    public User getCurrentUser() {
//        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//    }
//
//    private boolean isPasswordConform(String currentPassword) {
//        return passwordEncoder.matches(currentPassword, getCurrentUser().getMotdepasse());
//    }
//
//    @Secured("ROLE_ADMIN")
//    @RequestMapping(value = "user/add", method = RequestMethod.GET)
//    public String add(Model model) {
//        model.addAttribute("userDTO", new UserDTO());
//        model.addAttribute("partenaires", repositoryPartenaire.findAll());
//        return "user/add";
//    }
//
//    @Secured("ROLE_ADMIN")
//    @RequestMapping(value = "user/add", method = RequestMethod.POST)
//    public String add(@ModelAttribute("userDTO") UserDTO userDTO, BindingResult result, Model model, RedirectAttributes ra) {
//        String pw;
//        try {
//            User test = userService.findByUsername(userDTO.getLogin().trim());
//            if (test != null) {
//                ra.addFlashAttribute("erreur", "ce login existe deja");
//                return "redirect:/user/list";
//            }
//
//            User user = userDTO.toEntity();
//            pw = RandomStringUtils.randomAlphanumeric(5);
//            String crypt = BCrypt.hashpw(pw, BCrypt.gensalt());
//            user.setMotdepasse(crypt);
//            //user.setRole("ROLE_USER");
//            Partenaire p = repositoryPartenaire.findOne(userDTO.getIdPartenaire());
//            user.setPartenaire(p);
//            userService.save(user);
//            if (user.getId() != null) {
//                mailSender.SendMail("mail.thinkbox@gmail.com", user.getEmail(), "Mot de passe", "Votre compte a ete cree :Login=" + user.getLogin() + " Password=" + pw);
//            }
//
//        } catch (Exception e) {
//            ra.addFlashAttribute("erreur", "une erreur s'est produite");
//            return "redirect:/user/list";
//        }
//        ra.addFlashAttribute("info", "utiliateur ajoute avec succes password");
//        return "redirect:/user/list";
//    }
//

    @RequestMapping(value = "users/resetpassword", method = RequestMethod.GET)
    public String forgetpassword(CustomUser customUser) {
        return "users/resetpassword";
    }
//

    @RequestMapping(value = "users/resetpassword", method = RequestMethod.POST)
    public String forgetpassword(@Valid CustomUser customUser, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "users/resetpassword";
        }
        try {
            String password = RandomUtil.generatePassword();
            Optional<User> user = userService.requestPasswordReset(customUser.getEmail(), password);
            if (!user.isPresent()) {
                model.addAttribute("info", "aucun compte n'a été trouvé,il se peut que ce compte soit desactivé");
                return "/users/resetpassword";
            }
            boolean res = mailSender.SendMail("mail.thinkbox@gmail.com", customUser.getEmail(), "Mot de passe", "Votre compte a été réinitialisé , votre nouveau mot de passe est : " + password);
            if (res) {
                model.addAttribute("info", "un nouveau mot de passe a ete envoye sur votre email");
                return "/users/resetpassword";
            } else {
                model.addAttribute("erreur", "une erreur est survenue lors de l'envoi du  mot de passe");
                return "/users/resetpassword";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            model.addAttribute("erreur", "une erreur s'est produite lors de l'operation");
            return "/users/resetpassword";
        }
    }
    
    
    // djibril affiche details utilisateur
    
    @RequestMapping(value = "users/details/{login}", method = RequestMethod.GET)
    public String detail(Model model, @PathVariable(required = true) String login) {
        UserDTO user = userService.getUserWithAuthoritiesByLogin(login)
                .map(UserDTO::new).get();
       String partenaire= repositoryPartenaire.findOne(user.getPartenaireId()).getNom();
      
      
        		
        System.out.println("Partenaire id " + user.getPartenaireId());
        model.addAttribute("user", user);
        model.addAttribute("partenaire",partenaire );
        return "users/details_user";
    }

}
