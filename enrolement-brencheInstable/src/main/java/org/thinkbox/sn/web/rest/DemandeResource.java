/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.thinkbox.sn.constantes.EtatDemande;
import org.thinkbox.sn.domain.Demande;
import org.thinkbox.sn.domain.Equipement;
import org.thinkbox.sn.domain.Responsable;
import org.thinkbox.sn.security.AuthoritiesConstants;
import org.thinkbox.sn.service.dto.CustomDTO;
import org.thinkbox.sn.service.dto.DemandeDTO;
import org.thinkbox.sn.service.DemandeService;
import org.thinkbox.sn.service.dto.ResponsableDTO;
import org.thinkbox.sn.web.rest.errors.BadRequestAlertException;
import org.thinkbox.sn.web.rest.util.HeaderUtil;
import org.thinkbox.sn.web.rest.util.ListWrapper;

/**
 *
 * @author guisse
 */
@RestController
@RequestMapping("/api")
public class DemandeResource {

    private final Logger log = LoggerFactory.getLogger(DemandeResource.class);

    private static final String ENTITY_NAME = "demande";

    private final DemandeService demandeService;
    
    @Value("${baseBusinessPlan}")
    private String baseDirectory;

    public DemandeResource(DemandeService demandeService) {
        this.demandeService = demandeService;
    }

    @PostMapping("/demandes")
    public ResponseEntity<DemandeDTO> createDemande(@Valid @RequestBody DemandeDTO demandeDTO) throws URISyntaxException {
        log.debug("REST request to save Demande : {}", demandeDTO);
        if (demandeDTO.getId() != null) {
            throw new BadRequestAlertException("A new demande cannot already have an ID", ENTITY_NAME, "idexists");
        }
        
        demandeDTO.setNumero(demandeService.numeroDossier());
        DemandeDTO result = demandeService.save(demandeDTO);
        return ResponseEntity.created(new URI("/api/demandes/" + result.getId()))
                .headers(HeaderUtil.createAlert("la demande  a été enregistrée avec succès", result.getId().toString()))
                .body(result);
    }

    @PutMapping("/demandes")
    public ResponseEntity<DemandeDTO> updateDemande(@Valid @RequestBody DemandeDTO demandeDTO) throws URISyntaxException {
        log.debug("REST request to update Demande : {}", demandeDTO);
        if (demandeDTO.getId() == null) {
            return createDemande(demandeDTO);
        }
        
        Demande demande = demandeService.findByNumero(demandeDTO.getNumero());
        if(null == demande){
            throw new BadRequestAlertException("A new demande cannot already have an ID", ENTITY_NAME, "idexists");
        }
        demandeDTO.setNumero(demande.getNumero());
        Optional<DemandeDTO> result = demandeService.updateDemande(demandeDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createAlert("la demande  a été modifiée avec succès", result.get().toString()))
                .body(result.get());
    }
    
    
    @PutMapping("/demandes/update")
    public ResponseEntity<CustomDTO> update(@Valid @RequestBody CustomDTO customDTO) throws URISyntaxException {
        log.debug("REST request to update Demande : {}", customDTO);
        if (customDTO.getId() == null) {
            throw new BadRequestAlertException("ID est requis", ENTITY_NAME, "idexists");
        }
        demandeService.update(customDTO);
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, "ok"))
                .body(customDTO);
    }
    

    @GetMapping("/demandes")
    public List<DemandeDTO> getAllDemandes() {
        log.debug("REST request to get all Demandes");
        return demandeService.findAll();
    }
    
     /* @GetMapping("/demande/filtre")
    public Demande filtre(String numero, String telephone1, String telephone2) {
        //model.addAttribute("demandes",demandeService.filterBy(numero, telephone1, telephone2));
        return demandeService.filterBy(numero, telephone1, telephone2);
    }*/

    @GetMapping("/demandes/{id}")
    public ResponseEntity<DemandeDTO> getDemande(@PathVariable Long id) {
        log.debug("REST request to get Demande : {}", id);
        DemandeDTO demandeDTO = demandeService.findOne(id);
        //DemandeDTO demandeDTO = demandeService.getDemandeDTOWithDependances(id);
        if (demandeDTO != null) {
            return new ResponseEntity<>(demandeDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/demandes/{id}")
    public ResponseEntity<Void>  deleteDemande(@PathVariable Long id) {
        log.debug("REST request to delete Demande : {}", id);
        demandeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    @PutMapping("/demandesupdatelist")
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<ListWrapper> updateDemande(@Valid @RequestBody ListWrapper wrapper) throws URISyntaxException {
        log.debug("REST request to update Demande : {}", wrapper.getIds());
        if (wrapper == null || wrapper.getIds().isEmpty()) {
            throw new BadRequestAlertException("la liste ne peut pas ete vide", ENTITY_NAME, "error");
        }
        switch (wrapper.getAction()) {
            case 0:
                demandeService.updateDemandeListByIdsAndEtat(EtatDemande.VALIDE, wrapper.getIds());
                break;
            case 1:
                demandeService.updateDemandeListByIdsAndEtat(EtatDemande.REJETE, wrapper.getIds());
                break;
            default:
                demandeService.updateDemandeListByIdsAndEtat(EtatDemande.EN_COURS, wrapper.getIds()); 
                break;
        }
       
        return ResponseEntity.ok()
                .headers(HeaderUtil.createAlert("la liste de demande  a été mise a jour avec succès", String.valueOf(wrapper.getIds().size())))
                .body(wrapper);
    }
    
    @PostMapping("/demandes/businessplan")
	public ResponseEntity<Void> add(@RequestParam MultipartFile businessPlan){
            if(businessPlan.isEmpty()){
                throw new BadRequestAlertException("Le ficher est requis velleiz uploader un ficher", ENTITY_NAME, "idexists");
            }
            try{
            byte[] bytes = businessPlan.getBytes();
            Path path = Paths.get(baseDirectory + businessPlan.getOriginalFilename());
            Files.write(path, bytes);   
            } 
            catch(Exception e){
               return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
         return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, "ok")).build();
		
	}

    @GetMapping(value = "", produces = {MediaType.APPLICATION_PDF_VALUE})
    public byte[] getPhoto(@RequestParam(required = true) Long id) {
        File photo = new File("");
        byte[] p = null;
        try {
            p = IOUtils.toByteArray(new FileInputStream(photo));
        } catch (IOException e) {
            p = null;
        }
        return p;
    }
    
    
//    
//    @RequestMapping(value = "demande-edit/{id}", method = RequestMethod.GET)
//    public String editDemande(@PathVariable(required = true) Long id, Model model) {
//        try {
//            Demande demande = demandeRepository.findOne(id);
//        if(!demande.getEtat().equals(EtatDemande.EN_COURS)){
//             return "redirect:/demande/list";
//        }
//        Set<Responsable> set = demande.getResponsables();
//        List<Responsable> responsables = set
//                .stream()
//                .collect(Collectors.toList());
//
//        Set<Equipement> eqs = demande.getEquipements();
//        List<Equipement> equipements = eqs
//                .stream()
//                .collect(Collectors.toList());
//        
//        DemandeDTO dto = demandeMapper.toDto(demande);
//        dto.setResponsablesDTO(responsableMapper.toDto(responsables));
//        model.addAttribute("demandeId", demande.getId());
//        model.addAttribute("demande",dto);
//       // model.addAttribute("responsables", );
//       // model.addAttribute("equipements", equipementMapper.toDto(equipements));
//        model.addAttribute("secteurs", secteurService.findAll());
//        model.addAttribute("regions", regionRepository.findAll());
//        return "demande/edit";
//            
//        } catch (Exception e) {
//            return "redirect:/demande/list";
//        }
//    }
//}
    
    
}
