package org.thinkbox.sn.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thinkbox.sn.domain.Region;
import org.thinkbox.sn.repository.RegionRepository;

@Controller
public class RegionController {
	
	@Autowired 
	RegionRepository repository;
	
    @RequestMapping(value = "region/list", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("regions", repository.findAll());
        return "region/list";
    }
    
    @RequestMapping(value="region/add",method=RequestMethod.GET)
   	public String add(Region region,Model model){
   		return "region/add";
   	}
    
    @RequestMapping(value="region/add",method=RequestMethod.POST)
	public String add(Region region,BindingResult result, Model model,RedirectAttributes ra){
		try {
			repository.save(region);
			
		} catch (Exception e) {
			model.addAttribute("erreur","une erreur s'est produite lors de l'operation");
			return "region/add";
		}
		ra.addFlashAttribute("success","Frome Juridique ajoute avec succes");
		return "redirect:/region/list";
	}
	

}
