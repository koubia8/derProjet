package org.thinkbox.sn.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AccountController {
  
    @RequestMapping(value = "/api/account/change-password", method = RequestMethod.GET)
    public String changePassword() {
        return "account/changepassword";
    }
    
    @RequestMapping(value = "/api/account/reset-password", method = RequestMethod.GET)
    public String resetPassword() {
        return "account/resetpassword";
    }

}
