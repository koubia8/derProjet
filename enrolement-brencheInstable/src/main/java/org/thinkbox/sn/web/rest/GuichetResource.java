/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thinkbox.sn.service.dto.GuichetDTO;
import org.thinkbox.sn.service.GuichetService;
import org.thinkbox.sn.web.rest.errors.BadRequestAlertException;
import org.thinkbox.sn.web.rest.util.HeaderUtil;

/**
 *
 * @author guisse
 */
@RestController
@RequestMapping("/api")
public class GuichetResource {
    
    private final Logger log = LoggerFactory.getLogger(GuichetResource.class);

    private static final String ENTITY_NAME = "guichet";

    private final GuichetService guichetService;

    public GuichetResource(GuichetService guichetService) {
        this.guichetService = guichetService;
    }

    

    @PostMapping("/guichets")
    public ResponseEntity<GuichetDTO> createGuichet(@Valid @RequestBody GuichetDTO guichetDTO) throws URISyntaxException {
        log.debug("REST request to save Guichet : {}", guichetDTO);
        if (guichetDTO.getId() != null) {
            throw new BadRequestAlertException("A new guichet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GuichetDTO result = guichetService.save(guichetDTO);
        return ResponseEntity.created(new URI("/api/guichets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/guichets")
    public ResponseEntity<GuichetDTO> updateGuichet(@Valid @RequestBody GuichetDTO guichetDTO) throws URISyntaxException {
        log.debug("REST request to update Guichet : {}", guichetDTO);
        if (guichetDTO.getId() == null) {
            return createGuichet(guichetDTO);
        }
        GuichetDTO result = guichetService.save(guichetDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, guichetDTO.getId().toString()))
            .body(result);
    }

    @GetMapping("/guichets")
    public List<GuichetDTO> getAllGuichets() {
        log.debug("REST request to get all Guichets");
        return guichetService.findAll();
        }

    @GetMapping("/guichets/{id}")
    public ResponseEntity<GuichetDTO> getGuichet(@PathVariable Long id) {
        log.debug("REST request to get Guichet : {}", id);
        GuichetDTO guichetDTO = guichetService.findOne(id);
        if(guichetDTO != null){
            return new ResponseEntity<>(guichetDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/guichets/{id}")
    public ResponseEntity<Void> deleteGuichet(@PathVariable Long id) {
        log.debug("REST request to delete Guichet : {}", id);
        guichetService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/getmaxvalueguichetsbynom/{nom}")
    public Double getMaxValueGuichetByNom(@PathVariable String nom) {
        log.debug("REST request to getuichet : {}", nom);
        return guichetService.getMaxValueGuichetByNom(nom);
    }
    
    @GetMapping("/getminmontantguichet")
    public Double getMinMontantGuichet() {
        return guichetService.getMinMontantGuichet();
    }
    
    @DeleteMapping("/guichet/desactive/{id}")
    public ResponseEntity<Void> desactive(@PathVariable("id") long id) {
        
        GuichetDTO guichetDTO = guichetService.findOne(id);
        guichetDTO.setActive(Boolean.FALSE);

        guichetService.save(guichetDTO);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("Guichet desactivé avec succès")).build();
    }
    @DeleteMapping( "/guichet/active/{id}")
    public ResponseEntity<Void> active(@PathVariable("id") long id) {
       GuichetDTO guichetDTO = guichetService.findOne(id);
        guichetDTO.setActive(Boolean.TRUE);
        guichetService.save(guichetDTO);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("Guichet activé avec succès")).build();
    }
}
