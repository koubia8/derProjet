/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thinkbox.sn.domain.Responsable;
import org.thinkbox.sn.service.DemandeService;
import org.thinkbox.sn.service.dto.ResponsableDTO;
import org.thinkbox.sn.service.ResponsableService;
import org.thinkbox.sn.web.rest.errors.BadRequestAlertException;
import org.thinkbox.sn.web.rest.util.HeaderUtil;

/**
 *
 * @author guisse
 */

@RestController
@RequestMapping("/api")
public class ResponsableResource {
    
    private final Logger log = LoggerFactory.getLogger(ResponsableResource.class);

    private static final String ENTITY_NAME = "responsable";

    private final ResponsableService responsableService;
    private final DemandeService demandeService;

    public ResponsableResource(ResponsableService responsableService, DemandeService demandeService) {
        this.responsableService = responsableService;
        this.demandeService = demandeService;
    }

    

    @PostMapping("/responsables")
    public ResponseEntity<ResponsableDTO> createResponsable(@Valid @RequestBody ResponsableDTO responsableDTO) throws URISyntaxException {
        log.debug("REST request to save Responsable : {}", responsableDTO);
        if (responsableDTO.getId() != null) {
            throw new BadRequestAlertException("A new responsable cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ResponsableDTO result = responsableService.save(responsableDTO);
        return ResponseEntity.created(new URI("/api/responsables/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
    
     /*@GetMapping("/demande/filtre")
    public Responsable filtre(String numero, String telephone1, String telephone2) {
        //model.addAttribute("demandes",demandeService.filterBy(numero, telephone1, telephone2));
        return demandeService.filterBy(numero, telephone1, telephone2);
    }*/

    @PutMapping("/responsables")
    public ResponseEntity<ResponsableDTO> updateResponsable(@Valid @RequestBody ResponsableDTO responsableDTO) throws URISyntaxException {
        log.debug("REST request to update Responsable : {}", responsableDTO);
        if (responsableDTO.getId() == null) {
            return createResponsable(responsableDTO);
        }
        ResponsableDTO result = responsableService.save(responsableDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, responsableDTO.getId().toString()))
            .body(result);
    }

    @GetMapping("/responsables")
    public List<ResponsableDTO> getAllResponsables() {
        log.debug("REST request to get all Responsables");
        return responsableService.findAll();
        }

    @GetMapping("/responsables/{id}")
    public ResponseEntity<ResponsableDTO> getResponsable(@PathVariable Long id) {
        log.debug("REST request to get Responsable : {}", id);
        ResponsableDTO responsableDTO = responsableService.findOne(id);
        if(responsableDTO != null){
            return new ResponseEntity<>(responsableDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/responsables/{id}")
    public ResponseEntity<Void> deleteResponsable(@PathVariable Long id) {
        log.debug("REST request to delete Responsable : {}", id);
        responsableService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
}
