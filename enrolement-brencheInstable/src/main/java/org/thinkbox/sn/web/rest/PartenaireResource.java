/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thinkbox.sn.service.PartenaireService;
import org.thinkbox.sn.service.dto.PartenaireDTO;
import org.thinkbox.sn.web.rest.errors.BadRequestAlertException;
import org.thinkbox.sn.web.rest.util.HeaderUtil;

/**
 *
 * @author guisse
 */
@RestController
@RequestMapping("/api")
public class PartenaireResource {
    
    private final Logger log = LoggerFactory.getLogger(PartenaireResource.class);

    private static final String ENTITY_NAME = "partenaire";

    private final PartenaireService partenaireService;

    public PartenaireResource(PartenaireService partenaireService) {
        this.partenaireService = partenaireService;
    }

    @PostMapping("/partenaires")
    public ResponseEntity<PartenaireDTO> createPartenaire(@Valid @RequestBody PartenaireDTO partenaireDTO) throws URISyntaxException {
        log.debug("REST request to save Partenaire : {}", partenaireDTO);
        if (partenaireDTO.getId() != null) {
            throw new BadRequestAlertException("A new partenaire cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PartenaireDTO result = partenaireService.save(partenaireDTO);
        return ResponseEntity.created(new URI("/api/partenaires/" + result.getId()))
            .headers(HeaderUtil.createAlert("partenaire crée avec succès"))
            .body(result);
    }
    
    @DeleteMapping("/partenaire/desactive/{id}")
    public ResponseEntity<Void> desactive(@PathVariable("id") long id) {
        partenaireService.disablePartenaire(id);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("Partenaire desactivé avec succès")).build();
    }
     @DeleteMapping( "/partenaire/active/{id}")
    public ResponseEntity<Void> activePartenaire(@PathVariable("id") long id) {
        partenaireService.enablePartenaire(id);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("Partenaire activé avec succès")).build();
    }   
    @PutMapping("/partenaires")
    public ResponseEntity<PartenaireDTO> updatePartenaire(@Valid @RequestBody PartenaireDTO partenaireDTO) throws URISyntaxException {
        log.debug("REST request to update Partenaire : {}", partenaireDTO);
        if (partenaireDTO.getId() == null) {
            return createPartenaire(partenaireDTO);
        }
        PartenaireDTO result = partenaireService.save(partenaireDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createAlert("partenaire modifié avec succès"))
            .body(result);
    }

    @GetMapping("/partenaires")
    public List<PartenaireDTO> getAllPartenaires() {
        log.debug("REST request to get all Partenaires");
        return partenaireService.findAll();
        }

    @GetMapping("/partenaires/{id}")
    public ResponseEntity<PartenaireDTO> getPartenaire(@PathVariable Long id) {
        log.debug("REST request to get Partenaire : {}", id);
        PartenaireDTO partenaireDTO = partenaireService.findOne(id);
        if(partenaireDTO != null){
            return new ResponseEntity<>(partenaireDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/partenaires/{id}")
    public ResponseEntity<Void> deletePartenaire(@PathVariable Long id) {
        log.debug("REST request to delete Partenaire : {}", id);
        partenaireService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("partenaire supprimé avec succès")).build();
    }
    
}
