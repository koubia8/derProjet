/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thinkbox.sn.domain.SousSecteur;
import org.thinkbox.sn.repository.SousSecteurRepository;
import org.thinkbox.sn.service.dto.SousSecteurDTO;
import org.thinkbox.sn.service.SousSecteurService;
import org.thinkbox.sn.service.mapper.SousSecteurMapper;
import org.thinkbox.sn.web.rest.errors.BadRequestAlertException;
import org.thinkbox.sn.web.rest.errors.SousSectreurAlreadyUsedException;
import org.thinkbox.sn.web.rest.util.HeaderUtil;

/**
 *
 * @author guisse
 */
@RestController
@RequestMapping("/api")
public class SousSecteurResource {
    
    private final Logger log = LoggerFactory.getLogger(SousSecteurResource.class);

    private static final String ENTITY_NAME = "sousSecteur";

    private final SousSecteurService sousSecteurService;
    private final SousSecteurRepository sousSecteurRepository;
    
    private final SousSecteurMapper sousSecteurMapper;

    public SousSecteurResource(SousSecteurService sousSecteurService, SousSecteurRepository sousSecteurRepository, SousSecteurMapper sousSecteurMapper) {
        this.sousSecteurService = sousSecteurService;
        this.sousSecteurRepository = sousSecteurRepository;
        this.sousSecteurMapper = sousSecteurMapper;
    }


    @PostMapping("/sousSecteurs")
    public ResponseEntity<SousSecteurDTO> createSousSecteur(@Valid @RequestBody SousSecteurDTO sousSecteurDTO) throws URISyntaxException {
        log.debug("REST request to save SousSecteur : {}", sousSecteurDTO);
        if (sousSecteurDTO.getId() != null) {
            throw new BadRequestAlertException("A new sousSecteur cannot already have an ID", ENTITY_NAME, "idexists");
        } else if (sousSecteurRepository.findOneByNomIgnoreCase(sousSecteurDTO.getNom()).isPresent()) {
            throw new SousSectreurAlreadyUsedException();
        }else{
        SousSecteurDTO result = sousSecteurService.save(sousSecteurDTO);
        return ResponseEntity.created(new URI("/api/sousSecteurs/" + result.getId()))
            .headers(HeaderUtil.createAlert("Sous secteur crée avec succès"))
            .body(result);
    }
    }

    @PutMapping("/sousSecteurs")
    public ResponseEntity<SousSecteurDTO> updateSousSecteur(@Valid @RequestBody SousSecteurDTO sousSecteurDTO) throws URISyntaxException {
        log.debug("REST request to update SousSecteur : {}", sousSecteurDTO);
        if (sousSecteurDTO.getId() == null) {
            return createSousSecteur(sousSecteurDTO);
        }
        SousSecteurDTO result = sousSecteurService.save(sousSecteurDTO);
       /* return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sousSecteurDTO.getId().toString()))
            .body(result);*/
        
        return ResponseEntity.ok()
            .headers(HeaderUtil.createAlert("sous secteur modifié avec succès"))
            .body(result);
    }

    @GetMapping("/sousSecteurs")
    public List<SousSecteurDTO> getAllSousSecteurs() {
        log.debug("REST request to get all SousSecteurs");
        return sousSecteurService.findAll();
        }

    @GetMapping("/sousSecteurs/{id}")
    public ResponseEntity<SousSecteurDTO> getSousSecteur(@PathVariable Long id) {
        log.debug("REST request to get SousSecteur : {}", id);
        SousSecteurDTO sousSecteurDTO = sousSecteurService.findOne(id);
        if(sousSecteurDTO != null){
            return new ResponseEntity<>(sousSecteurDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/sousSecteurs/{id}")
    public ResponseEntity<Void> deleteSousSecteur(@PathVariable Long id) {
        log.debug("REST request to delete SousSecteur : {}", id);
        sousSecteurService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/getSousSecteursBySecteur/{id}")
    public ResponseEntity<List<SousSecteurDTO>> getSousSecteurBySecturId(@PathVariable Long id) {
        log.debug("REST request to get SousSecteur : {}", id);
        List<SousSecteurDTO> sousSecteursDTO = sousSecteurService.findSousSecteurBySecteurId(id);
        if(sousSecteursDTO != null){
            return new ResponseEntity<>(sousSecteursDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
}
