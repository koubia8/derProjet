/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.controller;

import java.security.Principal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thinkbox.sn.domain.Departement;
import org.thinkbox.sn.domain.Partenaire;
import org.thinkbox.sn.domain.Region;
import org.thinkbox.sn.domain.User;
import org.thinkbox.sn.repository.CommuneRepository;
import org.thinkbox.sn.repository.DepartementRepository;
import org.thinkbox.sn.repository.UserRepository;
import org.thinkbox.sn.service.DemandeService;
import org.thinkbox.sn.service.StatistiqueService;

/**
 *
 * @author guisse
 */
@Controller
public class HomeController {

    private final DepartementRepository departementRepository;

    private final CommuneRepository communeRepository;

    private final UserRepository userRepository;

    private final DemandeService demandeService;

    private final StatistiqueService suiviDossierRegionService;

    public HomeController(DepartementRepository departementRepository, CommuneRepository communeRepository, UserRepository userRepository, DemandeService demandeService, StatistiqueService suiviDossierRegionService) {
        this.departementRepository = departementRepository;
        this.communeRepository = communeRepository;
        this.userRepository = userRepository;
        this.demandeService = demandeService;
        this.suiviDossierRegionService = suiviDossierRegionService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {

        return "redirect:/home";
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String list(Model model, Principal principal) {
        String titre = "COMMUNE";
        Region region = null;
        Departement departement =null;
        User user = userRepository.findByLogin(principal.getName());
        Partenaire partenaire = user.getPartenaire();
        if (partenaire != null) {
             model.addAttribute("partenaire", partenaire.getNom());
             region = partenaire.getRegion();
             departement = partenaire.getDepartement();
        }

        if (region != null) {
            model.addAttribute("region", region.getNom());
            model.addAttribute("departements", departementRepository.countDepartementByRegion(region.getId()));
        } else {
            model.addAttribute("departements", departementRepository.count());
        }

        if (departement != null) {
            model.addAttribute("departement", departement.getNom());
            model.addAttribute("communes", communeRepository.countCommuneByDepartement(departement.getId()));
            model.addAttribute("demandes", demandeService.getCountAllDemandeByDepartement(departement.getId()));
            model.addAttribute("beneficiaires", demandeService.getDemandeDepartmentValided(departement.getId()));
            model.addAttribute("vsdrs", suiviDossierRegionService.getStatsCommnes(departement.getId()));
        } else {
            titre = "REGION";
            model.addAttribute("communes", communeRepository.count());
            model.addAttribute("demandes", demandeService.count());
            model.addAttribute("beneficiaires", demandeService.countAllDemandeValide());
            model.addAttribute("vsdrs", suiviDossierRegionService.getStatsRegion());
        }

        model.addAttribute("titre", titre);
        return "home";
    }
}
