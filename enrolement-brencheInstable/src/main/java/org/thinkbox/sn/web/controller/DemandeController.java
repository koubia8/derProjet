/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.controller;

import java.security.Principal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thinkbox.sn.constantes.EtatDemande;
import org.thinkbox.sn.domain.Demande;
import org.thinkbox.sn.domain.Equipement;
import org.thinkbox.sn.domain.Region;
import org.thinkbox.sn.domain.Responsable;
import org.thinkbox.sn.repository.DemandeRepository;
import org.thinkbox.sn.repository.RegionRepository;
import org.thinkbox.sn.repository.UserRepository;
import org.thinkbox.sn.security.AuthoritiesConstants;
import org.thinkbox.sn.service.DemandeService;
import org.thinkbox.sn.service.SecteurService;
import org.thinkbox.sn.service.dto.DemandeDTO;
import org.thinkbox.sn.service.mapper.DemandeMapper;
import org.thinkbox.sn.service.mapper.EquipementMapper;
import org.thinkbox.sn.service.mapper.ResponsableMapper;

/**
 *
 * @author guisse
 */
@Controller
public class DemandeController {

    private final DemandeService demandeService;

    private final SecteurService secteurService;

    private final RegionRepository regionRepository;

    private final DemandeRepository demandeRepository;

    private final ResponsableMapper responsableMapper;

    private final UserRepository userRepository;

    private final EquipementMapper equipementMapper;

    private final DemandeMapper demandeMapper;

    public DemandeController(DemandeService demandeService, SecteurService secteurService, RegionRepository regionRepository, DemandeRepository demandeRepository, ResponsableMapper responsableMapper, UserRepository userRepository, EquipementMapper equipementMapper, DemandeMapper demandeMapper) {
        this.demandeService = demandeService;
        this.secteurService = secteurService;
        this.regionRepository = regionRepository;
        this.demandeRepository = demandeRepository;
        this.responsableMapper = responsableMapper;
        this.userRepository = userRepository;
        this.equipementMapper = equipementMapper;
        this.demandeMapper = demandeMapper;
    }

    @RequestMapping(value = "demande/list", method = RequestMethod.GET)
    public String list(Model model, Pageable pageable, Principal principal) {
        Page<Demande> pages = demandeRepository.findAll(pageable, principal.getName());
        model.addAttribute("demandes", demandeMapper.toDto(pages.getContent()));
        return "demande/list";
    }
   /* 
    @RequestMapping(value = "demande/filtre", method = RequestMethod.GET)
    public String filtre(Model model,@RequestParam(required=false) String numero,@RequestParam(required=false)  String cni,@RequestParam(required=false)  String telephone) {  

        System.out.println("****"+demandeService.filterBy(numero,cni, telephone));
        model.addAttribute("demandes",demandeService.filterBy(numero, cni, telephone));
        
        return "demande/recherche";
    }*/
    
    
    @RequestMapping(value = "demande/all", method = RequestMethod.GET)
    @Secured(AuthoritiesConstants.ADMIN)
    public String allDemande(Model model, Pageable pageable) {
        Page<Demande> pages = demandeService.allDemande(pageable);
        model.addAttribute("demandes", demandeMapper.toDto(pages.getContent()));
        return "demande/all";
    }

    @RequestMapping(value = "demande/pending", method = RequestMethod.GET)
    @Secured(AuthoritiesConstants.ADMIN)
    public String pendingDemande(Model model, Pageable pageable) {
        Page<Demande> pages = demandeService.allDemandePending(pageable);
        model.addAttribute("demandes", demandeMapper.toDto(pages.getContent()));
        return "demande/pending";
    }

    @RequestMapping(value = "demande/rejected", method = RequestMethod.GET)
    @Secured(AuthoritiesConstants.ADMIN)
    public String rejectedDemande(Model model, Pageable pageable) {
        Page<Demande> pages = demandeService.allDemandeRejected(pageable);
        model.addAttribute("demandes", demandeMapper.toDto(pages.getContent()));
        return "demande/rejected";
    }

    @RequestMapping(value = "demande/valided", method = RequestMethod.GET)
    @Secured(AuthoritiesConstants.ADMIN)
    public String validedDemande(Model model, Pageable pageable) {
        Page<Demande> pages = demandeService.allDemandeValidet(pageable);
        model.addAttribute("demandes", demandeMapper.toDto(pages.getContent()));
        return "demande/valided";
    }

    @RequestMapping(value = "demande/fiche/{id}", method = RequestMethod.GET)
    public String fiche(@PathVariable Long id, Model model) {
        Demande demande = demandeRepository.findOne(id);
        Set<Responsable> set = demande.getResponsables();
        List<Responsable> responsables = set
                .stream()
                .collect(Collectors.toList());

        Set<Equipement> eqs = demande.getEquipements();
        List<Equipement> equipements = eqs
                .stream()
                .collect(Collectors.toList());
        Responsable responsable= responsables.get(0);
        //Region  region=regionRepository.findOne(demande.getRegion().getId());
        //System.out.println("region "+region);
        model.addAttribute("demandeId", demande.getId());
        model.addAttribute("demande", demandeMapper.toDto(demande));
        model.addAttribute("region",demande.getRegion().getNom());
        model.addAttribute("departement",demande.getDepartement().getNom());
        model.addAttribute("commune",demande.getCommune().getNom());
        model.addAttribute("secteur",demande.getSecteur().getNom());
        model.addAttribute("responsable",responsableMapper.toDto(responsable));
        model.addAttribute("equipements", equipementMapper.toDto(equipements));
        return "demande/fiche";
    }

    @RequestMapping(value = "demande/listtochange", method = RequestMethod.GET)
    public String listToChange(Model model) {
        model.addAttribute("demandes", demandeService.findAll());
        return "demande/listtochange";
    }

    @RequestMapping(value = "demande/uploadfile", method = RequestMethod.GET)
    public String uploadfile(Model model) {
        return "demande/uploadfile";
    }

    @RequestMapping(value = "demande/add", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("secteurs", secteurService.findAll());
        model.addAttribute("regions", regionRepository.findAll());
        return "demande/add";
    }

    @RequestMapping(value = "demande/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(required = true) Long id, Model model) {
        try {
            Demande demande = demandeRepository.findOne(id);
            if (!demande.getEtat().equals(EtatDemande.EN_COURS)) {
                return "redirect:/demande/list";
            }
            Set<Responsable> set = demande.getResponsables();
            List<Responsable> responsables = set
                    .stream()
                    .collect(Collectors.toList());

            Set<Equipement> eqs = demande.getEquipements();
            List<Equipement> equipements = eqs
                    .stream()
                    .collect(Collectors.toList());

            DemandeDTO dto = demandeMapper.toDto(demande);
            dto.setResponsablesDTO(responsableMapper.toDto(responsables));
            model.addAttribute("demandeId", demande.getId());
            model.addAttribute("secteurs", secteurService.findAll());
            model.addAttribute("regions", regionRepository.findAll());
            return "demande/edit";

        } catch (Exception e) {
            return "redirect:/demande/list";
        }
    }
    
    // method pour le recu d'enroulement
    @RequestMapping(value = "demande/recu/{id}", method = RequestMethod.GET)
    public String recu(@PathVariable Long id, Model model) {
        Demande demande = demandeRepository.findOne(id);
        Set<Responsable> set = demande.getResponsables();
        List<Responsable> responsables = set
                .stream()
                .collect(Collectors.toList());

        Set<Equipement> eqs = demande.getEquipements();
        List<Equipement> equipements = eqs
                .stream()
                .collect(Collectors.toList());
        Responsable responsable= responsables.get(0);
       
        model.addAttribute("demandeId", demande.getId());
        model.addAttribute("demande", demandeMapper.toDto(demande));
        model.addAttribute("region",demande.getRegion().getNom());
        model.addAttribute("departement",demande.getDepartement().getNom());
        model.addAttribute("commune",demande.getCommune().getNom());
        model.addAttribute("secteur",demande.getSecteur().getNom());
        model.addAttribute("responsable",responsableMapper.toDto(responsable));
        model.addAttribute("equipements", equipementMapper.toDto(equipements));
        return "demande/recu";
    }
}
