package org.thinkbox.sn.web.rest.errors;

public class LoginAlreadyUsedException extends BadRequestAlertException {

    public LoginAlreadyUsedException() {
        super(ErrorConstants.LOGIN_ALREADY_USED_TYPE, "Ce login est déja utilisé", "userManagement", "userexists");
    }
}
