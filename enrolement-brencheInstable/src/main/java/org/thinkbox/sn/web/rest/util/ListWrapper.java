/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.rest.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author guisse
 */
public class ListWrapper {
    
    private int action;
    
    private List<Long> ids = new ArrayList<>();

    public ListWrapper() {
    }

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    } 
}
