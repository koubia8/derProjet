package org.thinkbox.sn.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thinkbox.sn.domain.Commune;
import org.thinkbox.sn.domain.Departement;
import org.thinkbox.sn.domain.Partenaire;
import org.thinkbox.sn.domain.Region;
import org.thinkbox.sn.domain.TypePartenaire;
import org.thinkbox.sn.service.dto.PartenaireDTO;
import org.thinkbox.sn.repository.CommuneRepository;
import org.thinkbox.sn.repository.DepartementRepository;
import org.thinkbox.sn.repository.PartenaireRepository;
import org.thinkbox.sn.repository.RegionRepository;
import org.thinkbox.sn.repository.TypePartenaireRepository;

@Controller
public class PartenaireController {

    @Autowired
    private PartenaireRepository partenaireRepository;
    @Autowired
    RegionRepository repositoryRegion;

    @Autowired
    DepartementRepository repositoryDepartement;

    @Autowired
    CommuneRepository repositoryCommune;

    @Autowired
    TypePartenaireRepository repositoryTypePartenaire;

    @RequestMapping(value = "partenaire/add", method = RequestMethod.GET)
    public String addPartenaire(PartenaireDTO partenaireDTO, Model model) {
        model.addAttribute("typesPartenaires", repositoryTypePartenaire.findAll());
        model.addAttribute("regions", repositoryRegion.findAll());
        return "partenaire/add";
    }

    @RequestMapping(value = "/partenaire-update")
    public String edit(Model model, @RequestParam(name = "id") long id) {
        //model.addAttribute("allIntervention", interventionrepository.findAll());
        model.addAttribute("typesPartenaires", repositoryTypePartenaire.findAll());
        model.addAttribute("regions", repositoryRegion.findAll());
        model.addAttribute("partenaire", partenaireRepository.findOne(id));
        return "/partenaire/update";
    }

    @RequestMapping(value = "partenaire/active/{id}", method = RequestMethod.POST)
    public void active(@PathVariable("id") long id) {
        Partenaire partenaire = partenaireRepository.findOne(id);
        partenaire.setActivated(Boolean.TRUE);
        partenaireRepository.save(partenaire);
    }

    @RequestMapping(value = "partenaire/desactive/{id}", method = RequestMethod.POST)
    public void desactive(@PathVariable("id") long id) {
        Partenaire partenaire = partenaireRepository.findOne(id);
        partenaire.setActivated(Boolean.FALSE);
        partenaireRepository.save(partenaire);
    }

    @RequestMapping(value = "partenaire/add", method = RequestMethod.POST)
    public String save(PartenaireDTO partenaireDTO, BindingResult result, Model model, RedirectAttributes ra) {
        try {
            Partenaire partenaire = partenaireDTO.toEntity();
            Region region = repositoryRegion.findOne(partenaireDTO.getIdRegion());
            Departement dep = repositoryDepartement.findOne(partenaireDTO.getIdDepartement());
            Commune commune = repositoryCommune.findOne(partenaireDTO.getIdCommune());
            TypePartenaire tp = repositoryTypePartenaire.findOne(partenaireDTO.getIdTypePartenaire());

            partenaire.setRegion(region);
            partenaire.setDepartement(dep);
            partenaire.setCommune(commune);
            partenaire.setTypePartenaire(tp);
            partenaireRepository.save(partenaire);
        } catch (Exception e) {
            model.addAttribute("error", "echec de l'operation");
        }
        ra.addFlashAttribute("info", "Partenaire ajoute avec succes");
        return "redirect:/partenaire/list";
    }

    @RequestMapping(value = "partenaire/list", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("partenaireDTO", new PartenaireDTO());
        model.addAttribute("typesPartenaires", repositoryTypePartenaire.findAll());
        model.addAttribute("regions", repositoryRegion.findAll());
        model.addAttribute("partenaires", partenaireRepository.findAll());
        return "partenaire/list";
    }

    @RequestMapping(value = "partenaire/desative/list", method = RequestMethod.GET)
    public String listPartenaireDeactive(Model model, Pageable pageable) {
        model.addAttribute("partenaireDTO", new PartenaireDTO());
        model.addAttribute("typesPartenaires", repositoryTypePartenaire.findAll());
        model.addAttribute("regions", repositoryRegion.findAll());
        model.addAttribute("partenaires", partenaireRepository.findByActivated(Boolean.FALSE, pageable).getContent());
        return "partenaire/desative/list";
    }

    @RequestMapping(value = "partenaire/active/list", method = RequestMethod.GET)
    public String listPartenaireActive(Model model, Pageable pageable) {
        model.addAttribute("partenaireDTO", new PartenaireDTO());
        model.addAttribute("typesPartenaires", repositoryTypePartenaire.findAll());
        model.addAttribute("regions", repositoryRegion.findAll());
        // model.addAttribute("partenaires", partenaireRepository.findByActivatedTrue(pageable).getContent());
        model.addAttribute("partenaires", partenaireRepository.findByActivated(Boolean.TRUE, pageable).getContent());

        return "partenaire/active/list";
    }

}
