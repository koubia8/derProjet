/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.rest;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thinkbox.sn.service.StatistiqueService;
import org.thinkbox.sn.service.util.DiagrammeModel;

/**
 *
 * @author guisse
 */
@RestController
@RequestMapping("/api")
public class DiagrammeResource {

    private final StatistiqueService statistiqueService;

    public DiagrammeResource(StatistiqueService statistiqueService) {
        this.statistiqueService = statistiqueService;
    }

    @GetMapping("/diagramme")
    public List<DiagrammeModel> getDiagramme() {
        return statistiqueService.getDiagramme();
    }
}
