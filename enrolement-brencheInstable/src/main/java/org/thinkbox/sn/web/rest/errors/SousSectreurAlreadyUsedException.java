package org.thinkbox.sn.web.rest.errors;

public class SousSectreurAlreadyUsedException extends BadRequestAlertException {

    public SousSectreurAlreadyUsedException() {
        super(ErrorConstants.SOUS_SECTEUR_ALREADY_USED_TYPE, "Ce sous secteur existe  déja ", "sousSecteurManagement", "souSseteurexists");
    }
}
