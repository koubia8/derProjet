package org.thinkbox.sn.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.thinkbox.sn.repository.SecteurRepository;
import org.thinkbox.sn.service.dto.SecteurDTO;
import org.thinkbox.sn.service.SecteurService;
import org.thinkbox.sn.service.SousSecteurService;
import org.thinkbox.sn.service.dto.SousSecteurDTO;
import org.thinkbox.sn.web.rest.errors.BadRequestAlertException;
import org.thinkbox.sn.web.rest.errors.SectreurAlreadyUsedException;
import org.thinkbox.sn.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Secteur.
 */
@RestController
@RequestMapping("/api")
public class SecteurResource {

    private final Logger log = LoggerFactory.getLogger(SecteurResource.class);

    private static final String ENTITY_NAME = "secteur";

    private final SecteurService secteurService;
    private final SousSecteurService sousSecteurService;
    private final SecteurRepository secteurRepository;

    public SecteurResource(SecteurService secteurService, SousSecteurService sousSecteurService, SecteurRepository secteurRepository) {
        this.secteurService = secteurService;
        this.sousSecteurService = sousSecteurService;
        this.secteurRepository = secteurRepository;
    }

    

    
    @PostMapping("/secteurs")
    public ResponseEntity<SecteurDTO> createSecteur(@Valid @RequestBody SecteurDTO secteurDTO) throws URISyntaxException {
        log.debug("REST request to save Secteur : {}", secteurDTO);
        if (secteurDTO.getId() != null) {
            throw new BadRequestAlertException("A new secteur cannot already have an ID", ENTITY_NAME, "idexists");
        } else if (secteurRepository.findOneByNomIgnoreCase(secteurDTO.getNom()).isPresent()) {
            throw new SectreurAlreadyUsedException();
        }else{
        SecteurDTO result = secteurService.save(secteurDTO);
        return ResponseEntity.created(new URI("/api/secteurs/" + result.getId()))
            .headers(HeaderUtil.createAlert("secteur crée avec succès"))
            .body(result);
    }
    }

    @PutMapping("/secteurs")
    public ResponseEntity<SecteurDTO> updateSecteur(@Valid @RequestBody SecteurDTO secteurDTO) throws URISyntaxException {
        log.debug("REST request to update Secteur : {}", secteurDTO);
        if (secteurDTO.getId() == null) {
            return createSecteur(secteurDTO);
        }
        SecteurDTO result = secteurService.save(secteurDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createAlert("secteur modifié avec succès"))
            .body(result);
    }

    @GetMapping("/secteurs")
    public List<SecteurDTO> getAllSecteurs() {
        log.debug("REST request to get all Secteurs");
        return secteurService.findAll();
        }

    @GetMapping("/secteurs/{id}")
    public ResponseEntity<SecteurDTO> getSecteur(@PathVariable Long id) {
        log.debug("REST request to get Secteur : {}", id);
        SecteurDTO secteurDTO = secteurService.findOne(id);
        if(secteurDTO != null){
            return new ResponseEntity<>(secteurDTO, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/secteurs/{id}")
    public ResponseEntity<Void> deleteSecteur(@PathVariable Long id) {
        log.debug("REST request to delete Secteur : {}", id);
        secteurService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("secteur supprimé avec succès")).build();
    }
    @DeleteMapping("/secteur/desactive/{id}")
    public ResponseEntity<Void> desactive(@PathVariable("id") long id) {
        SecteurDTO secteur = secteurService.findOne(id);
        secteur.setActivated(Boolean.FALSE);
        secteurService.save(secteur);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("secteur desactivé avec succès")).build();
    }
    @DeleteMapping( "/secteur/active/{id}")
    public ResponseEntity<Void> activeSecteur(@PathVariable("id") long id) {
       SecteurDTO secteur = secteurService.findOne(id);
        secteur.setActivated(Boolean.TRUE);
        secteurService.save(secteur);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("secteur activé avec succès")).build();
    }
    
     @DeleteMapping("/sousecteur/active/{id}")
    public ResponseEntity<Void> activeSousecteu(@PathVariable("id") long id) {
        sousSecteurService.enableSousSecteur(id);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("sous secteur activé avec succès")).build();    
    }

    @DeleteMapping("/sousecteur/desactive/{id}")
    public ResponseEntity<Void> desactiveSousec(@PathVariable("id") long id) {  
         sousSecteurService.disableSousSecteur(id);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("sous secteur desactivé avec succès")).build();
        
    }
}
