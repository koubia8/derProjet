package org.thinkbox.sn.web.rest.errors;

public class EmailAlreadyUsedException extends BadRequestAlertException {

    public EmailAlreadyUsedException() {
        super(ErrorConstants.EMAIL_ALREADY_USED_TYPE, "Cet email est déja utilisé", "userManagement", "emailexists");
    }
}
