package org.thinkbox.sn.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thinkbox.sn.domain.Departement;
import org.thinkbox.sn.domain.Region;
import org.thinkbox.sn.service.dto.DepartementDTO;
import org.thinkbox.sn.repository.DepartementRepository;
import org.thinkbox.sn.repository.RegionRepository;

@Controller
public class DepartementController {
	
	@Autowired 
	RegionRepository repositoryRegion;
	
	@Autowired 
	DepartementRepository repositoryDepartement;
	
    @RequestMapping(value = "departement/list", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("departements", repositoryDepartement.findAll());
        return "departement/list";
    }
    
    @RequestMapping(value="departement/add",method=RequestMethod.GET)
   	public String add(DepartementDTO departementDTO,Model model){
    	model.addAttribute("regions", repositoryRegion.findAll());
   		return "departement/add";
   	}
    
    @RequestMapping(value="departement/add",method=RequestMethod.POST)
	public String add(DepartementDTO departementDTO,BindingResult result, Model model,RedirectAttributes ra){
		try {
			Region region = repositoryRegion.findOne(departementDTO.getIdRegion());
			Departement dep = departementDTO.toEntity();
			dep.setRegion(region);
			repositoryDepartement.save(dep);
			
		} catch (Exception e) {
			model.addAttribute("erreur","une erreur s'est produite lors de l'operation");
			return "departement/add";
		}
		ra.addFlashAttribute("success","Departement ajoute avec succes");
		return "redirect:/departement/list";
	}
	

}
