/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thinkbox.sn.config.Constants;
import org.thinkbox.sn.domain.User;
import org.thinkbox.sn.repository.UserRepository;
import org.thinkbox.sn.security.AuthoritiesConstants;
import org.thinkbox.sn.service.CustomMailSender;
import org.thinkbox.sn.service.UserService;
import org.thinkbox.sn.service.dto.UserDTO;
import org.thinkbox.sn.service.util.RandomUtil;
import org.thinkbox.sn.web.rest.errors.BadRequestAlertException;
import org.thinkbox.sn.web.rest.errors.EmailAlreadyUsedException;
import org.thinkbox.sn.web.rest.errors.LoginAlreadyUsedException;
import org.thinkbox.sn.web.rest.util.HeaderUtil;
import org.thinkbox.sn.web.rest.util.PaginationUtil;
import org.thinkbox.sn.web.rest.util.ResponseUtil;

/**
 *
 * @author guisse
 */
@RestController
@RequestMapping("/api")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    private final UserRepository userRepository;

    private final UserService userService;

    private final CustomMailSender mailSender;

    public UserResource(UserRepository userRepository, UserService userService, CustomMailSender mailSender) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.mailSender = mailSender;
    }

    /**
     * POST /users : Creates a new user.
     * <p>
     * Creates a new user if the login and email are not already used, and sends
     * an mail with an activation link. The user needs to be activated on
     * creation.
     *
     * @param userDTO the user to create
     * @return the ResponseEntity with status 201 (Created) and with body the
     * new user, or with status 400 (Bad Request) if the login or email is
     * already in use
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws BadRequestAlertException 400 (Bad Request) if the login or email
     * is already in use
     */
    @PostMapping("/users")
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<User> createUser(@Valid @RequestBody UserDTO userDTO) throws URISyntaxException {
        //log.debug("REST request to save User : {}", userDTO);

        if (userDTO.getId() != null) {
            throw new BadRequestAlertException("l'utilisateur existe", "userManagement", "idexists");
            // Lowercase the user login before comparing with database
        } else if (userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).isPresent()) {
            throw new LoginAlreadyUsedException();
        } else if (userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).isPresent()) {
            throw new EmailAlreadyUsedException();
        } else {
            String password = RandomUtil.generatePassword();
            User newUser = userService.createUser(userDTO, password);
            //mailService.sendCreationEmail(newUser);
            mailSender.SendMail("mail.thinkbox@gmail.com", newUser.getEmail(), "Mot de passe", "Votre compte a été crée :Login= " + newUser.getLogin() + " Password=" + password);

            return ResponseEntity.created(new URI("/api/users/" + newUser.getLogin()))
                    .headers(HeaderUtil.createAlert("Utilisateur créé avec succés! un mail est envoyé.", newUser.getLogin()))
                    .body(newUser);
        }
    }

    /**
     * PUT /users : Updates an existing User.
     *
     * @param userDTO the user to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     * user
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is
     * already in use
     * @throws LoginAlreadyUsedException 400 (Bad Request) if the login is
     * already in use
     */
    @PutMapping("/users")
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserDTO userDTO) {
        log.debug("REST request to update User : {}", userDTO);
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getId().equals(userDTO.getId()))) {
            throw new EmailAlreadyUsedException();
        }
        existingUser = userRepository.findOneByLogin(userDTO.getLogin().toLowerCase());
        if (existingUser.isPresent() && (!existingUser.get().getId().equals(userDTO.getId()))) {
            throw new LoginAlreadyUsedException();
        }
        Optional<UserDTO> updatedUser = userService.updateUser(userDTO);

        return ResponseUtil.wrapOrNotFound(updatedUser,
                HeaderUtil.createAlert("Utilisateur mis à jour avec succés!", userDTO.getLogin()));
    }

    /**
     * GET /users : get all users.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all users
     */
    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getAllUsers(Pageable pageable) {
        final Page<UserDTO> page = userService.getAllManagedUsers(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/users");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * @return a string list of the all of the roles
     */
    @GetMapping("/users/authorities")
    @Secured(AuthoritiesConstants.ADMIN)
    public List<String> getAuthorities() {
        return userService.getAuthorities();
    }

    /**
     * GET /users/:login : get the "login" user.
     *
     * @param login the login of the user to find
     * @return the ResponseEntity with status 200 (OK) and with body the "login"
     * user, or with status 404 (Not Found)
     */
    @GetMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
    public ResponseEntity<UserDTO> getUser(@PathVariable String login) {
        log.debug("REST request to get User : {}", login);
        return ResponseUtil.wrapOrNotFound(
                userService.getUserWithAuthoritiesByLogin(login)
                        .map(UserDTO::new));
    }

    /**
     * DELETE /users/:login : delete the "login" User.
     *
     * @param login the login of the user to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<Void> deleteUser(@PathVariable String login) {
        log.debug("REST request to delete User: {}", login);
        userService.deleteUser(login);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("Utilisateur supprimé avec succés!", login)).build();
    }
    
    @DeleteMapping("/users/disable/{login:" + Constants.LOGIN_REGEX + "}")
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<Void> disableUser(@PathVariable String login) {
        log.debug("REST request to disable User: {}", login);
        userService.disableUser(login);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("Utilisateur desactivé avec succés!", login)).build();
    }
    
    @DeleteMapping("/users/enable/{login:" + Constants.LOGIN_REGEX + "}")
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<Void> enableUser(@PathVariable String login) {
        log.debug("REST request to disable User: {}", login);
        userService.enableUser(login);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert("Utilisateur activé avec succès!", login)).build();
    }
    
    @PostMapping(path = "/users/reset-password")
    public ResponseEntity<User> changePassword(@RequestBody String mail) {
        String password = RandomUtil.generatePassword();
        Optional<User> user= userService.requestPasswordReset(mail,password);
        mailSender.SendMail("mail.thinkbox@gmail.com",mail, "Mot de passe", "Votre compte a été réinitialisé , votre nouveau mot de passe est : " + password);
        return ResponseUtil.wrapOrNotFound(user,
                HeaderUtil.createAlert("Mot de passe reinitialisé avec succès!", mail));
   }

}
