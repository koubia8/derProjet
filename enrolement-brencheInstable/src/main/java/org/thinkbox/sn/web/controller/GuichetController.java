/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.web.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.thinkbox.sn.domain.Guichet;
import org.thinkbox.sn.repository.GuichetRepository;
import org.thinkbox.sn.service.GuichetService;

/**
 *
 * @author guisse
 */
@Controller
public class GuichetController {
    
    private final GuichetService guichetService;

    private final GuichetRepository guichetRepository;

    public GuichetController(GuichetService guichetService, GuichetRepository guichetRepository) {
        this.guichetService = guichetService;
        this.guichetRepository = guichetRepository;
    }

    @RequestMapping(value = "guichet/list", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("guichets", guichetService.findAll());
        return "guichet/list";
    } 
    
   
    @RequestMapping(value = "/guichets-add", method = RequestMethod.GET)
    public String joutGuichet(Model model) {
        model.addAttribute("guichet", new Guichet());
        return "/guichet/add";
    }
    
      @RequestMapping(value = "/guichet-update")
    public String edit(Model model, @RequestParam(name = "id") Long id) {
        //model.addAttribute("allIntervention", interventionrepository.findAll());
        model.addAttribute("guichet", guichetRepository.findOne(id));
        return "/guichet/modif_guichet";
    }
    
    
    @RequestMapping(value = "guichet/active/{id}", method = RequestMethod.POST)
	public void active (@PathVariable("id") long id) {
                Guichet guichet = guichetRepository.findOne(id);
                guichet.setActivated(Boolean.TRUE);
		guichetRepository.save(guichet);
	}
	@RequestMapping(value = "guichet/desactive/{id}", method = RequestMethod.POST)
	public void desactive (@PathVariable("id") long id) {
                Guichet guichet = guichetRepository.findOne(id);
                guichet.setActivated(Boolean.FALSE);
		guichetRepository.save(guichet);
	}

    @RequestMapping(value = "guichet/active/list", method = RequestMethod.GET)
    public String listGuichetActive(Model model,Pageable pageable) {
        model.addAttribute("guichets", guichetRepository.findByActivated(Boolean.TRUE, pageable));
        return "guichet/active/list";
    } 

    @RequestMapping(value = "guichet/desactive/list", method = RequestMethod.GET)
    public String listGuichetDesactive(Model model,Pageable pageable) {
       model.addAttribute("guichets", guichetRepository.findByActivated(Boolean.FALSE, pageable));
        return "guichet/desactive/list";
    } 

}
