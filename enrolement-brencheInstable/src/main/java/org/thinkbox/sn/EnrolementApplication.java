package org.thinkbox.sn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnrolementApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnrolementApplication.class, args);
	}
}
