package org.thinkbox.sn.domain.enumeration;

/**
 * The Sexe enumeration.
 */
public enum Sexe {
    HOMME, FEMME
}
