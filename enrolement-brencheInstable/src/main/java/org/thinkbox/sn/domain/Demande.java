/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.thinkbox.sn.constantes.EtatDemande;
import org.thinkbox.sn.domain.enumeration.TypeDemande;
import org.thinkbox.sn.domain.enumeration.FormeJuridique;

/**
 *
 * @author guisse
 */
@Entity
@Table(name = "demande")
public class Demande extends AbstractAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @NotNull
//    @Column(name = "date_demande", nullable = false,updatable = false)
//    @Temporal(TemporalType.TIMESTAMP)
//    private final Date dateDemande = new Date();

    @NotNull
    @Column(name = "numero", nullable = false,updatable = false)
    private String numero;
    
    @NotNull
    @Column(name = "description")
    @Lob
    private String description;

    @NotNull
    @Column(name = "montant", nullable = false)
    private Double montant;

    @Column(name = "score",nullable = false)
    private Float score;

    @NotNull
    @Column(name = "accepted", nullable = false)
    private Boolean accepted = false;

    @Column(name = "etat", length = 50)
    private String etat = EtatDemande.EN_COURS;

    @Column(name = "email")
    private String email;
    
    @Column(name = "business_plan")
    private String businessPlan;

    @Column(name = "site_web")
    private String siteWeb;

    @Column(name = "ninea")
    private String ninea = "N/A";

    @Column(name = "affliation")
    private String affliation = "N/A";

    @Column(name = "rccm")
    private String rccm = "N/A";

    @Column(name = "telephone1")
    private String telephone1;
    
    @Column(name = "telephone2")
    private String telephone2;
    
    @Column(name = "telephone3")
    private String telephone3;
    
    @Column(name = "nombre_membre")
    private Integer nombreMembre=1;
    
    @Column(name = "activites")
    @Lob
    private String activites;
    
    @Column(name = "produits_services")
    @Lob
    private String produitsServices;
    
    @Column(name = "prospects")
    private Boolean prospects;
    
    @Column(name = "clients")
    private Boolean clients;
    
    @Column(name = "nombre_homme")
    private Integer nombreHomme;
    
    @Column(name = "nombre_femme")
    private Integer nombreFemme;
    
    @Column(name = "compte_if")
    private Boolean compteIf;
    
    @Column(name = "credit_if")
    private Boolean creditIf;
    
    @Column(name = "financement")
    private Boolean financement;
    
    @Column(name = "nom_projet",length = 50)
    private String nomProjet;
    
    //qualification et l'activite et la comptence
    @Column(name = "adequation_q_a",length = 50)
    private String adequationQA;
    
     //adequation entre la competene et l'activete
    @Column(name = "adequation_c_a",length = 50)
    private String adequationCA;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type_demande", nullable = false)
    private TypeDemande typeDemande;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "forme_juridique", nullable = false)
    private FormeJuridique formeJurdique;

    @OneToMany(mappedBy = "demande",cascade = {CascadeType.ALL})
    @JsonIgnore
    private Set<Responsable> responsables = new HashSet<>();
    
    @OneToMany(mappedBy = "demande",cascade = {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE})
    @JsonIgnore
    private Set<Equipement> equipements = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    private Region region;

    @ManyToOne(fetch = FetchType.LAZY)
    private Departement departement;

    @ManyToOne(fetch = FetchType.LAZY)
    private Commune commune;

    @ManyToOne(fetch = FetchType.LAZY)
    private Secteur secteur;

    public Demande() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescription() {
        return description;
    }

    public String getBusinessPlan() {
        return businessPlan;
    }

    public void setBusinessPlan(String businessPlan) {
        this.businessPlan = businessPlan;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public String getNinea() {
        return ninea;
    }

    public void setNinea(String ninea) {
        this.ninea = ninea;
    }

    public String getAffliation() {
        return affliation;
    }

    public void setAffliation(String affliation) {
        this.affliation = affliation;
    }

    public String getRccm() {
        return rccm;
    }

    public void setRccm(String rccm) {
        this.rccm = rccm;
    }

    public String getTelephone1() {
        return telephone1;
    }

    public void setTelephone1(String telephone1) {
        this.telephone1 = telephone1;
    }

    public String getTelephone2() {
        return telephone2;
    }

    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }

    public String getTelephone3() {
        return telephone3;
    }

    public void setTelephone3(String telephone3) {
        this.telephone3 = telephone3;
    }

    public TypeDemande getTypeDemande() {
        return typeDemande;
    }

    public void setTypeDemande(TypeDemande typeDemande) {
        this.typeDemande = typeDemande;
    }

    public FormeJuridique getFormeJurdique() {
        return formeJurdique;
    }

    public void setFormeJurdique(FormeJuridique formeJurdique) {
        this.formeJurdique = formeJurdique;
    }

    public Set<Responsable> getResponsables() {
        return responsables;
    }

    public void setResponsables(Set<Responsable> responsables) {
        this.responsables = responsables;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    public Commune getCommune() {
        return commune;
    }

    public void setCommune(Commune commune) {
        this.commune = commune;
    }

    public Secteur getSecteur() {
        return secteur;
    }

    public void setSecteur(Secteur secteur) {
        this.secteur = secteur;
    }
    
    public void addResonsable(Responsable responsable){
        responsables.add(responsable);
        responsable.setDemande(this);
    }
    
    public void removeResponsable(Responsable responsable){
        responsables.remove(responsable);
        responsable.setDemande(null);
    }
    
     public void addEquipement(Equipement equipement){
        equipements.add(equipement);
        equipement.setDemande(this);
    }
    
    public void removeEquipement(Equipement equipement){
        equipements.remove(equipement);
        equipement.setDemande(null);
    }

    public Set<Equipement> getEquipements() {
        return equipements;
    }

    public void setEquipements(Set<Equipement> equipements) {
        this.equipements = equipements;
    }

    public Integer getNombreMembre() {
        return nombreMembre;
    }

    public void setNombreMembre(Integer nombreMembre) {
        this.nombreMembre = nombreMembre;
    }

    public String getActivites() {
        return activites;
    }

    public void setActivites(String activites) {
        this.activites = activites;
    }

    public String getProduitsServices() {
        return produitsServices;
    }

    public void setProduitsServices(String produitsServices) {
        this.produitsServices = produitsServices;
    }

    public Boolean getProspects() {
        return prospects;
    }

    public void setProspects(Boolean prospects) {
        this.prospects = prospects;
    }

    public Boolean getClients() {
        return clients;
    }

    public void setClients(Boolean clients) {
        this.clients = clients;
    }

    public Integer getNombreHomme() {
        return nombreHomme;
    }

    public void setNombreHomme(Integer nombreHomme) {
        this.nombreHomme = nombreHomme;
    }

    public Integer getNombreFemme() {
        return nombreFemme;
    }

    public void setNombreFemme(Integer nombreFemme) {
        this.nombreFemme = nombreFemme;
    }

    public Boolean getCompteIf() {
        return compteIf;
    }

    public void setCompteIf(Boolean compteIf) {
        this.compteIf = compteIf;
    }

    public Boolean getCreditIf() {
        return creditIf;
    }

    public void setCreditIf(Boolean creditIf) {
        this.creditIf = creditIf;
    }

    public Boolean getFinancement() {
        return financement;
    }

    public void setFinancement(Boolean financement) {
        this.financement = financement;
    }

    public String getNomProjet() {
        return nomProjet;
    }

    public void setNomProjet(String nomProjet) {
        this.nomProjet = nomProjet;
    }

    public String getAdequationQA() {
        return adequationQA;
    }

    public void setAdequationQA(String adequationQA) {
        this.adequationQA = adequationQA;
    }

    public String getAdequationCA() {
        return adequationCA;
    }

    public void setAdequationCA(String adequationCA) {
        this.adequationCA = adequationCA;
    }
    
    
}
