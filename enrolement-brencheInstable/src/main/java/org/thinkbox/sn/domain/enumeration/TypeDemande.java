package org.thinkbox.sn.domain.enumeration;

public enum TypeDemande {
	INDIVIDUEL, COLLECTIF
}
