package org.thinkbox.sn.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Secteur.
 */
@Entity
@Table(name = "secteur")
public class Secteur extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nom_secteur", nullable = false)
    private String nom;
    
   
    @Column(name = "description_secteur")
    private String description;
    
    @Column(name = "verified")
    private Boolean verified;
     
    @OneToMany(mappedBy = "secteur")
    @JsonIgnore
    private Set<SousSecteur> sousSecteurs = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public Secteur nomSecteur(String nomSecteur) {
        this.nom = nomSecteur;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public void addSousSecteur(SousSecteur sousSecteur){
        sousSecteurs.add(sousSecteur);
        sousSecteur.setSecteur(this);
    }
    
    public void removeSousSecteur(SousSecteur sousSecteur){
        sousSecteurs.remove(sousSecteur);
        sousSecteur.setSecteur(null);
    }

    public Set<SousSecteur> getSousSecteurs() {
        return sousSecteurs;
    }

    public void setSousSecteurs(Set<SousSecteur> sousSecteurs) {
        this.sousSecteurs = sousSecteurs;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
    

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Secteur secteur = (Secteur) o;
        if (secteur.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), secteur.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Secteur{" +
            "id=" + getId() +
            ", nomSecteur='" + getNom() + "'" +
            "}";
    }
}
