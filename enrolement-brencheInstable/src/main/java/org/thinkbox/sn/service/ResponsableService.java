/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.domain.Responsable;
import org.thinkbox.sn.service.dto.ResponsableDTO;
import org.thinkbox.sn.repository.ResponsableRepository;
import org.thinkbox.sn.service.mapper.ResponsableMapper;

/**
 *
 * @author guisse
 */
@Service
public class ResponsableService {
    
     private final ResponsableRepository responsableRepository;
    
    private final ResponsableMapper responsableMapper;

    public ResponsableService(ResponsableRepository responsableRepository, ResponsableMapper responsableMapper) {
        this.responsableRepository = responsableRepository;
        this.responsableMapper = responsableMapper;
    }
    
    public ResponsableDTO save(ResponsableDTO responsableDTO){
        Responsable responsable = responsableMapper.toEntity(responsableDTO);
        responsable = responsableRepository.save(responsable);
        return responsableMapper.toDto(responsable);
    }
    
    @Transactional(readOnly = true)
    public List<ResponsableDTO> findAll() {
        return responsableRepository.findAll().stream()
            .map(responsableMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    @Transactional(readOnly = true)
    public ResponsableDTO findOne(Long id) {
        Responsable responsable = responsableRepository.findOne(id);
        return responsableMapper.toDto(responsable);
    }

    public void delete(Long id) {
        responsableRepository.delete(id);
    }
    
}
