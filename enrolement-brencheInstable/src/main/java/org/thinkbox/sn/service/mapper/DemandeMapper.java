/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.mapper;

import org.springframework.stereotype.Service;
import org.thinkbox.sn.domain.Demande;
import org.thinkbox.sn.service.dto.DemandeDTO;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.thinkbox.sn.domain.Commune;
import org.thinkbox.sn.domain.Departement;
import org.thinkbox.sn.domain.Equipement;
import org.thinkbox.sn.domain.Region;
import org.thinkbox.sn.domain.Responsable;
import org.thinkbox.sn.domain.Secteur;
import org.thinkbox.sn.domain.enumeration.FormeJuridique;
import org.thinkbox.sn.domain.enumeration.TypeDemande;
import org.thinkbox.sn.service.dto.ResponsableDTO;
/**
 *
 * @author guisse
 */
@Service
public class DemandeMapper implements EntityMapper<DemandeDTO, Demande>{
    
    private final RegionMapper regionMapper;
    
    private final DepartementMapper departementMapper;
    
    private final CommuneMapper communeMapper;
    
    private final SecteurMapper secteurMapper;
    
    private final ResponsableMapper responsableMapper;
    
    private final EquipementMapper equipementMapper;

    public DemandeMapper(RegionMapper regionMapper, DepartementMapper departementMapper, CommuneMapper communeMapper, SecteurMapper secteurMapper, ResponsableMapper responsableMapper, EquipementMapper equipementMapper) {
        this.regionMapper = regionMapper;
        this.departementMapper = departementMapper;
        this.communeMapper = communeMapper;
        this.secteurMapper = secteurMapper;
        this.responsableMapper = responsableMapper;
        this.equipementMapper = equipementMapper;
    }

    
    @Override
    public Demande toEntity(DemandeDTO dto) {
        if(dto == null){
            return null;
        }
        Demande demande = new Demande();
        if(dto.getId() == null){
            if(!dto.getResponsablesDTO().isEmpty()){
                List<Responsable> responsables = responsableMapper.toEntity(dto.getResponsablesDTO());
                responsables.forEach(r->{
                demande.addResonsable(r);
            });
            }
            
            if(!dto.getEquipementsDTO().isEmpty()){
                List<Equipement> equipements = equipementMapper.toEntity(dto.getEquipementsDTO());
            equipements.forEach(e->{
                demande.addEquipement(e);
            });
            }
            
        }
        Region region = regionMapper.fromID(dto.getRegionId());
        Departement departement = departementMapper.fromID(dto.getDepartementId());
        Commune commune = communeMapper.fromID(dto.getCommuneId());
        Secteur secteur = secteurMapper.fromID(dto.getSecteurId());
        demande.setRegion(region);
        demande.setDepartement(departement);
        demande.setCommune(commune);
        demande.setSecteur(secteur);
        demande.setId(dto.getId());
        demande.setMontant(dto.getMontant());
        demande.setFormeJurdique(FormeJuridique.valueOf(dto.getFormeJuridique()));
        demande.setTypeDemande(TypeDemande.valueOf(dto.getTypeDemande()));
        demande.setSiteWeb(dto.getSiteWeb());
        demande.setNinea(dto.getNinea());
        demande.setRccm(dto.getRccm());
        demande.setAffliation(dto.getAffliation());
        demande.setDescription(dto.getDescription());
        demande.setNumero(dto.getNumero());
        demande.setTelephone1(dto.getTelephone1());
        demande.setTelephone2(dto.getTelephone2());
        demande.setTelephone3(dto.getTelephone3());
        demande.setActivites(dto.getActivites());
        demande.setProduitsServices(dto.getProduitsServices());
        demande.setProspects(dto.getProspects());
        demande.setClients(dto.getClients());
        demande.setNombreHomme(dto.getNombreHomme());
        demande.setNombreFemme(dto.getNombreFemme());
        demande.setCompteIf(dto.getCompteIf());
        demande.setCreditIf(dto.getCreditIf());
        demande.setFinancement(dto.getFinancement());
        demande.setNomProjet(dto.getNomProjet());
        demande.setAdequationQA(dto.getAdequationQA());
        demande.setAdequationCA(dto.getAdequationCA());
        
        if(dto.getNombreMembre() != null){
            demande.setNombreMembre(dto.getNombreMembre());
        }
        demande.setScore(setScore(dto));
        return demande;
    }

    @Override
    public DemandeDTO toDto(Demande entity) {
        
        if(entity == null){
            return null;
        }
        DemandeDTO demandeDTO =new DemandeDTO();
        demandeDTO.setRegionId(entity.getRegion().getId());
        demandeDTO.setDepartementId(entity.getDepartement().getId());
        demandeDTO.setCommuneId(entity.getCommune().getId());
        if(null != entity.getSecteur()){
          demandeDTO.setSecteurId(entity.getSecteur().getId());  
        }
        
        demandeDTO.setId(entity.getId());
        demandeDTO.setMontant(entity.getMontant());
        demandeDTO.setFormeJuridique(entity.getFormeJurdique().name());
        demandeDTO.setTypeDemande(entity.getTypeDemande().name());
        demandeDTO.setSiteWeb(entity.getSiteWeb());
        demandeDTO.setNinea(entity.getNinea());
        demandeDTO.setRccm(entity.getRccm());
        demandeDTO.setAffliation(entity.getAffliation());
        demandeDTO.setNumero(entity.getNumero());
        demandeDTO.setEtat(entity.getEtat());
        demandeDTO.setDescription(entity.getDescription());
        demandeDTO.setScore(entity.getScore());
        demandeDTO.setNombreMembre(entity.getNombreMembre());
        demandeDTO.setCreatedDate(entity.getCreatedDate());
        demandeDTO.setCreatedBy(entity.getCreatedBy());
        
        demandeDTO.setActivites(entity.getActivites());
        demandeDTO.setProduitsServices(entity.getProduitsServices());
        demandeDTO.setProspects(entity.getProspects());
        demandeDTO.setClients(entity.getClients());
        demandeDTO.setNombreHomme(entity.getNombreHomme());
        demandeDTO.setNombreFemme(entity.getNombreFemme());
        demandeDTO.setCompteIf(entity.getCompteIf());
        demandeDTO.setCreditIf(entity.getCreditIf());
        demandeDTO.setFinancement(entity.getFinancement());
        
        demandeDTO.setTelephone1(entity.getTelephone1());
        demandeDTO.setTelephone2(entity.getTelephone2());
        demandeDTO.setTelephone3(entity.getTelephone3());
        demandeDTO.setNomProjet(entity.getNomProjet());
        demandeDTO.setAdequationQA(entity.getAdequationQA());
        demandeDTO.setAdequationCA(entity.getAdequationCA());
        
        return demandeDTO;
    }

    @Override
    public List<Demande> toEntity(List<DemandeDTO> dtoList) {
        return dtoList.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<DemandeDTO> toDto(List<Demande> entityList) {
        return entityList.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }   
    
    public Demande fromId(Long id){
        if(id == null){
            return null;
        }
        Demande demande = new Demande();
        demande.setId(id);
        return demande;
    }
    
    public Float setScore(DemandeDTO dto){
        ResponsableDTO r = dto.getResponsablesDTO().get(0);
        int totalCoef = 3 + 4;

    	Float competence = (float) (3*r.getQualification() + 4*r.getExperience());
    	Float lotalNote = competence;
    	Float res = (lotalNote / totalCoef);
        
        return res;
    }
}
