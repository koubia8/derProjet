/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.thinkbox.sn.domain.Demande;
import org.thinkbox.sn.domain.Responsable;
import org.thinkbox.sn.domain.enumeration.Sexe;
import org.thinkbox.sn.service.dto.ResponsableDTO;

/**
 *
 * @author guisse
 */
@Service
public class ResponsableMapper implements EntityMapper<ResponsableDTO, Responsable>{
     
    @Override
    public Responsable toEntity(ResponsableDTO dto) {
        if(dto == null){
            return null;
        }
        Responsable responsable = new Responsable();
        if(dto.getDemandeId() != null){
            Demande demande = new Demande();
            demande.setId(dto.getDemandeId());
            responsable.setDemande(demande);
        }
        responsable.setId(dto.getId());
        responsable.setNom(dto.getNom());
        responsable.setPrenom(dto.getPrenom());
        responsable.setCni(dto.getCni());
        responsable.setFonction(dto.getFonction());
        responsable.setSexe(Sexe.valueOf(dto.getSexe()));
        responsable.setDateNaissance(dto.getDateNaissance());
        responsable.setExperience(dto.getExperience());
        responsable.setQualification(dto.getQualification());
        responsable.setAdresse(dto.getAdresse());
        responsable.setLieuNaissance(dto.getLieuNaissance());
        responsable.setTelephone(dto.getTelephone());
        responsable.setEmail(dto.getEmail());
        responsable.setTelephone2(dto.getTelephone2());
        responsable.setTelephoneProche(dto.getTelephoneProche());
        responsable.setSiteWeb(dto.getSiteWeb());
        return responsable;
    }

    @Override
    public ResponsableDTO toDto(Responsable entity) {
        
        if(entity == null){
            return null;
        }
        ResponsableDTO responsableDTO =new ResponsableDTO();
        responsableDTO.setId(entity.getId());
        responsableDTO.setNom(entity.getNom());
        responsableDTO.setPrenom(entity.getPrenom());
        responsableDTO.setCni(entity.getCni());
        responsableDTO.setFonction(entity.getFonction());
        responsableDTO.setSexe(entity.getSexe().name());
        responsableDTO.setDateNaissance(entity.getDateNaissance());
        responsableDTO.setExperience(entity.getExperience());
        responsableDTO.setQualification(entity.getQualification());
        responsableDTO.setAdresse(entity.getAdresse());
        responsableDTO.setLieuNaissance(entity.getLieuNaissance());
        responsableDTO.setTelephone(entity.getTelephone());
        responsableDTO.setEmail(entity.getEmail());
        responsableDTO.setTelephone2(entity.getTelephone2());
        responsableDTO.setTelephoneProche(entity.getTelephoneProche());
        responsableDTO.setSiteWeb(entity.getSiteWeb());
        return responsableDTO;
    }

    @Override
    public List<Responsable> toEntity(List<ResponsableDTO> dtoList) {
        return dtoList.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<ResponsableDTO> toDto(List<Responsable> entityList) {
        return entityList.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }
    
}
