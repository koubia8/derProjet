/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.thinkbox.sn.domain.Secteur;
import org.thinkbox.sn.domain.SousSecteur;
import org.thinkbox.sn.service.dto.SousSecteurDTO;
import org.thinkbox.sn.repository.SecteurRepository;
import org.thinkbox.sn.repository.SousSecteurRepository;

/**
 *
 * @author guisse
 */
@Service
public class SousSecteurMapper implements EntityMapper<SousSecteurDTO, SousSecteur>{
    
    private final SecteurRepository secteurRepository;
    
    private final SousSecteurRepository sousSecteurRepository;
    
    private final SecteurMapper secteurMapper;

    public SousSecteurMapper(SecteurRepository secteurRepository, SousSecteurRepository sousSecteurRepository, SecteurMapper secteurMapper) {
        this.secteurRepository = secteurRepository;
        this.sousSecteurRepository = sousSecteurRepository;
        this.secteurMapper = secteurMapper;
    }

    
    @Override
    public SousSecteur toEntity(SousSecteurDTO dto) {
        if(dto == null){
            return null;
        }
        SousSecteur sousSecteur = new SousSecteur();
        Secteur secteur = secteurMapper.fromID(dto.getSecteurId());
        sousSecteur.setSecteur(secteur);
        sousSecteur.setId(dto.getId());
        sousSecteur.setNom(dto.getNom());
        sousSecteur.setActivated(dto.isActive());
        sousSecteur.setDescription(dto.getDescription());
        return sousSecteur;
    }

    @Override
    public SousSecteurDTO toDto(SousSecteur entity) {
        
        if(entity == null){
            return null;
        }
        SousSecteurDTO sousSecteurDTO =new SousSecteurDTO();
        sousSecteurDTO.setId(entity.getId());
        sousSecteurDTO.setNom(entity.getNom());
        sousSecteurDTO.setActive(entity.getActivated());
        sousSecteurDTO.setDescription(entity.getDescription());
        return sousSecteurDTO;
    }

    @Override
    public List<SousSecteur> toEntity(List<SousSecteurDTO> dtoList) {
        return dtoList.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<SousSecteurDTO> toDto(List<SousSecteur> entityList) {
        return entityList.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }
    
    public SousSecteur sousSecteurFromId(Long id){
        if(id == null){
            return null;
        }
        SousSecteur sousSecteur = new SousSecteur();
        sousSecteur.setId(id);
        return sousSecteur;
    }
}
