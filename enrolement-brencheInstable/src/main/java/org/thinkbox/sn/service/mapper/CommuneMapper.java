/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.thinkbox.sn.domain.Commune;
import org.thinkbox.sn.domain.Departement;
import org.thinkbox.sn.service.dto.CommuneDTO;

/**
 *
 * @author guisse
 */
@Service
public class CommuneMapper implements EntityMapper<CommuneDTO, Commune>{
    
    private final DepartementMapper departementMapper;

    public CommuneMapper(DepartementMapper departementMapper) {
        this.departementMapper = departementMapper;
    }
    
    
       @Override
    public Commune toEntity(CommuneDTO dto) {
        if(dto == null){
            return null;
        }
        Commune commune = new Commune();
        Departement departent = departementMapper.fromID(dto.getIdDepartement());
        commune.setDepartement(departent);
        commune.setId(dto.getId());
        commune.setNom(dto.getNom());
        return commune;
    }

    @Override
    public CommuneDTO toDto(Commune entity) {
        
        if(entity == null){
            return null;
        }
        CommuneDTO communeDTO =new CommuneDTO();
        communeDTO.setId(entity.getId());
        communeDTO.setNom(entity.getNom());
        return communeDTO;
    }

    @Override
    public List<Commune> toEntity(List<CommuneDTO> dtoList) {
        return dtoList.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<CommuneDTO> toDto(List<Commune> entityList) {
        return entityList.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }
    
    public Commune fromID(Long id){
        if(id == null){
            return null;
        }
        Commune commune = new Commune();
        commune.setId(id);
        return commune;
    }
    
}
