/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.domain.Equipement;
import org.thinkbox.sn.service.dto.EquipementDTO;
import org.thinkbox.sn.repository.EquipementRepository;
import org.thinkbox.sn.service.mapper.EquipementMapper;

/**
 *
 * @author guisse
 */
@Service
public class EquipementService {

    private final EquipementRepository equipementRepository;

    private final EquipementMapper equipementMapper;

    public EquipementService(EquipementRepository equipementRepository, EquipementMapper equipementMapper) {
        this.equipementRepository = equipementRepository;
        this.equipementMapper = equipementMapper;
    }

    public EquipementDTO save(EquipementDTO equipementDTO) {
        Equipement equipement = equipementMapper.toEntity(equipementDTO);
        equipement = equipementRepository.save(equipement);
        return equipementMapper.toDto(equipement);
    }

    @Transactional(readOnly = true)
    public List<EquipementDTO> findAll() {
        return equipementRepository.findAll().stream()
                .map(equipementMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Transactional(readOnly = true)
    public EquipementDTO findOne(Long id) {
        Equipement equipement = equipementRepository.findOne(id);
        return equipementMapper.toDto(equipement);
    }

    public void delete(Long id) {
        equipementRepository.delete(id);
    }

}
