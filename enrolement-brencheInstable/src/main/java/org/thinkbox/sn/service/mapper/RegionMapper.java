/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.thinkbox.sn.domain.Region;
import org.thinkbox.sn.service.dto.RegionDTO;

/**
 *
 * @author guisse
 */
@Service
public class RegionMapper implements EntityMapper<RegionDTO, Region>{
    
    @Override
    public Region toEntity(RegionDTO dto) {
        if(dto == null){
            return null;
        }
        Region region = new Region();
        region.setId(dto.getId());
        region.setNom(dto.getNom());
        return region;
    }

    @Override
    public RegionDTO toDto(Region entity) {
        
        if(entity == null){
            return null;
        }
        RegionDTO regionDTO =new RegionDTO();
        regionDTO.setId(entity.getId());
        regionDTO.setNom(entity.getNom());
        return regionDTO;
    }

    @Override
    public List<Region> toEntity(List<RegionDTO> dtoList) {
        return dtoList.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<RegionDTO> toDto(List<Region> entityList) {
        return entityList.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }
    
    public Region fromID(Long id){
        if(id == null){
            return null;
        }
        Region region = new Region();
        region.setId(id);
        return region;
    }
    
}
