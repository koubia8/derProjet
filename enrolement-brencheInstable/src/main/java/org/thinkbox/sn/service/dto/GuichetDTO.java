/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.dto;

import java.io.Serializable;

/**
 *
 * @author guisse
 */
public class GuichetDTO implements Serializable {

    private Long id;

    private String nom;

    private Double max;

    private boolean active;
    

    public GuichetDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

}
