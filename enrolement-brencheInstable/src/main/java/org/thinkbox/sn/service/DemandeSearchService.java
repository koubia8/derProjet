/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Service;
import org.thinkbox.sn.service.util.DemandeSearchModel;

/**
 *
 * @author guisse
 */
@Service
public class DemandeSearchService {

    private static final String VIEW_SEARCH_DEMANDE = "view_search_demande";

    private static final String QUERY_SEARCH_DEMANDE = "SELECT * FROM " + VIEW_SEARCH_DEMANDE + " WHERE numero =:numero or cni =:cni or telephone=:telephone LIMIT 10";

    private final EntityManager em;

    public DemandeSearchService(EntityManager em) {
        this.em = em;
    }

    public List<DemandeSearchModel> getDemandes(String numero,String cni,String telephone) {
        try {
            List<Object[]> objects = em.createNativeQuery(QUERY_SEARCH_DEMANDE)
                    .setParameter("numero",numero)
                    .setParameter("cni",cni)
                    .setParameter("telephone",telephone)
                    .getResultList();
            List<DemandeSearchModel> models = new ArrayList<>();
            objects.stream().map((object) -> {
                DemandeSearchModel model = new DemandeSearchModel();
                model.setId(((BigInteger) object[0]));
                model.setDateDemande((Date) object[1]);
                model.setNumero((String) object[2]);
                model.setMontant((Double) object[3]);
                model.setEtat((String) object[4]);
                model.setNomPorteurProjet((String) object[5]);
                model.setPrenomPorteurProjet((String) object[6]);
                model.setSexePorteurProjet((String) object[7]);
                model.setTelephonePorteurProjet((String) object[8]);
                model.setCniPorteurProjet((String) object[9]);
                return model;
            }).forEachOrdered((model) -> {
                models.add(model);
            });
            return models;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}
