package org.thinkbox.sn.service.dto;

import java.io.Serializable;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class CustomUser implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message="ce champ est obigatoire")
	@Email(message="email non pas valide")
	private String email;

	public CustomUser() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
