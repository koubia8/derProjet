package org.thinkbox.sn.service.dto;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.thinkbox.sn.domain.Secteur;

public class SecteurDTO implements Serializable {

    private Long id;

    
    @NotEmpty
   // @Length(min = 0, message = "xxxxxxxxxxxxxxxxxxxxxxxxxxx")
    private String nom;
    
    private String description;
    
    private boolean activated;
   
    private Boolean verified;

    public SecteurDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }
 
 
    

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public Secteur toEntity() {
        Secteur entity = new Secteur();
        entity.setNom(this.nom);
        entity.setDescription(this.description);
        return entity;
    }

}
