package org.thinkbox.sn.service.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.thinkbox.sn.domain.Region;

public class RegionDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RegionDTO() {
	}

	private Long id;
	
	private String nom;
	
	public Region toEntity(RegionDTO dto) {
		Region entity = new Region();
		entity.setNom(dto.getNom());
		
		return entity;
	}
	
	private List<DepartementDTO> departements= new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<DepartementDTO> getDepartements() {
		return departements;
	}

	public void setDepartements(List<DepartementDTO> departements) {
		this.departements = departements;
	}
}
	