/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.constantes.EtatDemande;
import org.thinkbox.sn.domain.Commune;
import org.thinkbox.sn.domain.Demande;
import org.thinkbox.sn.domain.Departement;
import org.thinkbox.sn.domain.Region;
import org.thinkbox.sn.domain.Responsable;
import org.thinkbox.sn.domain.Secteur;
import org.thinkbox.sn.domain.enumeration.FormeJuridique;
import org.thinkbox.sn.domain.enumeration.Sexe;
import org.thinkbox.sn.domain.enumeration.TypeDemande;
import org.thinkbox.sn.service.dto.CustomDTO;
import org.thinkbox.sn.repository.DemandeRepository;
import org.thinkbox.sn.service.dto.DemandeDTO;
import org.thinkbox.sn.service.dto.ResponsableDTO;
import org.thinkbox.sn.service.mapper.CommuneMapper;
import org.thinkbox.sn.service.mapper.DemandeMapper;
import org.thinkbox.sn.service.mapper.DepartementMapper;
import org.thinkbox.sn.service.mapper.EquipementMapper;
import org.thinkbox.sn.service.mapper.RegionMapper;
import org.thinkbox.sn.service.mapper.ResponsableMapper;
import org.thinkbox.sn.service.mapper.SecteurMapper;

/**
 *
 * @author guisse
 */
@Service
@Transactional
public class DemandeService {

    private final DemandeRepository demandeRepository;

    private final DemandeMapper demandeMapper;

    private final ResponsableMapper responsableMapper;

    private final ResponsableService responsableService;
    
     private final RegionMapper regionMapper;
    
    private final DepartementMapper departementMapper;
    
    private final CommuneMapper communeMapper;
    
    private final SecteurMapper secteurMapper;
    
    private final EquipementMapper equipementMapper;

    public DemandeService(DemandeRepository demandeRepository, DemandeMapper demandeMapper, ResponsableMapper responsableMapper, ResponsableService responsableService, RegionMapper regionMapper, DepartementMapper departementMapper, CommuneMapper communeMapper, SecteurMapper secteurMapper, EquipementMapper equipementMapper) {
        this.demandeRepository = demandeRepository;
        this.demandeMapper = demandeMapper;
        this.responsableMapper = responsableMapper;
        this.responsableService = responsableService;
        this.regionMapper = regionMapper;
        this.departementMapper = departementMapper;
        this.communeMapper = communeMapper;
        this.secteurMapper = secteurMapper;
        this.equipementMapper = equipementMapper;
    }

    

    public DemandeDTO save(DemandeDTO demandeDTO) {
        Demande demande = demandeMapper.toEntity(demandeDTO);
        demande = demandeRepository.save(demande);
        return demandeMapper.toDto(demande);
    }

    @Transactional(readOnly = true)
    public List<DemandeDTO> findAll() {
        return demandeRepository.findAll().stream()
                .map(demandeMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Transactional(readOnly = true)
    public DemandeDTO findOne(Long id) {
        Demande demande = demandeRepository.findOne(id);
        return demandeMapper.toDto(demande);
    }

    public void delete(Long id) {
        demandeRepository.delete(id);
    }

    @Transactional(readOnly = true)
    public DemandeDTO getDemandeDTOWithDependances(Long id) {
        Demande demande = demandeRepository.findOne(id);
        List<Responsable> listes = demande.getResponsables()
                .stream()
                .collect(Collectors.toList());
        DemandeDTO demandeDTO = demandeMapper.toDto(demande);
        demandeDTO.setResponsablesDTO(responsableMapper.toDto(listes));
        return demandeDTO;
    }

    @Transactional(readOnly = true)
    public List<DemandeDTO> findDemandeByDepartement(Long id) {
        return demandeMapper.toDto(
                demandeRepository.findDemandeByDepartement(id)
        );
    }

    @Transactional(readOnly = true)
    public List<DemandeDTO> findDemandeByCommune(Long id) {
        return demandeMapper.toDto(
                demandeRepository.findDemandeByCommune(id)
        );
    }

    @Transactional(readOnly = true)
    public List<DemandeDTO> findDemandeByRegion(Long id) {
        return demandeMapper.toDto(
                demandeRepository.findDemandeByRegion(id)
        );
    }

    @Transactional(readOnly = true)
    public List<DemandeDTO> findByEtat(String etat) {
        return demandeMapper.toDto(
                demandeRepository.findByEtat(etat)
        );
    }

    public void updateDemandeListByIdsAndEtat(String etat, List<Long> ids) {
        demandeRepository.updateDemandeByIdsAndEtat(etat, ids);
    }

    @Transactional(readOnly = true)
    public String numeroDossier() {
        String id = String.valueOf(demandeRepository.count() + 1);
        String numero = "000 0000";
        String sub = numero.substring(0, numero.length() - id.length());
        return sub + id;
    }

    public void update(CustomDTO dto) {
        demandeRepository.update(dto.getNinea(), dto.getRccm(), dto.getAffliation(), dto.getDescription(), dto.getId());
    }

    @Transactional(readOnly = true)
    public Long getDemandeDepartmentValided(Long id) {
        return demandeRepository.countALlByStarus(id, EtatDemande.VALIDE);
    }

    @Transactional(readOnly = true)
    public Long getDemandeDepartmentRejected(Long id) {
        return demandeRepository.countALlByStarus(id, EtatDemande.REJETE);
    }

    @Transactional(readOnly = true)
    public Long getCountAllDemandeByDepartement(Long id) {
        return demandeRepository.countALlByDepartement(id);
    }

    @Transactional(readOnly = true)
    public Long countAllDemandeValide() {
        return demandeRepository.countAllDemandeByStatus(EtatDemande.VALIDE);
    }

    @Transactional(readOnly = true)
    public Long countAllDemandeRejete() {
        return demandeRepository.countAllDemandeByStatus(EtatDemande.REJETE);
    }

    @Transactional(readOnly = true)
    public Long count() {
        return demandeRepository.count();
    }

    @Transactional(readOnly = true)
    public Page<Demande> allDemande(Pageable pageable) {
        return demandeRepository.allDemande(pageable);
    }

    @Transactional(readOnly = true)
    public Page<Demande> allDemandePending(Pageable pageable) {
        return demandeRepository.allDemandeByStatus(pageable, EtatDemande.EN_COURS);
    }

    @Transactional(readOnly = true)
    public Page<Demande> allDemandeRejected(Pageable pageable) {
        return demandeRepository.allDemandeByStatus(pageable, EtatDemande.REJETE);
    }

    @Transactional(readOnly = true)
    public Page<Demande> allDemandeValidet(Pageable pageable) {
        return demandeRepository.allDemandeByStatus(pageable, EtatDemande.VALIDE);
    }

    @Transactional(readOnly = true)
    public Demande findByNumero(String numero) {
        return demandeRepository.findByNumero(numero);
    }

    public Optional<DemandeDTO> updateDemande(DemandeDTO dto) {
        return Optional.of(demandeRepository
                .findOne(dto.getId()))
                .map(demande -> {
                    Region region = regionMapper.fromID(dto.getRegionId());
                    Departement departement = departementMapper.fromID(dto.getDepartementId());
                    Commune commune = communeMapper.fromID(dto.getCommuneId());
                    Secteur secteur = secteurMapper.fromID(dto.getSecteurId());
                    demande.setRegion(region);
                    demande.setDepartement(departement);
                    demande.setCommune(commune);
                    demande.setSecteur(secteur);
                    demande.setId(dto.getId());
                    demande.setMontant(dto.getMontant());
                    demande.setFormeJurdique(FormeJuridique.valueOf(dto.getFormeJuridique()));
                    demande.setTypeDemande(TypeDemande.valueOf(dto.getTypeDemande()));
                    demande.setSiteWeb(dto.getSiteWeb());
                    demande.setNinea(dto.getNinea());
                    demande.setRccm(dto.getRccm());
                    demande.setAffliation(dto.getAffliation());
                    demande.setDescription(dto.getDescription());
                    demande.setNumero(dto.getNumero());
                    demande.setTelephone1(dto.getTelephone1());
                    demande.setTelephone2(dto.getTelephone2());
                    demande.setTelephone3(dto.getTelephone3());
                    demande.setActivites(dto.getActivites());
                    demande.setProduitsServices(dto.getProduitsServices());
                    demande.setProspects(dto.getProspects());
                    demande.setClients(dto.getClients());
                    demande.setNombreHomme(dto.getNombreHomme());
                    demande.setNombreFemme(dto.getNombreFemme());
                    demande.setCompteIf(dto.getCompteIf());
                    demande.setCreditIf(dto.getCreditIf());
                    demande.setFinancement(dto.getFinancement());

                    if (dto.getNombreMembre() != null) {
                        demande.setNombreMembre(dto.getNombreMembre());
                    }
                    demande.setScore(demandeMapper.setScore(dto));
                    Set<Responsable> responsables = demande.getResponsables();
                    ResponsableDTO responsableDTO = dto.getResponsablesDTO().get(0);
                    Responsable responsable = responsables.iterator().next();
                    responsable.setNom(responsableDTO.getNom());
                    responsable.setPrenom(responsableDTO.getPrenom());
                    responsable.setCni(responsableDTO.getCni());
                    responsable.setFonction(responsableDTO.getFonction());
                    responsable.setSexe(Sexe.valueOf(responsableDTO.getSexe()));
                    responsable.setDateNaissance(responsableDTO.getDateNaissance());
                    responsable.setExperience(responsableDTO.getExperience());
                    responsable.setQualification(responsableDTO.getQualification());
                    responsable.setAdresse(responsableDTO.getAdresse());
                    responsable.setLieuNaissance(responsableDTO.getLieuNaissance());
                    responsable.setTelephone(responsableDTO.getTelephone());
                    return demande;
                })
                .map(DemandeDTO::new);
    }

}
