package org.thinkbox.sn.service.dto;

import java.io.Serializable;

import org.thinkbox.sn.domain.Commune;

public class CommuneDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Long id;
	
	private String nom;
	
	private Long idDepartement;
	
	private String nomDepartement;

	public CommuneDTO() {
		
	}
	
	public Commune toEntity() {
		Commune entity =new Commune();
		entity.setNom(this.nom);
		return entity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Long getIdDepartement() {
		return idDepartement;
	}

	public void setIdDepartement(Long idDepartement) {
		this.idDepartement = idDepartement;
	}

	public String getNomDepartement() {
		return nomDepartement;
	}

	public void setNomDepartement(String nomDepartement) {
		this.nomDepartement = nomDepartement;
	}
	
}
