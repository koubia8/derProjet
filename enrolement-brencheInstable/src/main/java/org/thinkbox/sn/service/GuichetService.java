/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.domain.Guichet;
import org.thinkbox.sn.service.dto.GuichetDTO;
import org.thinkbox.sn.repository.GuichetRepository;
import org.thinkbox.sn.service.mapper.GuichetMapper;

/**
 *
 * @author guisse
 */
@Service
public class GuichetService {
    
    private final GuichetRepository guichetRepository;
    
    private final GuichetMapper guichetMapper;

    public GuichetService(GuichetRepository guichetRepository, GuichetMapper guichetMapper) {
        this.guichetRepository = guichetRepository;
        this.guichetMapper = guichetMapper;
    }
    
    public GuichetDTO save(GuichetDTO guichetDTO){
        Guichet guichet = guichetMapper.toEntity(guichetDTO);
        guichet = guichetRepository.save(guichet);
        return guichetMapper.toDto(guichet);
    }
    
    @Transactional(readOnly = true)
    public List<GuichetDTO> findAll() {
        return guichetRepository.findAll().stream()
            .map(guichetMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
  
    @Transactional(readOnly = true)
    public GuichetDTO findOne(Long id) {
        Guichet guichet = guichetRepository.findOne(id);
        return guichetMapper.toDto(guichet);
    }

    public void delete(Long id) {
        guichetRepository.delete(id);
    }
    
    @Transactional(readOnly = true)
    public Double getMaxValueGuichetByNom(String nom){
        Guichet guichet = guichetRepository.findByNom(nom);
        if(guichet == null){
            return 0.0;
        }
        return guichet.getMax();
    }
    
    @Transactional(readOnly = true)
    public Double getMinMontantGuichet(){
        return guichetRepository.getMaxMontant();
    }
}
