/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.util;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author guisse
 */
public class DemandeSearchModel implements Serializable{
    
    private BigInteger id;
    
    private Date dateDemande;
    
    private String numero;
    
    private Double montant;
    
    private String etat;
    
    private String nomPorteurProjet;
    
    private String prenomPorteurProjet;
    
    private String sexePorteurProjet;
    
    private String telephonePorteurProjet;
    
    private String cniPorteurProjet;

    public DemandeSearchModel() {
    }

    public Date getDateDemande() {
        return dateDemande;
    }

    public void setDateDemande(Date dateDemande) {
        this.dateDemande = dateDemande;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getNomPorteurProjet() {
        return nomPorteurProjet;
    }

    public void setNomPorteurProjet(String nomPorteurProjet) {
        this.nomPorteurProjet = nomPorteurProjet;
    }

    public String getPrenomPorteurProjet() {
        return prenomPorteurProjet;
    }

    public void setPrenomPorteurProjet(String prenomPorteurProjet) {
        this.prenomPorteurProjet = prenomPorteurProjet;
    }

    public String getSexePorteurProjet() {
        return sexePorteurProjet;
    }

    public void setSexePorteurProjet(String sexePorteurProjet) {
        this.sexePorteurProjet = sexePorteurProjet;
    }

    public String getTelephonePorteurProjet() {
        return telephonePorteurProjet;
    }

    public void setTelephonePorteurProjet(String telephonePorteurProjet) {
        this.telephonePorteurProjet = telephonePorteurProjet;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }
    
    public String getCniPorteurProjet() {
        return cniPorteurProjet;
    }

    public void setCniPorteurProjet(String cniPorteurProjet) {
        this.cniPorteurProjet = cniPorteurProjet;
    }
}
