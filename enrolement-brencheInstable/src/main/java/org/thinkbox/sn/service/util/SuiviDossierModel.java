/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author guisse
 */
public class SuiviDossierModel implements Serializable{
    
    private String titre;
    
    private BigInteger demandes;
    
    private BigDecimal beneficiares;
    
    private BigDecimal nonbeneficiaires;

    public SuiviDossierModel() {
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public BigInteger getDemandes() {
        return demandes;
    }

    public void setDemandes(BigInteger demandes) {
        this.demandes = demandes;
    }

    public BigDecimal getBeneficiares() {
        return beneficiares;
    }

    public void setBeneficiares(BigDecimal beneficiares) {
        this.beneficiares = beneficiares;
    }

    public BigDecimal getNonbeneficiaires() {
        return nonbeneficiaires;
    }

    public void setNonbeneficiaires(BigDecimal nonbeneficiaires) {
        this.nonbeneficiaires = nonbeneficiaires;
    }
    
    

   
}
