/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.thinkbox.sn.domain.Guichet;
import org.thinkbox.sn.domain.enumeration.TypeGuichet;
import org.thinkbox.sn.service.dto.GuichetDTO;

/**
 *
 * @author guisse
 */
@Service
public class GuichetMapper implements EntityMapper<GuichetDTO,Guichet>{
 
    @Override
    public Guichet toEntity(GuichetDTO dto) {
        if(dto == null){
            return null;
        }
        Guichet guichet = new Guichet();
        guichet.setId(dto.getId());
        guichet.setMax(dto.getMax());
        guichet.setActivated(dto.isActive());
        guichet.setNom(TypeGuichet.valueOf(dto.getNom()));
        return guichet;
    }

    @Override
    public GuichetDTO toDto(Guichet entity) {
        
        if(entity == null){
            return null;
        }
        GuichetDTO guichetDTO =new GuichetDTO();
        guichetDTO.setId(entity.getId());
        guichetDTO.setNom(entity.getNom().name());
        guichetDTO.setMax(entity.getMax());
        guichetDTO.setActive(entity.getActivated());
        return guichetDTO;
    }

    @Override
    public List<Guichet> toEntity(List<GuichetDTO> dtoList) {
        return dtoList.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<GuichetDTO> toDto(List<Guichet> entityList) {
        return entityList.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }
}
