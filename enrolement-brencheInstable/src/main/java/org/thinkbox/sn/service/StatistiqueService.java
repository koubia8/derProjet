/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Service;
import org.thinkbox.sn.service.util.DiagrammeModel;
import org.thinkbox.sn.service.util.SuiviDossierModel;

/**
 *
 * @author guisse
 */
@Service
public class StatistiqueService {

    private static final String VIEW_NAME_REGION = "view_sd_region";
    
    private static final String VIEW_NAME_COMMUNE = "view_sd_commune";
    
    private static final String VIEW_NAME_DIAGRAMME = "view_diagramme";

    private static final String QUERY_REGION = "SELECT region,demandes,beneficiaires,nonbeneficiaires FROM " + VIEW_NAME_REGION;
    
    private static final String QUERY_COMMUNE = "SELECT commune,demandes,beneficiaires,nonbeneficiaires FROM " + VIEW_NAME_COMMUNE+ " WHERE departement_id=:departement_id";
    
    private static final String QUERY_DIAGRAMME = "SELECT * FROM " + VIEW_NAME_DIAGRAMME;
    
    private final EntityManager em;

    public StatistiqueService(EntityManager em) {
        this.em = em;
    }

   public  List<SuiviDossierModel> getStatsRegion() {
        try {
            List<Object[]> objects = em.createNativeQuery(QUERY_REGION)
                .getResultList();
        List<SuiviDossierModel> models = new ArrayList<>();
        objects.stream().map((object) -> {
            SuiviDossierModel model = new SuiviDossierModel();
            model.setTitre((String) object[0]);
            model.setDemandes((BigInteger) object[1]);
            model.setBeneficiares((BigDecimal) object[2]);
            model.setNonbeneficiaires((BigDecimal) object[3]);
            return model;
        }).forEachOrdered((model) -> {
            models.add(model);
        });
        return models;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
   
   
   public  List<SuiviDossierModel> getStatsCommnes(Long id) {
        try {
            List<Object[]> objects = em.createNativeQuery(QUERY_COMMUNE)
                    .setParameter("departement_id",id)
                .getResultList();
        List<SuiviDossierModel> models = new ArrayList<>();
        objects.stream().map((object) -> {
            SuiviDossierModel model = new SuiviDossierModel();
            model.setTitre((String) object[0]);
            model.setDemandes((BigInteger) object[1]);
            model.setBeneficiares((BigDecimal) object[2]);
            model.setNonbeneficiaires((BigDecimal) object[3]);
            return model;
        }).forEachOrdered((model) -> {
            models.add(model);
        });
        return models;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
   
      public  List<DiagrammeModel> getDiagramme() {
        try {
            List<Object[]> objects = em.createNativeQuery(QUERY_DIAGRAMME)
                .getResultList();
        List<DiagrammeModel> models = new ArrayList<>();
        objects.stream().map((object) -> {
            DiagrammeModel model = new DiagrammeModel();
            model.setSecteur((String) object[0]);
            model.setTotal((BigInteger) object[1]);
            return model;
        }).forEachOrdered((model) -> {
            models.add(model);
        });
        return models;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
   
}
