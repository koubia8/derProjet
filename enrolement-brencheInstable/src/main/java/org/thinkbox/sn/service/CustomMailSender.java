package org.thinkbox.sn.service;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Async;

@Service
public class CustomMailSender {

    private final JavaMailSender javaMailSender;

    public CustomMailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Async
    public boolean SendMail(String from, String to, String subject, String texte) {
        try {
            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setTo(to);
            mail.setFrom(from);
            mail.setSubject(subject);
            mail.setText(texte);
            javaMailSender.send(mail);
            return true;
        } catch (MailException e) {
            //e.printStackTrace();
            return false;
        } catch (Exception e) {
            //e.printStackTrace();
            return false;
        }

    }

}
