/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.domain.Secteur;
import org.thinkbox.sn.service.dto.SecteurDTO;
import org.thinkbox.sn.repository.SecteurRepository;
import org.thinkbox.sn.service.mapper.SecteurMapper;

/**
 *
 * @author guisse
 */
@Service
@Transactional
public class SecteurService {
    
    private final SecteurRepository secteurRepository;
    
    private final SecteurMapper secteurMapper;

    public SecteurService(SecteurRepository secteurRepository, SecteurMapper secteurMapper) {
        this.secteurRepository = secteurRepository;
        this.secteurMapper = secteurMapper;
    }
    
    public SecteurDTO save(SecteurDTO secteurDTO){
        Secteur secteur = secteurMapper.toEntity(secteurDTO);
        secteur = secteurRepository.save(secteur);
        return secteurMapper.toDto(secteur);
    }
    
    @Transactional(readOnly = true)
    public List<SecteurDTO> findAll() {
        return secteurRepository.findAll().stream()
            .map(secteurMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    @Transactional(readOnly = true)
    public SecteurDTO findOne(Long id) {
        Secteur secteur = secteurRepository.findOne(id);
        return secteurMapper.toDto(secteur);
    }

    public void delete(Long id) {
        secteurRepository.delete(id);
    }
    
}
