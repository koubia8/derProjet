/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.domain.Partenaire;
import org.thinkbox.sn.repository.PartenaireRepository;
import org.thinkbox.sn.service.dto.PartenaireDTO;
import org.thinkbox.sn.service.mapper.PartenaireMapper;

/**
 *
 * @author guisse
 */
@Service
@Transactional
public class PartenaireService {
    private final PartenaireRepository partenaireRepository;
    
    private final PartenaireMapper partenaireMapper;

    public PartenaireService(PartenaireRepository partenaireRepository, PartenaireMapper partenaireMapper) {
        this.partenaireRepository = partenaireRepository;
        this.partenaireMapper = partenaireMapper;
    }
    
    public PartenaireDTO save(PartenaireDTO partenaireDTO){
        Partenaire partenaire = partenaireMapper.toEntity(partenaireDTO);
        partenaire = partenaireRepository.save(partenaire);
        return partenaireMapper.toDto(partenaire);
    }
    
    @Transactional(readOnly = true)
    public List<PartenaireDTO> findAll() {
        return partenaireRepository.findAll().stream()
            .map(partenaireMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    @Transactional(readOnly = true)
    public PartenaireDTO findOne(Long id) {
        Partenaire partenaire = partenaireRepository.findOne(id);
        return partenaireMapper.toDto(partenaire);
    }

    public void delete(Long id) {
        partenaireRepository.delete(id);
    }
     public void enablePartenaire(Long id) {
        partenaireRepository.enableOrdisable(id, Boolean.TRUE);
    }
    
    public void disablePartenaire(Long id) {
        partenaireRepository.enableOrdisable(id, Boolean.FALSE);
    }
}
