/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.util;

import java.time.format.DateTimeFormatter;

/**
 *
 * @author guisse
 */
public final class DateFormatUtil{
        
    /**
     *
     */
   public static final String FORMAT_DATE = "yyyy-mm-dd";
    
   public static  final DateTimeFormatter MY_PATTERN = DateTimeFormatter.ofPattern(FORMAT_DATE);
    
}
