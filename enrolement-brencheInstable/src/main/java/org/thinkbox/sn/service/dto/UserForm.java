package org.thinkbox.sn.service.dto;



public class UserForm {
	
	private String password;
	
	private String newPassword;
	
	private String confirmation;

	public UserForm() {
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmation() {
		return confirmation;
	}

	public void setConfirmation(String confirmation) {
		this.confirmation = confirmation;
	}
	
	public boolean controle(){
		if(newPassword.equals(confirmation))
			return true;
		else 
			return false;
	}
}
