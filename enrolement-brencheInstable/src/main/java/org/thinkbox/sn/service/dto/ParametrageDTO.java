package org.thinkbox.sn.service.dto;

public class ParametrageDTO {

	private String invidivuel;

	private String collectif;

	private Long idGuichetIndividuel;

	private Long idGuichetCollectif;

	private String nomGuichetIndividuel;

	private String nomGuichetCollectif;

	public ParametrageDTO() {
		
	}

	public String getInvidivuel() {
		return invidivuel;
	}

	public void setInvidivuel(String invidivuel) {
		this.invidivuel = invidivuel;
	}

	public String getCollectif() {
		return collectif;
	}

	public void setCollectif(String collectif) {
		this.collectif = collectif;
	}

	public Long getIdGuichetIndividuel() {
		return idGuichetIndividuel;
	}

	public void setIdGuichetIndividuel(Long idGuichetIndividuel) {
		this.idGuichetIndividuel = idGuichetIndividuel;
	}

	public Long getIdGuichetCollectif() {
		return idGuichetCollectif;
	}

	public void setIdGuichetCollectif(Long idGuichetCollectif) {
		this.idGuichetCollectif = idGuichetCollectif;
	}

	public String getNomGuichetIndividuel() {
		return nomGuichetIndividuel;
	}

	public void setNomGuichetIndividuel(String nomGuichetIndividuel) {
		this.nomGuichetIndividuel = nomGuichetIndividuel;
	}

	public String getNomGuichetCollectif() {
		return nomGuichetCollectif;
	}

	public void setNomGuichetCollectif(String nomGuichetCollectif) {
		this.nomGuichetCollectif = nomGuichetCollectif;
	}

	
}
