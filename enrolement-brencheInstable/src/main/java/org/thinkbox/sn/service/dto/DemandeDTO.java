/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.thinkbox.sn.domain.Demande;

/**
 *
 * @author guisse
 */
public class DemandeDTO implements Serializable {

    private Long id;

    private String numero;
    

    private String description;

    @NotNull
    private Double montant;

    private Float score;

    private Boolean accepted;

    private String etat;

    private String email;

    private String siteWeb;

    private String ninea;

    private String affliation;

    private String rccm;

    private String businessPlan;

    @NotNull
    private String typeDemande;

    @NotNull
    private String formeJuridique;

    @NotNull
    private Long departementId;

    @NotNull
    private Long regionId;

    @NotNull
    private Long communeId;

    private Long secteurId;

    private String telephone1;

    private String telephone2;

    private String telephone3;

    private Integer nombreMembre;

    private Date createdDate;

    private String createdBy;

    private String activites;

    private String produitsServices;

    private Boolean prospects;

    private Boolean clients;

    private Integer nombreHomme;

    private Integer nombreFemme;

    private Boolean compteIf;

    private Boolean creditIf;

    private Boolean financement;

    private String nomProjet;

    //qualification et l'activite et la comptence
    private String adequationQA;

    //adequation entre la competene et l'activete
    private String adequationCA;

    private List<ResponsableDTO> responsablesDTO = new ArrayList<ResponsableDTO>();

    private List<EquipementDTO> equipementsDTO = new ArrayList<EquipementDTO>();

    public DemandeDTO() {
    }

    public DemandeDTO(Demande demande) {
//        this.setRegionId(demande.getRegion().getId());
//        this.setDepartementId(demande.getDepartement().getId());
//        this.setCommuneId(demande.getCommune().getId());
//        if(null != demande.getSecteur()){
//          this.setSecteurId(demande.getSecteur().getId());  
//        }
//        
//        this.setId(demande.getId());
//        this.setMontant(demande.getMontant());
//        this.setFormeJuridique(demande.getFormeJurdique().name());
//        this.setTypeDemande(demande.getTypeDemande().name());
//        this.setSiteWeb(demande.getSiteWeb());
//        this.setNinea(demande.getNinea());
//        this.setRccm(demande.getRccm());
//        this.setAffliation(demande.getAffliation());
//        this.setNumero(demande.getNumero());
//        this.setEtat(demande.getEtat());
//        this.setDescription(demande.getDescription());
//        this.setScore(demande.getScore());
//        this.setNombreMembre(demande.getNombreMembre());
//        this.setCreatedDate(demande.getCreatedDate());
//        this.setCreatedBy(demande.getCreatedBy());
//        
//        this.setActivites(demande.getActivites());
//        this.setProduitsServices(demande.getProduitsServices());
//        this.setProspects(demande.getProspects());
//        this.setClients(demande.getClients());
//        this.setNombreHomme(demande.getNombreHomme());
//        this.setNombreFemme(demande.getNombreFemme());
//        this.setCompteIf(demande.getCompteIf());
//        this.setCreditIf(demande.getCreditIf());
//        this.setFinancement(demande.getFinancement());
//        
//        this.setTelephone1(demande.getTelephone1());
//        this.setTelephone2(demande.getTelephone2());
//        this.setTelephone3(demande.getTelephone3());

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMontant() {
        return montant;
    }

    public Long getDepartementId() {
        return departementId;
    }

    public void setDepartementId(Long departementId) {
        this.departementId = departementId;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public Long getCommuneId() {
        return communeId;
    }

    public void setCommuneId(Long communeId) {
        this.communeId = communeId;
    }

    public Long getSecteurId() {
        return secteurId;
    }

    public void setSecteurId(Long secteurId) {
        this.secteurId = secteurId;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Float getScore() {
        return score;
    }

    public String getBusinessPlan() {
        return businessPlan;
    }

    public void setBusinessPlan(String businessPlan) {
        this.businessPlan = businessPlan;
    }

    public List<ResponsableDTO> getResponsablesDTO() {
        return responsablesDTO;
    }

    public void setResponsablesDTO(List<ResponsableDTO> responsablesDTO) {
        this.responsablesDTO = responsablesDTO;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public String getNinea() {
        return ninea;
    }

    public void setNinea(String ninea) {
        this.ninea = ninea;
    }

    public String getAffliation() {
        return affliation;
    }

    public void setAffliation(String affliation) {
        this.affliation = affliation;
    }

    public String getRccm() {
        return rccm;
    }

    public void setRccm(String rccm) {
        this.rccm = rccm;
    }

    public String getTypeDemande() {
        return typeDemande;
    }

    public void setTypeDemande(String typeDemande) {
        this.typeDemande = typeDemande;
    }

    public String getFormeJuridique() {
        return formeJuridique;
    }

    public void setFormeJuridique(String formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    public String getTelephone1() {
        return telephone1;
    }

    public void setTelephone1(String telephone1) {
        this.telephone1 = telephone1;
    }

    public String getTelephone2() {
        return telephone2;
    }

    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }

    public String getTelephone3() {
        return telephone3;
    }

    public void setTelephone3(String telephone3) {
        this.telephone3 = telephone3;
    }

    public List<EquipementDTO> getEquipementsDTO() {
        return equipementsDTO;
    }

    public void setEquipementsDTO(List<EquipementDTO> equipementsDTO) {
        this.equipementsDTO = equipementsDTO;
    }

    public Integer getNombreMembre() {
        return nombreMembre;
    }

    public void setNombreMembre(Integer nombreMembre) {
        this.nombreMembre = nombreMembre;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getActivites() {
        return activites;
    }

    public void setActivites(String activites) {
        this.activites = activites;
    }

    public String getProduitsServices() {
        return produitsServices;
    }

    public void setProduitsServices(String produitsServices) {
        this.produitsServices = produitsServices;
    }

    public Boolean getProspects() {
        return prospects;
    }

    public void setProspects(Boolean prospects) {
        this.prospects = prospects;
    }

    public Boolean getClients() {
        return clients;
    }

    public void setClients(Boolean clients) {
        this.clients = clients;
    }

    public Integer getNombreHomme() {
        return nombreHomme;
    }

    public void setNombreHomme(Integer nombreHomme) {
        this.nombreHomme = nombreHomme;
    }

    public Integer getNombreFemme() {
        return nombreFemme;
    }

    public void setNombreFemme(Integer nombreFemme) {
        this.nombreFemme = nombreFemme;
    }

    public Boolean getCompteIf() {
        return compteIf;
    }

    public void setCompteIf(Boolean compteIf) {
        this.compteIf = compteIf;
    }

    public Boolean getCreditIf() {
        return creditIf;
    }

    public void setCreditIf(Boolean creditIf) {
        this.creditIf = creditIf;
    }

    public Boolean getFinancement() {
        return financement;
    }

    public void setFinancement(Boolean financement) {
        this.financement = financement;
    }

    public String getNomProjet() {
        return nomProjet;
    }

    public void setNomProjet(String nomProjet) {
        this.nomProjet = nomProjet;
    }

    public String getAdequationQA() {
        return adequationQA;
    }

    public void setAdequationQA(String adequationQA) {
        this.adequationQA = adequationQA;
    }

    public String getAdequationCA() {
        return adequationCA;
    }

    public void setAdequationCA(String adequationCA) {
        this.adequationCA = adequationCA;
    }
}
