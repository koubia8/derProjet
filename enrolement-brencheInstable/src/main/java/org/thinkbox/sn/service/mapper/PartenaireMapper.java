/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.thinkbox.sn.domain.Commune;
import org.thinkbox.sn.domain.Departement;
import org.thinkbox.sn.domain.Partenaire;
import org.thinkbox.sn.domain.Secteur;
import org.thinkbox.sn.domain.Partenaire;
import org.thinkbox.sn.domain.Region;
import org.thinkbox.sn.domain.TypePartenaire;
import org.thinkbox.sn.repository.SecteurRepository;
import org.thinkbox.sn.repository.PartenaireRepository;
import org.thinkbox.sn.service.dto.PartenaireDTO;
import org.thinkbox.sn.service.dto.PartenaireDTO;

/**
 *
 * @author guisse
 */
@Service
public class PartenaireMapper implements EntityMapper<PartenaireDTO, Partenaire>{
    
    private final PartenaireRepository partenaireRepository;
    
    private final RegionMapper regionMapper;

    private final CommuneMapper communeMapper;
    
    private final DepartementMapper departementMapper;

    public PartenaireMapper(PartenaireRepository partenaireRepository, RegionMapper regionMapper, CommuneMapper communeMapper, DepartementMapper departementMapper) {
        this.partenaireRepository = partenaireRepository;
        this.regionMapper = regionMapper;
        this.communeMapper = communeMapper;
        this.departementMapper = departementMapper;
    }
    
    
    
    @Override
    public Partenaire toEntity(PartenaireDTO dto) {
        if(dto == null){
            return null;
        }
        Partenaire partenaire = new Partenaire();
        Region region = regionMapper.fromID(dto.getIdRegion());
        Departement departement = departementMapper.fromID(dto.getIdDepartement());
        Commune commune = communeMapper.fromID(dto.getIdCommune());
        TypePartenaire typePartenaire = fromIdTypePartenaire(dto.getIdTypePartenaire());
        partenaire.setRegion(region);
        partenaire.setDepartement(departement);
        partenaire.setCommune(commune);
        partenaire.setTypePartenaire(typePartenaire);
        partenaire.setId(dto.getId());
        partenaire.setNom(dto.getNom());
        partenaire.setActivated(dto.isActive());
        partenaire.setEmail(dto.getEmail());
        partenaire.setTelephone(dto.getTelephone());
        partenaire.setCommentaire(dto.getCommentaire());
        return partenaire;
    }

    @Override
    public PartenaireDTO toDto(Partenaire entity) {
        
        if(entity == null){
            return null;
        } 
        PartenaireDTO partenaireDTO =new PartenaireDTO();
        
       // partenaireDTO.setIdRegion(entity.getRegion().getId());
       // partenaireDTO.setIdDepartement(entity.getDepartement().getId());
       // partenaireDTO.setIdCommune(entity.getCommune().getId());
        
        partenaireDTO.setId(entity.getId());
        partenaireDTO.setNom(entity.getNom());
        partenaireDTO.setTelephone(entity.getTelephone());
        partenaireDTO.setEmail(entity.getEmail());
      //  partenaireDTO.setIdTypePartenaire(entity.getTypePartenaire());
        partenaireDTO.setActive(entity.getActivated());
        
        return partenaireDTO;
    }

    @Override
    public List<Partenaire> toEntity(List<PartenaireDTO> dtoList) {
        return dtoList.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<PartenaireDTO> toDto(List<Partenaire> entityList) {
        return entityList.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }
    
    public Partenaire partenaireFromId(Long id){
        if(id == null){
            return null;
        }
        Partenaire partenaire = new Partenaire();
        partenaire.setId(id);
        return partenaire;
    }
    
    
    private TypePartenaire fromIdTypePartenaire(Long idTypePartenaire){
        if(idTypePartenaire == null){
            return null;
        }
        TypePartenaire tp = new TypePartenaire();
        tp.setId(idTypePartenaire);
        return tp;
    }
        
}
