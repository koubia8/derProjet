/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.dto;

import java.io.Serializable;

/**
 *
 * @author guisse
 */
public class CustomDTO implements Serializable{
     private Long id ;
    private String formeJuridique;
    private String  ninea ;
    private String  rccm;
    private String  affliation ;
    private String  description ;

        public CustomDTO() {
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getFormeJuridique() {
            return formeJuridique;
        }

        public void setFormeJuridique(String formeJuridique) {
            this.formeJuridique = formeJuridique;
        }

        public String getNinea() {
            return ninea;
        }

        public void setNinea(String ninea) {
            this.ninea = ninea;
        }

        public String getRccm() {
            return rccm;
        }

        public void setRccm(String rccm) {
            this.rccm = rccm;
        }

        public String getAffliation() {
            return affliation;
        }

        public void setAffliation(String affliation) {
            this.affliation = affliation;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
   
}
