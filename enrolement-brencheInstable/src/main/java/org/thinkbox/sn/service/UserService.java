package org.thinkbox.sn.service;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.config.Constants;
import org.thinkbox.sn.domain.Authority;
import org.thinkbox.sn.domain.Partenaire;
import org.thinkbox.sn.domain.User;
import org.thinkbox.sn.repository.AuthorityRepository;
import org.thinkbox.sn.repository.UserRepository;
import org.thinkbox.sn.security.AuthoritiesConstants;
import org.thinkbox.sn.security.SecurityUtils;
import org.thinkbox.sn.service.dto.UserDTO;
import org.thinkbox.sn.service.util.RandomUtil;

@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
    }


    public User registerUser(UserDTO userDTO, String password) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(userDTO.getLogin());
        // new user gets initially a generated password
        newUser.setMotdepasse(encryptedPassword);
        newUser.setNom(userDTO.getNom());
        newUser.setPrenom(userDTO.getPrenom());
        newUser.setEmail(userDTO.getEmail());
        newUser.setActivated(false);
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public User createUser(UserDTO userDTO,String password) {
        User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setNom(userDTO.getNom());
        user.setPrenom(userDTO.getPrenom());
        user.setEmail(userDTO.getEmail());
        user.setTelephone(userDTO.getTelephone());
        if(userDTO.getPartenaireId() != null){
            Partenaire partenaire = new Partenaire();
            partenaire.setId(userDTO.getPartenaireId());
            user.setPartenaire(partenaire);
        }
        if (userDTO.getAuthorities() != null) {
            Set<Authority> authorities = userDTO.getAuthorities().stream()
                .map(authorityRepository::findOne)
                .collect(Collectors.toSet());
            user.setAuthorities(authorities);
        }
        String encryptedPassword = passwordEncoder.encode(password);
        user.setMotdepasse(encryptedPassword);
        user.setActivated(userDTO.isActivated());
        userRepository.save(user);
        //log.debug("Created Information for User: {}", user);
        return user;
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user
     * @param lastName last name of user
     * @param email email id of user
     * @param langKey language key
     * @param imageUrl image URL of user
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                user.setNom(firstName);
                user.setPrenom(lastName);
                user.setEmail(email);
                log.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update
     * @return updated user
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional.of(userRepository
            .findOne(userDTO.getId()))
            .map(user -> {
                if(userDTO.getPartenaireId() != null){
                    Partenaire p =new Partenaire();
                    p.setId(userDTO.getPartenaireId());
                    user.setPartenaire(p);
                }
                user.setLogin(userDTO.getLogin());
                user.setNom(userDTO.getNom());
                user.setTelephone(userDTO.getTelephone());
                user.setPrenom(userDTO.getPrenom());
                user.setEmail(userDTO.getEmail());
                user.setActivated(userDTO.isActivated());
                Set<Authority> managedAuthorities = user.getAuthorities();
                managedAuthorities.clear();
                userDTO.getAuthorities().stream()
                    .map(authorityRepository::findOne)
                    .forEach(managedAuthorities::add);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            userRepository.delete(user);
           log.debug("Deleted User: {}", user);
        });
    }
    
     public void disableUser(String login) {
        userRepository.findOneByLoginAndActivatedTrue(login)
                .ifPresent(user -> {
            user.setActivated(Boolean.FALSE);
        });
    }
     
    public void enableUser(String login) {
        userRepository.findOneByLoginAndActivatedFalse(login)
                .ifPresent(user -> {
            user.setActivated(Boolean.TRUE);
        });
    }
    

    public void changePassword(String password) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                String encryptedPassword = passwordEncoder.encode(password);
                user.setMotdepasse(encryptedPassword);
                log.debug("Changed password for User: {}", user);
            });
    }
    
    
    public Optional<User> requestPasswordReset(String mail,String password) {
        return userRepository.findOneByEmailIgnoreCase(mail)
            .filter(User::getActivated)
            .map(user -> {
                String encryptedPassword = passwordEncoder.encode(password);
                user.setMotdepasse(encryptedPassword);
                return user;
            });
    }
       
    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ADMIN).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities(Long id) {
        return userRepository.findOneWithAuthoritiesById(id);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }

    /**
     * @return a list of all the authorities
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }
}