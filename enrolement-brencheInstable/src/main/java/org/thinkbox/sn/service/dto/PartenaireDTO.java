package org.thinkbox.sn.service.dto;

import java.io.Serializable;



import org.thinkbox.sn.domain.Partenaire;


public class PartenaireDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String nom;

	private String telephone;

	private String email;

	private Long idRegion;

	private String nomRegion;

	private Long idDepartement;

	private String nomDepartement;
        
	private boolean active;

	private Long idCommune;

	private String nomCommune;
	
	private Long idTypePartenaire;
	
	private String nomTypePartenaire;
	
	private String commentaire;

	public PartenaireDTO() {
		
	}

	public Partenaire toEntity(){
		Partenaire entity = new Partenaire();
		entity.setNom(this.nom);
		entity.setTelephone(this.telephone);
		entity.setEmail(this.email);
		entity.setCommentaire(this.commentaire);
                entity.setActivated(this.active);
	
		return entity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
       
	public void setEmail(String email) {
		this.email = email;
	}

	public Long getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(Long idRegion) {
		this.idRegion = idRegion;
	}

	public String getNomRegion() {
		return nomRegion;
	}

	public void setNomRegion(String nomRegion) {
		this.nomRegion = nomRegion;
	}

	public Long getIdDepartement() {
		return idDepartement;
	}

	public void setIdDepartement(Long idDepartement) {
		this.idDepartement = idDepartement;
	}

	public String getNomDepartement() {
		return nomDepartement;
	}

	public void setNomDepartement(String nomDepartement) {
		this.nomDepartement = nomDepartement;
	}

	public Long getIdCommune() {
		return idCommune;
	}

	public void setIdCommune(Long idCommune) {
		this.idCommune = idCommune;
	}

	public String getNomCommune() {
		return nomCommune;
	}

	public void setNomCommune(String nomCommune) {
		this.nomCommune = nomCommune;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Long getIdTypePartenaire() {
		return idTypePartenaire;
	}

	public void setIdTypePartenaire(Long idTypePartenaire) {
		this.idTypePartenaire = idTypePartenaire;
	}

	public String getNomTypePartenaire() {
		return nomTypePartenaire;
	}

	public void setNomTypePartenaire(String nomTypePartenaire) {
		this.nomTypePartenaire = nomTypePartenaire;
	}
}
