/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.thinkbox.sn.domain.Departement;
import org.thinkbox.sn.domain.Departement;
import org.thinkbox.sn.domain.Region;
import org.thinkbox.sn.service.dto.DepartementDTO;
import org.thinkbox.sn.service.dto.DepartementDTO;

/**
 *
 * @author guisse
 */
@Service
public class DepartementMapper implements EntityMapper<DepartementDTO, Departement>{
    
    private final RegionMapper regionMapper;

    public DepartementMapper(RegionMapper regionMapper) {
        this.regionMapper = regionMapper;
    }
    
    @Override
    public Departement toEntity(DepartementDTO dto) {
        if(dto == null){
            return null;
        }
        Departement departement = new Departement();
        Region region = regionMapper.fromID(dto.getIdRegion());
        departement.setRegion(region);
        departement.setId(dto.getId());
        departement.setNom(dto.getNom());
        return departement;
    }

    @Override
    public DepartementDTO toDto(Departement entity) {
        
        if(entity == null){
            return null;
        }
        DepartementDTO departementDTO =new DepartementDTO();
        departementDTO.setId(entity.getId());
        departementDTO.setNom(entity.getNom());
        return departementDTO;
    }

    @Override
    public List<Departement> toEntity(List<DepartementDTO> dtoList) {
        return dtoList.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<DepartementDTO> toDto(List<Departement> entityList) {
        return entityList.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }
    
    public Departement fromID(Long id){
        if(id == null){
            return null;
        }
        Departement departement = new Departement();
        departement.setId(id);
        return departement;
    }
    
}
