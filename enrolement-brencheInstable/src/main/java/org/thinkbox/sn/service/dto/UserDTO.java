package org.thinkbox.sn.service.dto;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.thinkbox.sn.domain.Authority;
import org.thinkbox.sn.domain.Partenaire;
import org.thinkbox.sn.domain.User;

public class UserDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @NotEmpty(message = "le login est obigatoire")
    private String login;

    @NotEmpty(message = "le nom est obigatoire")
    private String nom;

    @NotEmpty(message = "le prenom est obigatoire")
    private String prenom;

    private String telephone;
    
    private String partenaireName;

    @NotEmpty(message = "l'email est obigatoire")
    @Email(message = "cet email n'est pas valide")
    private String email;

    private boolean activated = false;

    private Set<String> authorities;

    private Long partenaireId;

    public User toEntity() {
        User u = new User();
        u.setNom(this.nom);
        u.setPrenom(this.prenom);
        u.setTelephone(this.telephone);
        u.setLogin(login);
        u.setEmail(this.email);
        u.setActivated(this.activated);

        return u;
    }

    public UserDTO() {

    }

    public UserDTO(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.nom = user.getNom();
        this.prenom = user.getPrenom();
        this.email = user.getEmail();
        this.telephone = user.getTelephone();
        this.activated = user.getActivated();
        Partenaire p = user.getPartenaire();
        if(p != null){
            this.setPartenaireId(p.getId());
            this.setPartenaireName(p.getNom());
        }
        this.authorities = user.getAuthorities().stream()
                .map(Authority::getName)
                .collect(Collectors.toSet());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getPartenaireName() {
        return partenaireName;
    }

    public void setPartenaireName(String partenaireName) {
        this.partenaireName = partenaireName;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Long getPartenaireId() {
        return partenaireId;
    }

    public void setPartenaireId(Long partenaireId) {
        this.partenaireId = partenaireId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }

}
