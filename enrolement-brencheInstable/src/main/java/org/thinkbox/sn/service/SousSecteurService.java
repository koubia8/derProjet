/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thinkbox.sn.domain.SousSecteur;
import org.thinkbox.sn.service.dto.SousSecteurDTO;
import org.thinkbox.sn.repository.SousSecteurRepository;
import org.thinkbox.sn.service.mapper.SousSecteurMapper;

/**
 *
 * @author guisse
 */
@Service
@Transactional
public class SousSecteurService {
    
     private final SousSecteurRepository sousSecteurRepository;
    
    private final SousSecteurMapper sousSecteurMapper;

    public SousSecteurService(SousSecteurRepository sousSecteurRepository, SousSecteurMapper sousSecteurMapper) {
        this.sousSecteurRepository = sousSecteurRepository;
        this.sousSecteurMapper = sousSecteurMapper;
    }
    
    public SousSecteurDTO save(SousSecteurDTO sousSecteurDTO){
        SousSecteur sousSecteur = sousSecteurMapper.toEntity(sousSecteurDTO);
        sousSecteur = sousSecteurRepository.save(sousSecteur);
        return sousSecteurMapper.toDto(sousSecteur);
    }
    
    @Transactional(readOnly = true)
    public List<SousSecteurDTO> findAll() {
        return sousSecteurRepository.findAll().stream()
            .map(sousSecteurMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    @Transactional(readOnly = true)
    public SousSecteurDTO findOne(Long id) {
        SousSecteur sousSecteur = sousSecteurRepository.findOne(id);
        return sousSecteurMapper.toDto(sousSecteur);
    }

    public void delete(Long id) {
        sousSecteurRepository.delete(id);
    }
    
    public void enableSousSecteur(Long id) {
        sousSecteurRepository.enableOrdisable(id, Boolean.TRUE);
    }
    
    public void disableSousSecteur(Long id) {
        sousSecteurRepository.enableOrdisable(id, Boolean.FALSE);
    }
    
     public List<SousSecteurDTO> findSousSecteurBySecteurId(Long id){
         return sousSecteurMapper.toDto(
                 sousSecteurRepository.findSousSecteurBySecteurId(id)
         );
     }
    
}
