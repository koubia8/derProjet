/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.thinkbox.sn.domain.Secteur;
import org.thinkbox.sn.service.dto.SecteurDTO;

/**
 *
 * @author guisse
 */
@Service
public class SecteurMapper implements EntityMapper<SecteurDTO,Secteur>{
  
    @Override
    public Secteur toEntity(SecteurDTO dto) {
        if(dto == null){
            return null;
        }
        Secteur secteur = new Secteur();
        secteur.setId(dto.getId());
        secteur.setNom(dto.getNom());
        secteur.setDescription(dto.getDescription());
        secteur.setVerified(dto.getVerified());
        secteur.setActivated(dto.isActivated());
        return secteur;
    }

    @Override
    public SecteurDTO toDto(Secteur entity) {
        
        if(entity == null){
            return null;
        }
        SecteurDTO secteurDTO =new SecteurDTO();
        secteurDTO.setId(entity.getId());
        secteurDTO.setNom(entity.getNom());
        secteurDTO.setDescription(entity.getDescription());
        secteurDTO.setVerified(entity.getVerified());
        secteurDTO.setActivated(entity.getActivated());
        return secteurDTO;
    }

    @Override
    public List<Secteur> toEntity(List<SecteurDTO> dtoList) {
        return dtoList.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<SecteurDTO> toDto(List<Secteur> entityList) {
        return entityList.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }
    
    public Secteur fromID(Long id){
        if(id == null){
            return null;
        }
        Secteur secteur = new Secteur();
        secteur.setId(id);
        return secteur;
    }
    
}
