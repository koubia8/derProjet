/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thinkbox.sn.service.mapper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.thinkbox.sn.domain.Demande;
import org.thinkbox.sn.domain.Equipement;
import org.thinkbox.sn.service.dto.EquipementDTO;

/**
 *
 * @author guisse
 */
@Service
public class EquipementMapper implements EntityMapper<EquipementDTO, Equipement>{
    
    @Override
    public Equipement toEntity(EquipementDTO dto) {
        if(dto == null){
            return null;
        }
        Equipement equipement = new Equipement();
        if(dto.getDemandeId() != null){
            Demande demande = new Demande();
            demande.setId(dto.getDemandeId());
            equipement.setDemande(demande);
        }
        equipement.setId(dto.getId());
        equipement.setType(dto.getType());
        equipement.setQuantite(dto.getQuantite());
        equipement.setPrixUnitaire(dto.getPrixUnitaire());
        equipement.setFournisseur(dto.getFournisseur());
        return equipement;
    }

    @Override
    public EquipementDTO toDto(Equipement entity) {
        
        if(entity == null){
            return null;
        }
        EquipementDTO equipementDTO =new EquipementDTO();
        equipementDTO.setId(entity.getId());
        equipementDTO.setType(entity.getType());
        equipementDTO.setQuantite(entity.getQuantite());
        equipementDTO.setPrixUnitaire(entity.getPrixUnitaire());
        equipementDTO.setFournisseur(entity.getFournisseur());
        return equipementDTO;
    }

    @Override
    public List<Equipement> toEntity(List<EquipementDTO> dtoList) {
        return dtoList.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<EquipementDTO> toDto(List<Equipement> entityList) {
        return entityList.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }
}
