package org.thinkbox.sn.service.dto;

import java.io.Serializable;

import org.thinkbox.sn.domain.Departement;

public class DepartementDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String nom;
	
	private Long idRegion;
	
	private String nomRegion;

	public DepartementDTO() {
		
	}
	
	public Departement toEntity() {
		Departement entity = new Departement();
		entity.setNom(this.nom);
		return entity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Long getIdRegion() {
		return idRegion;
	}

	public void setIdRegion(Long idRegion) {
		this.idRegion = idRegion;
	}

	public String getNomRegion() {
		return nomRegion;
	}

	public void setNomRegion(String nomRegion) {
		this.nomRegion = nomRegion;
	}
}
